
var gulp = require('gulp');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var del = require('del');

var sources = [
	'./src/core/BlockView.js',
	'./src/core/Direction.js',
	'./src/core/VertexShifter.js',
	'./src/core/EnvironmentObject.js',
	'./src/core/Generator.js',
	'./src/core/Region.js',
	'./src/core/RegionView.js',
	'./src/core/Resources.js',
	'./src/core/SeedRandom.min.js',
	'./src/core/World.js',
	'./src/core/WorldManager.js',
	'./src/core/WorldView.js',
	'./src/geometry/*.js',
	'./src/regions/*.js',
	'./src/objects/*.js',
	'./src/extra/*.js'
];

gulp.task('default', ['clean'], function() {
	gulp.start('scripts');
});

gulp.task('watch', function() {
	gulp.watch('src/**/*.js', ['scripts']);
});

gulp.task('scripts', function() {
	return gulp.src(sources)
		.pipe(concat('world-engine.js'))
		.pipe(gulp.dest('./dist/'))
		.pipe(rename({ suffix: '.min' }))
		.pipe(uglify({ output: { max_line_len: 8000 } }))
		.pipe(gulp.dest('./dist/'));
});

gulp.task('clean', function(callback) {
	del(['./dist/*'], callback);
});
