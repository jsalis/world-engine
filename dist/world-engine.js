/**
 * @class			BlockView
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Builds geometry to represent a single element of region data.
 */
BlockView = (function() {

	BlockViewLayer = Object.freeze({
		CEILING:  'ceiling',
		FLOOR:    'floor',
		WATER:    'water',
		WALL:     'wall',
		BOUNDARY: 'boundary',
	});

	/**
	 * @constant {Integer}  The scale of the wall height.
	 */
	BlockView.HEIGHT_SCALE = 2;

	/**
	 * @constant {Integer}  The maximum number of block neighbors (equal to Moore neighborhood).
	 */
	var MAX_NEIGHBORS = 8;

	/**
	 * @var {Object}  The object containing block configuration variables.
	 */
	var bc = {};

	/**
	 * Class Constructor
	 */
	function BlockView() {}

	/**
	 * Creates a block mesh for a single element in the region data. The geometry depends on the 
	 * value of the element and its immediate neighbors.
	 * 
	 * @param  {Array}    data  A 2D array of region data.
	 * @param  {Integer}  row   The row position of the block.
	 * @param  {Integer}  col   The column position of the block.
	 */
	BlockView.generate = function(data, row, col)
	{
		configure(data, row, col);

		var result = {};

		result.geometry = [];
		result.matrix = [];

		generateWall(result);
		generateCeiling(result);
		generateFloor(result);
		generateWater(result);

		return result;
	};

	/**
	 * Interprets the region data around the given row and column, and configures the static variables 
	 * used for block generation.
	 * 
	 * @param  {Array}    data  A 2D array of region data.
	 * @param  {Integer}  row   The row position of the block.
	 * @param  {Integer}  col   The column position of the block.
	 */
	function configure(data, row, col)
	{
		// Get the data layers
		var blockData = data[ RegionLayer.BLOCK ];
		var waterData = data[ RegionLayer.WATER ];
		var floorData = data[ RegionLayer.FLOOR ];

		// Set the block row, column, and type
		bc.row = row;
		bc.col = col;
		bc.type = blockData[ row ][ col ];

		// Set flag for the water
		bc.hasWater = waterData[ row ][ col ];

		// Set flags for the center and the neighbors
		bc.center = floorData[ row ][ col ];
		bc.north = floorData[ row - 1 ][ col ];
		bc.south = floorData[ row + 1 ][ col ];
		bc.west = floorData[ row ][ col - 1 ];
		bc.east = floorData[ row ][ col + 1 ];

		bc.northWest = floorData[ row - 1 ][ col - 1 ];
		bc.northEast = floorData[ row - 1 ][ col + 1 ];
		bc.southWest = floorData[ row + 1 ][ col - 1 ];
		bc.southEast = floorData[ row + 1 ][ col + 1 ];

		// Set flags for the four possible tip configurations
		var nn = !bc.north && bc.west && bc.south && bc.east;
		var ss = bc.north && bc.west && !bc.south && bc.east;
		var ww = bc.north && !bc.west && bc.south && bc.east;
		var ee = bc.north && bc.west && bc.south && !bc.east;

		bc.isTip = (nn || ss || ww || ee) && !bc.center;

		// Set flags for the four possible corner configurations
		var nw = !bc.north && !bc.west && bc.south && bc.east;
		var ne = !bc.north && !bc.east && bc.south && bc.west;
		var sw = !bc.south && !bc.west && bc.north && bc.east;
		var se = !bc.south && !bc.east && bc.north && bc.west;

		bc.isCorner = (nw || ne || sw || se) && !bc.center;

		bc.colorCorner = (nw && !bc.northWest) || (ne && !bc.northEast) || (sw && !bc.southWest) || (se && !bc.southEast);

		// Count total neighbors excluding center
		var neighborCount = 0;
		
		for (var i = -1; i <= 1; i++)
		{
			for (var j = -1; j <= 1; j++)
			{
				if (i === 0 && j === 0)
					continue;

				if (!floorData[row + i][col + j])
					neighborCount ++;
			}
		}

		bc.isSide = neighborCount != MAX_NEIGHBORS && !bc.center;

		// Set rotation matrix
		bc.rotation = 0;
		bc.matrix = new THREE.Matrix4();

		if (ss || se)
		{
			bc.rotation = Math.PI;
			bc.matrix.makeRotationY(bc.rotation);
		}
		else if (ww || sw)
		{
			bc.rotation = Math.PI / 2;
			bc.matrix.makeRotationY(bc.rotation);
		}
		else if (ee || ne)
		{
			bc.rotation = Math.PI / -2;
			bc.matrix.makeRotationY(bc.rotation);
		}
	}

	/**
	 * Generates a wall geometry and a matrix based on the block configuration.
	 * 
	 * @param  {Object}  result  The container object.
	 */
	function generateWall(result)
	{
		var geometry, matrix, layer = BlockViewLayer.WALL;

		if (bc.isTip)
		{
			geometry = Resources.wallList[ bc.type ].tip;
		}
		else if (bc.isCorner)
		{
			geometry = Resources.wallList[ bc.type ].corner;
		}
		else if (bc.isSide)
		{
			geometry = new THREE.Geometry();

			var sideGeometry = Resources.wallList[ bc.type ].side;

			if (bc.north)
			{
				matrix = new THREE.Matrix4().makeRotationY(Math.PI);
				geometry.merge(sideGeometry, matrix);
			}

			if (bc.east)
			{
				matrix = new THREE.Matrix4().makeRotationY(Math.PI / 2);
				geometry.merge(sideGeometry, matrix);
			}

			if (bc.west)
			{
				matrix = new THREE.Matrix4().makeRotationY(Math.PI / -2);
				geometry.merge(sideGeometry, matrix);
			}

			if (bc.south)
			{
				geometry.merge(sideGeometry);
			}
		}
		else
			return;

		// Set material index for faces
		for (var n = 0; n < geometry.faces.length; n++)
		{
			geometry.faces[n].materialIndex = bc.type;
		}

		// Set block matrix
		matrix = new THREE.Matrix4().makeTranslation(0, BlockView.HEIGHT_SCALE / 2, 0);
		matrix.multiply(bc.matrix);

		result.geometry[ layer ] = geometry;
		result.matrix[ layer ] = matrix;
	}

	/**
	 * Generates a ceiling geometry and a matrix based on the block configuration.
	 * 
	 * @param  {Object}  result  The container object.
	 */
	function generateCeiling(result)
	{
		var geometry, matrix, layer = BlockViewLayer.CEILING;

		if (bc.isTip)
		{
			geometry = Resources.ceilingList[ bc.type ].tip;
			BasicSideCeiling.computeVertexUvs(geometry, bc.rotation);
		}
		else if (bc.isCorner)
		{
			geometry = Resources.ceilingList[ bc.type ].corner;
			BasicSideCeiling.computeVertexUvs(geometry, bc.rotation);

			geometry.removeColor();

			if (bc.colorCorner)
				geometry.colorCorner();
		}
		else if (bc.isSide)
		{
			geometry = new THREE.Geometry();

			var geo, sideGeometry = Resources.ceilingList[ bc.type ].side;

			matrix = new THREE.Matrix4().makeRotationY(Math.PI);
			geo = (bc.north) ? sideGeometry : Resources.sideCeiling;
			geometry.merge(geo, matrix);

			matrix = new THREE.Matrix4().makeRotationY(Math.PI / 2);
			geo = (bc.east) ? sideGeometry : Resources.sideCeiling;
			geometry.merge(geo, matrix);

			matrix = new THREE.Matrix4().makeRotationY(Math.PI / -2);
			geo = (bc.west) ? sideGeometry : Resources.sideCeiling;
			geometry.merge(geo, matrix);

			geo = (bc.south) ? sideGeometry : Resources.sideCeiling;
			geometry.merge(geo);

			BasicSideCeiling.computeVertexUvs(geometry);

			if (!bc.northWest && !bc.north && !bc.west)
				BasicSideCeiling.colorNorthWest(geometry);

			if (!bc.northEast && !bc.north && !bc.east)
				BasicSideCeiling.colorNorthEast(geometry);

			if (!bc.southWest && !bc.south && !bc.west)
				BasicSideCeiling.colorSouthWest(geometry);

			if (!bc.southEast && !bc.south && !bc.east)
				BasicSideCeiling.colorSouthEast(geometry);
		}
		else if (!bc.center)
		{
			geometry = Resources.fullCeiling;
		}
		else
			return;

		// Set material index for faces
		for (var n = 0; n < geometry.faces.length; n++)
		{
			geometry.faces[n].materialIndex = bc.type;
		}

		// Set block matrix
		matrix = new THREE.Matrix4().makeTranslation(0, BlockView.HEIGHT_SCALE, 0);
		matrix.multiply(bc.matrix);

		result.geometry[ layer ] = geometry;
		result.matrix[ layer ] = matrix;
	}

	/**
	 * Generates a floor geometry and a matrix based on the block configuration.
	 * 
	 * @param  {Object}  result  The container object.
	 */
	function generateFloor(result)
	{
		if (!bc.isTip && !bc.isCorner && !bc.isSide && !bc.center)
			return;

		var layer = BlockViewLayer.FLOOR;
		var mat = Resources.materialList[ layer ].materials[ bc.type ];
		var rowRepeat = ('rowRepeat' in mat) ? mat.rowRepeat : 1;
		var colRepeat = ('colRepeat' in mat) ? mat.colRepeat : 1;

		BasicSideCeiling.computeVertexUvs(Resources.basicPlane, 0, bc.row, bc.col, rowRepeat, colRepeat);
		
		result.geometry[ layer ] = Resources.basicPlane;

		// Set material index for faces
		for (var n = 0; n < result.geometry[ layer ].faces.length; n++)
		{
			result.geometry[ layer ].faces[n].materialIndex = bc.type;
		}
	}

	/**
	 * Generates a water geometry and a matrix based on the block configuration.
	 * 
	 * @param  {Object}  result  The container object.
	 */
	function generateWater(result)
	{
		if (bc.hasWater)
			result.geometry[ BlockViewLayer.WATER ] = Resources.basicPlane;
	}

	return BlockView;

})();

/**
 * @class			Direction
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Extension of THREE.Vector2 that represents grid directions.
 */
Direction = (function() {

	/**
	 * Class Constructor
	 * 
	 * @param  {Number}  row  The row direction.
	 * @param  {Number}  col  The column direction.
	 */
	function Direction(row, col)
	{
		THREE.Vector2.call(this, col, row);
	}

	Direction.prototype = Object.create(THREE.Vector2.prototype);

	Direction.prototype.constructor = Direction;

	Object.defineProperty(Direction.prototype, 'row', {

		get: function()
		{
			return this.y;
		}
	});

	Object.defineProperty(Direction.prototype, 'col', {

		get: function()
		{
			return this.x;
		}
	});

	Direction.CARDINALS = Object.freeze({
		N: Object.freeze(new Direction(-1, 0)),
		W: Object.freeze(new Direction(0, -1)),
		S: Object.freeze(new Direction(1, 0)),
		E: Object.freeze(new Direction(0, 1)),
	});

	Direction.NEIGHBORS = Object.freeze({
		NW: Object.freeze(new Direction(-1, -1)),
		N:  Object.freeze(new Direction(-1, 0)),
		NE: Object.freeze(new Direction(-1, 1)),
		W:  Object.freeze(new Direction(0, -1)),
		C:  Object.freeze(new Direction(0, 0)),
		E:  Object.freeze(new Direction(0, 1)),
		SW: Object.freeze(new Direction(1, -1)),
		S:  Object.freeze(new Direction(1, 0)),
		SE: Object.freeze(new Direction(1, 1)),
	});

	return Direction;

})();

/**
 * @class           VertexShifter
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     
 */
VertexShifter = (function() {

	var ROUGHNESS = 0.32;

	var MAX_DISPLACE = 0.25;

	var SCALE = 2;

	var BASE_OFFSET = 0.35;

	/**
	 * @constant {Integer}  The decimal precision to use when comparing vertex positions.
	 */
	var PRECISION = Math.pow(10, 1);

	/**
	 * @constant {Array}    The list of valid face indices.
	 */
	var FACE_INDICES = ['a', 'b', 'c'];

	/**
	 * @constructor
	 * @param  {Number}  size  The size of geometry chunks. Used to determine which vertices are shared.
	 */
	function VertexShifter(size)
	{
		this.size = size;
		this._map = {};
	}

	/**
	 * Corrects geometry by applying the vertex transformation map.
	 * 
	 * @param  {THREE.Geometry}  geometry     The geometry to correct.
	 * @param  {THREE.Matrix4}   matrixWorld  A matrix to transform a vertex into world coordinates.
	 */
	function _correctGeometry(geometry, matrixWorld)
	{
		var inverseMatrix = new THREE.Matrix4().getInverse(matrixWorld);

		for (var i = 0; i < geometry.vertices.length; i++)
		{
			var vertex = geometry.vertices[ i ];
			var vertexWorld = vertex.clone().applyMatrix4(matrixWorld);
			var key = _encodeKey(vertexWorld);

			if (this._map[ key ] !== undefined)
			{
				var correctVertex = this._map[ key ].clone().applyMatrix4(inverseMatrix);

				vertex.x = correctVertex.x;
				vertex.z = correctVertex.z;
			}
		}
	}

	/**
	 * Transforms vertices of a geometry in the direction of their normals.
	 * 
	 * @param  {THREE.Geometry}  geometry     The geometry to apply the transformation.
	 * @param  {THREE.Matrix4}   matrixWorld  A matrix to transform a vertex into world coordinates.
	 * @param  {String}          seed         A seed for the random generator.
	 */
	function _generateNoise(geometry, matrixWorld, seed, targets)
	{
		var normList = [];
		var ignore = [];

		// Map each vertex index to its normal
		for (var i = 0; i < geometry.faces.length; i++)
		{
			var face = geometry.faces[ i ];
			for (var m = 0; m < FACE_INDICES.length; m++)
			{
				var current = face[ FACE_INDICES[ m ] ];
				normList[ current ] = face.vertexNormals[ m ].clone();
				ignore[ current ] = (targets.indexOf(face.materialIndex) === -1);
			}
		}

		var gen = new Math.seedrandom(seed);
		var heightMap = Generator.getHeightMap(SCALE, ROUGHNESS, MAX_DISPLACE, gen());
		var size = Math.pow(2, SCALE) + 1;
		var index = 0;

		var inverseMatrix = new THREE.Matrix4().getInverse(matrixWorld);

		for (var i = 0; i < geometry.vertices.length; i++)
		{
			var vertex = geometry.vertices[ i ];
			var vertexWorld = vertex.clone().applyMatrix4(matrixWorld);
			var isShared = _isSharedVertex(vertex, this.size);
			var onCeiling = vertex.y > BlockView.HEIGHT_SCALE - (1 / PRECISION);

			var key = _encodeKey(vertexWorld);

			if (this._map[ key ] !== undefined)
			{
				var correctVertex = this._map[ key ].clone().applyMatrix4(inverseMatrix);

				vertex.x = correctVertex.x;
				vertex.z = correctVertex.z;
			}
			else
			{
				if (!ignore[ i ])
				{
					var row = Math.floor(index / size);
					var col = index % size;

					vertex.x += normList[ i ].x * heightMap[ row ][ col ];
					vertex.z += normList[ i ].z * heightMap[ row ][ col ];

					if (vertex.y < (1 / PRECISION))
					{
						vertex.x += normList[ i ].x * BASE_OFFSET;
						vertex.z += normList[ i ].z * BASE_OFFSET;
					}

					if (this._map[ key ] === undefined && (isShared || onCeiling))
					{
						this._map[ key ] = vertex.clone().applyMatrix4(matrixWorld);
					}
				}
				else if (isShared)
				{
					this._map[ key ] = vertex.clone().applyMatrix4(matrixWorld);
				}
			}

			index ++;
			if (index >= size * size)
			{
				heightMap = Generator.getHeightMap(SCALE, ROUGHNESS, MAX_DISPLACE, gen());
				index = 0;
			}
		}

		geometry.computeFaceNormals();
		geometry.computeVertexNormals();
	}

	/**
	 * Removes all shared vertices with a position outside the defined area.
	 * This function is for memory management.
	 * 
	 * @param  {Number}  z         The center Z position.
	 * @param  {Number}  x         The center X position.
	 * @param  {Number}  distance  The distance from the center.
	 */
	function _clean(z, x, distance, container)
	{
		for (var prop in this._map)
		{
			var vertex = _decodeKey(prop);

			if (vertex.x > x + distance || vertex.x < x - distance ||
				vertex.z > z + distance || vertex.z < z - distance)
			{
				delete this._map[ prop ];
			}
		}
	}

	/**
	 * Determines whether a vertex is on the edge of a chunk, where it is shared by other chunks.
	 * 
	 * @param  {THREE.Vector3}  vertex  The vertex to test.
	 * @return {Boolean}                Whether the vertex is shared by multiple chunks.
	 */
	function _isSharedVertex(vertex, size)
	{
		var halfSize = size / 2;

		var farEast = vertex.x > halfSize - 0.1;
		var farWest = vertex.x < -halfSize + 0.1;
		var farNorth = vertex.z < -halfSize + 0.1;
		var farSouth = vertex.z > halfSize - 0.1;

		return farEast || farWest || farNorth || farSouth;
	}

	/**
	 * Encodes a vector into a unique string defined by its position.
	 * 
	 * @param  {THREE.Vector3}  vector  The vector to encode.
	 * @return {String}                 The encoded string.
	 */
	function _encodeKey(vector)
	{
		return Math.round(vector.x * PRECISION) + '_' +
			   Math.round(vector.y * PRECISION) + '_' +
			   Math.round(vector.z * PRECISION);
	}

	/**
	 * Decodes a string into a vector.
	 * 
	 * @param  {String}        string   The string to decode.
	 * @return {THREE.Vector3}          The decoded vector.
	 */
	function _decodeKey(string)
	{
		var parts = string.split('_');
		var vx = parseInt(parts[0], 10) / PRECISION;
		var vy = parseInt(parts[1], 10) / PRECISION;
		var vz = parseInt(parts[2], 10) / PRECISION;

		return new THREE.Vector3(vx, vy, vz);
	}

	VertexShifter.prototype = {
		constructor:     VertexShifter,
		correctGeometry: _correctGeometry,
		generateNoise:   _generateNoise,
		clean:           _clean
	};

	return VertexShifter;

})();

/**
 * @class			EnvironmentObject
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
EnvironmentObject = (function() {

	/**
	 * Class Constructor
	 */
	function EnvironmentObject()
	{
		THREE.Object3D.call(this);

		this.tick = 0;
	}

	EnvironmentObject.generate = function(grid, outerRow, outerCol, innerRow, innerCol) {};

	EnvironmentObject.prototype = Object.create(THREE.Object3D.prototype);

	EnvironmentObject.prototype.constructor = EnvironmentObject;

	/**
	 * Updates the environment object for the next frame.
	 */
	EnvironmentObject.prototype.update = function()
	{
		this.tick ++;
	};

	THREE.EventDispatcher.prototype.apply(EnvironmentObject.prototype);

	return EnvironmentObject;

})();

/**
 * @class			Generator
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Holds static functions for generating and mutating two dimensional 
 *                  data sets using various algorithms.
 */
Generator = (function() {

	/**
	 * Class Constructor
	 */
	function Generator() {}

	/**
	 * Checks if the specified element exists within a 2D array of data.
	 * 
	 * @param  {Array}		data	The 2D array of data.
	 * @param  {Number}		row     The row of the array to check.
	 * @param  {Number}		col     The column of the array to check.
	 * @return {Boolean}			Whether the element is not null.
	 */
	function elementExists(data, row, col)
	{
		return (data != null && data[row] != null && data[row][col] != null);
	}

	/**
	 * Gets a random element from a list of data.
	 * 
	 * @param  {Array} data		The array pick an element from.
	 * @return {var}
	 */
	function randomElement(data, random)
	{
		return data[ Math.floor(random() * data.length) ];
	}

	/**
	 * Checks if a given position is in the range of a data set.
	 * 
	 * @param  {Array}   data The 2D array of data.
	 * @param  {Number}  row  The row of the position.
	 * @param  {Number}  col  The column of the position.
	 * @return {Boolean}      True if in range, false otherwise.
	 */
	function inRange(data, row, col)
	{
		return checkIndex(data, row) === 0 && checkIndex(data[0], col) === 0;
	}

	/**
	 * Compares the index of an array to the range of the data. Output is 0 if
	 * the index is within range, -1 if below the range, and 1 if above the range.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} index   The index to compare.
	 * @return {Number}			The result of the comparison. {-1, 0, 1}
	 */
	function checkIndex(data, index)
	{
		if (index < 0)
		{
			return -1;
		}
		else if (index >= data.length)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Wraps an index of an array around the range of the data.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} index   The index to wrap.
	 * @return {Number}			The new index.
	 */
	function wrapIndex(data, index)
	{
		if (index < 0)
		{
			return index + (data.length - 1);
		}
		else if (index >= data.length)
		{
			return index - (data.length - 1);
		}
		else
		{
			return index;
		}
	}

	/**
	 * Localizes a specified location within a 2D array of 2D data by shifting the location when 
	 * the inner location is out of range. The inner data sets should also be 2D arrays but they 
	 * are not accessed within this function.
	 * 
	 * @param  {Array}  data		The 2D array of data.
	 * @param  {Number} outerRow	The outer row location.
	 * @param  {Number} outerCol	The outer column location.
	 * @param  {Number} innerRow	The inner row location.
	 * @param  {Number} innerCol	The inner column location.
	 * @return {Object}				An object containing the local data set and the new inner location.
	 */
	function localize(data, outerRow, outerCol, innerRow, innerCol)
	{
		if (!elementExists(data, outerRow, outerCol))
			return null;

		var check, temp = data[outerRow][outerCol];

		// Localize row index
		check = checkIndex(temp, innerRow);
		while (check !== 0)
		{
			outerRow += check;
			innerRow -= check * temp.length;
			check = checkIndex(temp, innerRow);
		}

		// Localize column index
		check = checkIndex(temp, innerCol);
		while (check !== 0)
		{
			outerCol += check;
			innerCol -= check * temp.length;
			check = checkIndex(temp, innerCol);
		}

		if (elementExists(data, outerRow, outerCol))
		{
			return {
				data: data[outerRow][outerCol],
				row: innerRow,
				col: innerCol,
			};
		}
		else
		{
			return null;
		}
	}

	/**
	 * [getElement description]
	 * 
	 * @param  {[type]} regList    [description]
	 * @param  {[type]} regRow     [description]
	 * @param  {[type]} regCol     [description]
	 * @param  {[type]} row        [description]
	 * @param  {[type]} col        [description]
	 * @param  {[type]} layerIndex [description]
	 * @return {[type]}            [description]
	 */
	Generator.getElement = function(regList, regRow, regCol, row, col, layerIndex)
	{
		var local = localize(regList, regRow, regCol, row, col);

		if (local != null)
			return local.data.getElement(local.row, local.col, layerIndex);
		else
			return null;
	};

	/**
	 * [setElement description]
	 * 
	 * @param {[type]} regList    [description]
	 * @param {[type]} regRow     [description]
	 * @param {[type]} regCol     [description]
	 * @param {[type]} row        [description]
	 * @param {[type]} col        [description]
	 * @param {[type]} layerIndex [description]
	 * @param {[type]} val        [description]
	 */
	Generator.setElement = function(regList, regRow, regCol, row, col, layerIndex, val)
	{
		var local = localize(regList, regRow, regCol, row, col);

		if (local != null)
			local.data.setElement(local.row, local.col, val, layerIndex);
	};

	/**
	 * Gets a random value that can be used seed any random generation.
	 * 
	 * @return {String}
	 */
	Generator.getSeed = function()
	{
		return Math.seedrandom(null, { pass: function(prng, seed) {
			return seed;
		}});
	};

	/**
	 * [getRandomData description]
	 * 
	 * @param  {[type]} size         [description]
	 * @param  {[type]} distribution [description]
	 * @param  {[type]} seed         [description]
	 * @return {[type]}              [description]
	 */
	Generator.getRandomData = function(size, distribution, options)
	{
		options = options || {};
		var random = ('seed' in options) ? new Math.seedrandom(options.seed) : Math.random;
		var low = ('low' in options) ? options.low : 0;
		var high = ('high' in options) ? options.high : 1;

		var data = new Array(size);
		for (var i = 0; i < data.length; i++)
		{
			data[i] = new Array(size);
			for (var j = 0; j < data[i].length; j++)
			{
				data[i][j] = (random() < distribution) ? high : low;
			}
		}
		return data;
	};

	/**
	 * Gets a random row and column position of a 2D array that matches a given value.
	 * All matching elements are equally likely to be chosen.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} value	The value to search for.
	 * @param  {String} seed    The seed value for the random generator.
	 * @return {Object}			An object containing a row and column. Null if no match is found.
	 */
	Generator.getRandomPosition = function(data, value, seed)
	{
		var random = (seed == null) ? Math.random : new Math.seedrandom(seed);
		var elements = [];

		for (var i = 0; i < data.length; i++)
		{
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == value)
				{
					elements.push({ row : i, col : j });
				}
			}
		}

		if (elements.length == 0)
		{
			return null;
		}
		else
		{
			var index = Math.floor(random() * (elements.length - 1));
			return elements[index];
		}
	};

	/**
	 * Counts all elements in the data that match the specified value.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} value	The value to search for.
	 * @return {Number}			The number of elements in the data that match the value.
	 */
	Generator.countElements = function(data, value)
	{
		var count = 0;
		for (var i = 0; i < data.length; i++)
		{
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == value)
					count ++;
			}
		}
		return count;
	};

	/**
	 * Finds a random element on a spedified edge of a data set. There are four possible edges,
	 * the top and bottom row, and the left and right column. Any element on the far edge has
	 * an equal probability of being chosen.
	 *
	 * @param  {Array}    data    The 2D array of data.
	 * @param  {Number}   target  The target value to search for.
	 * @param  {Boolean}  rowDir  The row direction of the search. {-1, 0, 1}
	 * @param  {Boolean}  colDir  The column direction of the search. {-1, 0, 1}
	 * @return {Object}           An object containing the row and col. Null if not found.
	 */
	Generator.findFromEdge = function(data, target, rowDir, colDir, seed)
	{
		var random = (seed === null) ? Math.random : new Math.seedrandom(seed);
		var positive = (rowDir > 0 || colDir > 0);
		var start = (positive) ? 0 : data.length - 1;
		var d = (positive) ? 1 : -1;

		for (var i = start; i >= 0 && i < data.length; i += d)
		{
			var list = [];
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == target && colDir === 0)
				{
					list.push({ row: i, col: j });
				}
				else if (data[j][i] == target && rowDir === 0)
				{
					list.push({ row: j, col: i });
				}
			}
			if (list.length > 0)
			{
				var index = Math.round(random() * (list.length - 1));
				return list[index];
			}
		}
		return null;
	};

	/**
	 * [applyCellAutomata description]
	 * 
	 * @param  {[type]} data    [description]
	 * @param  {[type]} born    [description]
	 * @param  {[type]} survive [description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	Generator.applyCellAutomata = function(data, born, survive, options)
	{
		options = options || {};
		var passCount = options.passCount || 1;
		var neighborhoodSize = options.neighborhoodSize || 1;
		var regList = options.regList || null;
		var regRow = ('regRow' in options) ? options.regRow : null;
		var regCol = ('regCol' in options) ? options.regCol : null;
		var layerIndex = ('layerIndex' in options) ? options.layerIndex : null;
		var live = ('live' in options) ? options.live : 1;
		var dead = ('dead' in options) ? options.dead : 0;

		var newData = data;
		for (var n = 0; n < passCount; n++)
		{
			var tempData = new Array(data.length);
			for (var i = 0; i < data.length; i++)
			{
				tempData[i] = new Array(data[i].length);
				for (var j = 0; j < data[i].length; j++)
				{
					tempData[i][j] = generateAt(newData, i, j);
				}
			}
			newData = tempData;
		}

		function generateAt(data, row, col)
		{
			var neighborCount = 0;
			for (var i = -neighborhoodSize; i <= neighborhoodSize; i++)
			{
				for (var j = -neighborhoodSize; j <= neighborhoodSize; j++)
				{
					if (i == 0 && j == 0)
						continue;

					if (checkIndex(data, row + i) == 0 && checkIndex(data, col + j) == 0)
					{
						if (data[row + i][col + j] == live)
						{
							neighborCount ++;
						}
					}
					else if (regList != null && Generator.getElement(regList, regRow, regCol, row + i, col + j, layerIndex) == live) 
					{
						neighborCount ++;
					}
				}
			}

			if (neighborCount >= born)
			{
				return live;
			}
			else if (neighborCount >= survive && data[row][col] == live)
			{
				return live;
			}
			else
			{
				return dead;
			}
		}

		return newData;
	};

	/**
	 * [floodFill description]
	 * 
	 * @param  {[type]} data        [description]
	 * @param  {[type]} row         [description]
	 * @param  {[type]} col         [description]
	 * @param  {[type]} target      [description]
	 * @param  {[type]} replacement [description]
	 * @param  {[type]} options     [description]
	 */
	Generator.floodFill = function(data, row, col, target, replace, options)
	{
		options = options || {};
		var spread = ('spread' in options) ? options.spread : 1;
		var decay = ('decay' in options) ? options.decay : 1;
		var useCardinals = ('useCardinals' in options) ? options.useCardinals : false;
		var regList = ('regList' in options) ? options.regList : null;
		var regRow = ('regRow' in options) ? options.regRow : null;
		var regCol = ('regCol' in options) ? options.regCol : null;
		var fromLayer = ('fromLayer' in options) ? options.fromLayer : null;
		var toLayer = ('toLayer' in options) ? options.toLayer : null;

		var random = options.random || (('seed' in options) ? new Math.seedrandom(options.seed) : Math.random);

		var directions = (useCardinals) ? Direction.CARDINALS : Direction.NEIGHBORS;

		function floodFillRecursive(row, col, spread)
		{
			var fromElement, toElement;

			if (regList == null)
			{
				if (checkIndex(data, row) == 0 && checkIndex(data, col) == 0)
				{
					fromElement = toElement = data[row][col];
				}
				else
					return;
			}
			else
			{
				fromElement = Generator.getElement(regList, regRow, regCol, row, col, fromLayer);
				toElement = Generator.getElement(regList, regRow, regCol, row, col, toLayer);
			}
			
			if (toElement == replace)
			{
				return;
			}
			else if (fromElement == target)
			{
				if (regList == null)
				{
					data[row][col] = replace;
				}
				else
				{
					Generator.setElement(regList, regRow, regCol, row, col, toLayer, replace);
				}

				for (var d in directions)
				{
					var dir = directions[d];

					if (dir.row == 0 && dir.col == 0)
					{
						continue;
					}
					else if (spread == 1 || random() < spread)
					{
						floodFillRecursive(row + dir.row, col + dir.col, spread * decay);
					}
				}
			}
		}

		floodFillRecursive(row, col, spread);
	};

	/**
	 * [getHeightMap description]
	 * 
	 * @param  {[type]} scale       [description]
	 * @param  {[type]} roughness   [description]
	 * @param  {[type]} maxDisplace [description]
	 * @param  {[type]} seed        [description]
	 * @return {[type]}             [description]
	 */
	Generator.getHeightMap = function(scale, roughness, maxDisplace, seed)
	{
		var gen = (seed == null) ? null : new Math.seedrandom(seed);
		var size = Math.pow(2, scale) + 1;
		var map = new Array(size);

		for (var i = 0; i < map.length; i++)
		{
			map[i] = new Array(size);
			for (var j = 0; j < map[i].length; j++)
			{
				map[i][j] = 0;
			}
		}

		function calcDisplace(total, displace)
		{
			var random = (gen == null) ? Math.random() : gen();
			return (total / 4) + (random * displace * 2) - displace;
		}

		function squareStep(row, col, unit, displace)
		{
			if (map[row][col] != 0)
				return;

			var total = map[wrapIndex(map, row + unit)][col] + map[wrapIndex(map, row - unit)][col] +
						map[row][wrapIndex(map, col + unit)] + map[row][wrapIndex(map, col - unit)];
			map[row][col] = calcDisplace(total, displace);

			if (row == 0)
			{
				map[map.length - 1][col] = map[row][col];
				return;
			}
			else if (col == 0)
			{
				map[row][map.length - 1] = map[row][col];
				return;
			}
		}

		var unit = (map.length - 1) / 2;
		var displace = maxDisplace;

		while (unit >= 1)
		{
			// Diamond Step
			for (var row = 0; row < map.length - 1; row += unit * 2)
			{
				for (var col = 0; col < map.length - 1; col += unit * 2)
				{
					var total = map[row][col] + map[row][col + (unit * 2)] + 
								map[row + (unit * 2)][col] + map[row + (unit * 2)][col + (unit * 2)];
					map[row + unit][col + unit] = calcDisplace(total, displace);
				}
			}

			// Square Step
			for (var row = 0; row < map.length - 1; row += unit * 2)
			{
				for (var col = 0; col < map.length - 1; col += unit * 2)
				{
					squareStep(row, col + unit, unit, displace);
					squareStep(row + unit, col, unit, displace);
					squareStep(row + unit, col + (unit * 2), unit, displace);
					squareStep(row + (unit * 2), col + unit, unit, displace);
				}
			}

			unit /= 2;
			displace *= roughness;
		}

		return map;
	};

	/**
	 * [growMaze description]
	 * 
	 * @param  {[type]} data    [description]
	 * @param  {[type]} row     [description]
	 * @param  {[type]} col     [description]
	 * @param  {[type]} replace [description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	Generator.growMaze = function(data, row, col, replace, options)
	{
		options = options || {};
		var walkable = ('walkable' in options) ? options.walkable : null;
		var runFactor = ('runFactor' in options) ? options.runFactor : 0.5;
		var random = ('random' in options) ? options.random : Math.random;

		// TO DO : Assert that replace is not equal to walkable.

		var lastDir, nodes = [{ row: row, col: col }];

		data[row][col] = replace;

		while (nodes.length > 0)
		{
			var node = nodes[nodes.length - 1];
			var localDirs = expand(node.row, node.col);

			if (localDirs.length > 0)
			{
				var dir;

				if (localDirs.indexOf(lastDir) != -1 && random() < runFactor)
					dir = lastDir;
				else
					dir = randomElement(localDirs, random);

				var r = node.row + dir.row;
				var c = node.col + dir.col;

				data[r][c] = replace;

				r += dir.row;
				c += dir.col;

				data[r][c] = replace;

				nodes.push({ row: r, col: c });
				lastDir = dir;
			}
			else
			{
				nodes.pop();
				lastDir = null;
			}
		}

		function expand(row, col)
		{
			var localDirs = [];

			for (var d in Direction.CARDINALS)
			{
				var dir = Direction.CARDINALS[d];
				var r = row + (dir.row * 2);
				var c = col + (dir.col * 2);

				if (!inRange(data, r, c))
					continue;

				var isWalkable;

				if (walkable === null)
					isWalkable = (data[r][c] != replace);
				else
					isWalkable = (data[r][c] == walkable);

				if (isWalkable)
					localDirs.push(dir);
			}

			return localDirs;
		}
	};

	/**
	 * [getPaddedMap description]
	 * 
	 * @param  {[type]} regList [description]
	 * @param  {[type]} regRow  [description]
	 * @param  {[type]} regCol  [description]
	 * @param  {[type]} pad     [description]
	 * @return {[type]}         [description]
	 */
	Generator.getPaddedMap = function(regList, regRow, regCol, layerIndex, pad) // TO DO : Fix pad param
	{
		var length = regList[ regRow ][ regCol ].data[ layerIndex ].length;
		var paddedMap = [];

		for (var i = -1; i <= length; i++)
		{
			paddedMap[i] = [];

			for (var j = -1; j <= length; j++)
			{
				paddedMap[i][j] = Generator.getElement(regList, regRow, regCol, i, j, layerIndex);
			}
		}

		return paddedMap;
	};

	/**
	 * [createDungeon description]
	 * 
	 * @param  {[type]} size       [description]
	 * @param  {[type]} wallValue  [description]
	 * @param  {[type]} floorValue [description]
	 * @param  {[type]} options    [description]
	 * @return {[type]}            [description]
	 */
	Generator.createDungeon = function(size, wallValue, floorValue, options)
	{
		options = options || {};
		var minRoomSize = ('minRoomSize' in options) ? options.minRoomSize : 5;
		var maxRoomSize = ('maxRoomSize' in options) ? options.maxRoomSize : 13;
		var roomAttempts = ('roomAttempts' in options) ? options.roomAttempts : 8;
		var connectivity = ('connectivity' in options) ? options.connectivity : 0;
		var random = ('seed' in options) ? new Math.seedrandom(options.seed) : Math.random;

		// TO DO : Assert that min and max room size are odd.
		// TO DO : Assert that roomAttempts is positive.
		
		var NO_CONNECTOR = 0;
		var X_CONNECTOR = 1;
		var Y_CONNECTOR = 2;

		var roomRange = maxRoomSize - minRoomSize;

		var tempWallValue = 0;
		var regionIndex = 1;
		var evenSize = false;

		if (size % 2 == 0)
		{
			size --;
			evenSize = true;
		}

		var data = Generator.getRandomData(size, 1, { high: tempWallValue });

		// Create rooms
		while (roomAttempts > 0)
		{
			if (createRoom())
				regionIndex ++;
			else
				roomAttempts --;
		}

		// Fill gaps with mazes
		for (var i = 1; i < size; i += 2)
		{
			for (var j = 1; j < size; j += 2)
			{
				if (data[i][j] == tempWallValue)
				{
					Generator.growMaze(data, i, j, regionIndex, {
						walkable: tempWallValue,
						random: random,
					});
					regionIndex ++;
				}
			}
		}

		connectRegions(findConnectorNodes());
		prepareData();
		trimEnds(findEndNodes());

		function createRoom()
		{
			var row = Math.floor(random() * (size - 1) / 2) * 2 + 1;
			var col = Math.floor(random() * (size - 1) / 2) * 2 + 1;

			var width = Math.floor(random() * (roomRange - 1) / 2) * 2 + minRoomSize;
			var height = Math.floor(random() * (roomRange - 1) / 2) * 2 + minRoomSize;

			var i, j;

			for (i = 0; i < height; i++)
			{
				for (j = 0; j < width; j++)
				{
					var inRange = (checkIndex(data, row + i) === 0 && checkIndex(data, col + j) === 0);

					if (!inRange || data[row + i][col + j] != tempWallValue)
						return false;
				}
			}
			for (i = 0; i < height; i++)
			{
				for (j = 0; j < width; j++)
				{
					data[row + i][col + j] = regionIndex;
				}
			}
			return true;
		}

		function connectRegions(nodes)
		{
			var regionCount = regionIndex - 1;

			while (regionCount > 1)
			{
				var index = Math.floor(random() * nodes.length);
				var node = nodes.splice(index, 1)[0];

				var north = data[node.row - 1][node.col];
				var south = data[node.row + 1][node.col];
				var east = data[node.row][node.col + 1];
				var west = data[node.row][node.col - 1];

				if (isConnectorNode(node.row, node.col) != NO_CONNECTOR)
				{
					if (node.type == Y_CONNECTOR)
					{
						data[node.row][node.col] = south;
						Generator.floodFill(data, node.row, node.col, south, north);

						regionCount --;
					}
					else if (node.type == X_CONNECTOR)
					{
						data[node.row][node.col] = east;
						Generator.floodFill(data, node.row, node.col, east, west);

						regionCount --;
					}
				}
				else if (north == south && east == west && random() < connectivity) // TO DO : Check for connectors on both sides.
				{
					data[node.row][node.col] = floorValue;
				}
			}
		}

		function findConnectorNodes()
		{
			var nodes = [];

			for (var i = 1; i < size - 1; i++)
			{
				for (var j = 1; j < size - 1; j++)
				{
					var type = isConnectorNode(i, j);

					if (type != NO_CONNECTOR)
						nodes.push({ row: i, col: j, type: type });
				}
			}

			return nodes;
		}

		function isConnectorNode(row, col)
		{
			if (data[row][col] != tempWallValue)
				return NO_CONNECTOR;

			var north = data[row - 1][col];
			var south = data[row + 1][col];
			var east = data[row][col + 1];
			var west = data[row][col - 1];

			if (north != south && north != tempWallValue && south != tempWallValue)
				return Y_CONNECTOR;

			if (east != west && east != tempWallValue && west != tempWallValue)
				return X_CONNECTOR;

			return NO_CONNECTOR;
		}

		function trimEnds(nodes)
		{
			for (var i = 0; i < nodes.length; i++)
			{
				var node = nodes[i];
				var exit = node.exit;

				do
				{
					data[node.row][node.col] = wallValue;
					node.row = exit.row;
					node.col = exit.col;
					exit = isEndNode(node.row, node.col);
				}
				while (exit !== false);
			}
		}

		function findEndNodes()
		{
			var nodes = [];

			for (var i = 0; i < size; i++)
			{
				for (var j = 0; j < size; j++)
				{
					var exit = isEndNode(i, j);

					if (exit !== false)
						nodes.push({ row: i, col: j, exit: exit });
				}
			}

			return nodes;
		}

		function isEndNode(row, col)
		{
			if (data[row][col] == wallValue)
				return false;

			var north = (inRange(data, row - 1, col)) ? data[row - 1][col] : wallValue;
			var south = (inRange(data, row + 1, col)) ? data[row + 1][col] : wallValue;
			var east = (inRange(data, row, col + 1)) ? data[row][col + 1] : wallValue;
			var west = (inRange(data, row, col - 1)) ? data[row][col - 1] : wallValue;

			if (north == floorValue && south == wallValue && east == wallValue && west == wallValue)
				return { row: row - 1, col: col };

			if (north == wallValue && south == floorValue && east == wallValue && west == wallValue)
				return { row: row + 1, col: col };

			if (north == wallValue && south == wallValue && east == floorValue && west == wallValue)
				return { row: row, col: col + 1 };

			if (north == wallValue && south == wallValue && east == wallValue && west == floorValue)
				return { row: row, col: col - 1 };

			return false;
		}

		function prepareData()
		{
			for (var i = 0; i < size; i++)
			{
				for (var j = 0; j < size; j++)
				{
					data[i][j] = (data[i][j] == tempWallValue) ? wallValue : floorValue;
				}
			}

			if (evenSize)
			{
				data[size] = [];
				for (var i = 0; i < size + 1; i++)
				{
					data[size][i] = data[i][size] = wallValue;
				}
			}
		}

		return data;
	};

	/**
	 * Prints a 2D array to the console.
	 * 
	 * @param  {Array} data		The 2D array of data.
	 */
	Generator.print = function(data)
	{
		if (data === null)
			return;

		var str = '';
		for (var i = 0; i < data.length; i++)
		{
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] === 0)
					str += '. ';
				else
					str += data[i][j] + ' ';
			}
			str += '\n';
		}
		console.log(str);
	};

	return Generator;

})();

/**
 * @class			Region
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Stores block data for a single region of a given size.
 */
Region = (function() {

	Region.SIZE = 30;

	RegionEvent = Object.freeze({
		BEFORE_GENERATE: 'before-generate',
		AFTER_GENERATE:  'after-generate',
	});

	RegionLayer = Object.freeze({
		BLOCK:  'block',
		FLOOR:  'floor',
		WATER:  'water',
		OBJECT: 'object',
	});

	BlockType = Object.freeze({
		ROCK:  0,
		DIRT:  1,
		STONE: 2,
		NONE:  3,
	});

	/**
	 * Class Constructor
	 */
	function Region()
	{
		this.length = Region.SIZE;
		this.layers = null;
		this.data = null;
		this.seeds = [];

		for (var t in BlockType)
		{
			var type = BlockType[t];
			this.seeds[ type ] = Generator.getSeed();
		}
	}

	/**
	 * [generate description]
	 * 
	 * @param  {[type]} regList [description]
	 * @param  {[type]} regRow  [description]
	 * @param  {[type]} regCol  [description]
	 * @return {[type]}         [description]
	 */
	Region.generate = function(regList, regRow, regCol)
	{
		Region.dispatchEvent({
			type: RegionEvent.BEFORE_GENERATE,
			list: regList,
			row:  regRow,
			col:  regCol,
		});

		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[ d ];
			var current = regList[ regRow + dir.row ][ regCol + dir.col ];

			current.exportLayers();
			current.layers = null;
		}

		Region.dispatchEvent({
			type: RegionEvent.AFTER_GENERATE,
			list: regList,
			row:  regRow,
			col:  regCol,
		});
	};

	/**
	 * Compares the index of an array to the range of the data. Output is 0 if
	 * the index is within range, -1 if below the range, and 1 if above the range.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} index   The index to compare.
	 * @return {Number}			The result of the comparison. {-1, 0, 1}
	 */
	function checkIndex(data, index) // ??
	{
		if (index < 0)
		{
			return -1;
		}
		else if (index >= data.length)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	Region.prototype = {

		constructor: Region,

		exportLayers: function() {},

		initialize: function()
		{
			this.layers = [];
			this.data = [];

			// Initialize region layers
			for (var l in RegionLayer)
			{
				var layer = RegionLayer[l];
				this.data[ layer ] = [];

				for (var i = 0; i < this.length; i++)
				{
					this.data[ layer ][i] = [];
				}
			}
		},

		getElement: function(row, col, layerIndex)
		{
			var data = (typeof layerIndex == 'string') ? this.data : this.layers;

			if (data == null || data[ layerIndex ] == null)
				return null;
			else
				return data[ layerIndex ][row][col];
		},

		setElement: function(row, col, val, layerIndex)
		{
			var data = (typeof layerIndex == 'string') ? this.data : this.layers;

			if (data != null && data[ layerIndex ] != null)
				data[ layerIndex ][row][col] = val;
		},

		clear: function()
		{
			this.layers = null;
			this.data = null;
		},

		getData: function()
		{
			return (this.data == null) ? null : this.data;
		},

		getLayer: function(i)
		{
			return (this.layers == null) ? null : this.layers[ i || 0 ];
		},

		getSeed: function(i)
		{
			return (this.seeds == null) ? null : this.seeds[ i || 0 ];
		},

		getCellCount: function()
		{
			return this.length * this.length;
		},

		getIndex: function(row, col)
		{
			if (checkIndex(row, col))
			{
				return (row * this.length) + col;
			}
			else
				return -1;
		},

		getRowCol: function(i)
		{
			var r = Math.floor(i / this.length);
			var c = i % this.length;
			return { row: r, col: c };
		},

		getCellValue: function(i)
		{
			var pos = this.getRowCol(i);
			if (checkIndex(pos.row, pos.col))
			{
				return this.data[ pos.row ][ pos.col ];
			}
			else
				return -1;
		},

		print: function(layerIndex)
		{
			Generator.print(this.data[ layerIndex ]);
		},
	};

	THREE.EventDispatcher.prototype.apply(Region);

	return Region;

})();

/**
 * @class           MapView
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     Builds geometry to represent map data and controls the adding and removing 
 *                  of objects to the region.
 */
MapView = (function() {

	/**
	 * @constant {Integer}  The number of chunks to divide the length of the map data into.
	 */
	var DIVISIONS = 2;

	/**
	 * @constant {Integer}  The size of the chunks of map data.
	 */
	var CHUNK_SIZE = Region.SIZE / DIVISIONS;

	/**
	 * @constant {Boolean}  Whether to use a single material for all layers in a chunk.
	 */
	var USE_CHUNK_MATERIAL = false;

	/**
	 * @constant {Boolean}  Whether to set a random color for each chunk material.
	 */
	var COLORIZE_CHUNKS = true;

	/**
	 * @constant {Boolean}  Whether to set the chunk material to wireframe.
	 */
	var WIREFRAME_CHUNKS = true;

	/**
	 * @var {Object}  The object containing vertex positions mapped to translated positions.
	 */
	var vertexShifter = new VertexShifter(CHUNK_SIZE);

	/**
	 * Class Constructor
	 * 
	 * @param  {Array}   map   A 2D array of map data.
	 * @param  {Number}  z     The center Z position.
	 * @param  {Number}  x     The center X position.
	 * @param  {String}  seed  The seed to be used for random generation.
	 */
	function MapView(map, z, x, seed)
	{
		this.seed = seed;

		this._lastRowDir = 0;
		this._lastColDir = 0;

		this.root = new THREE.Object3D();
		this.root.scale.set(World.SCALE, World.SCALE, World.SCALE);
		this.root.position.set(x, 0, z);
		this.root.updateMatrix();
		this.root.updateMatrixWorld();

		this._objects = new THREE.Object3D();
		this.root.add(this._objects);

		this._meshList = [];

		// Initialize mesh layers
		for (var l in BlockViewLayer)
		{
			var layer = BlockViewLayer[ l ];

			this._meshList[ layer ] = new THREE.Object3D();
			this.root.add(this._meshList[ layer ]);
		}

		// Hide the boundary layer
		this._meshList[ BlockViewLayer.BOUNDARY ].visible = false;

		generateMesh(this, map);
	}

	/**
	 * Generates all chunks of mesh and adds them to their respective containers.
	 * 
	 * @param  {MapView}  that  The map view to build on.
	 * @param  {Array}    map   A 2D array of map data.
	 */
	function generateMesh(that, map)
	{
		for (var i = 0; i < Region.SIZE; i += CHUNK_SIZE)
		{
			for (var j = 0; j < Region.SIZE; j += CHUNK_SIZE)
			{
				generateMeshAt(that, map, i, j);
			}
		}
	}

	/**
	 * Builds a chunk of mesh using a section of map data, and adds them to their 
	 * respective containers.
	 * 
	 * @param  {MapView}  that  The map view.
	 * @param  {Array}    map   A 2D array of map data.
	 * @param  {Integer}  row   The root row of the map data.
	 * @param  {Integer}  col   The root column of the map data.
	 */
	function generateMeshAt(that, map, row, col)
	{
		var name = (row / CHUNK_SIZE) + '_' + (col / CHUNK_SIZE);

		removeChunk(that, name);
		createChunk(that, map, row, col, name);
	}

	/**
	 * Removes a chunk with the specified name.
	 * 
	 * @param  {MapView}  that  The map view.
	 * @param  {String}   name  The name of the chunk to remove.
	 */
	function removeChunk(that, name)
	{
		var obj = that._objects.getObjectByName(name);

		if (obj !== undefined)
		{
			that._objects.remove(obj);
		}

		for (var l in BlockViewLayer)
		{
			var layer = BlockViewLayer[ l ];
			var mesh = that._meshList[ layer ];
			obj = mesh.getObjectByName(name);

			if (obj !== undefined)
			{
				mesh.remove(obj);
				obj.geometry.dispose();
			}
		}
	}

	/**
	 * Creates a chunk for a section of the map data. The mesh is positioned according to the 
	 * position of the chunk in the map data. No special transformations are applied to the geometry.
	 *
	 * @param  {MapView}  that  The map view to build on.
	 * @param  {Array}    map   A 2D array of map data.
	 * @param  {Integer}  row   The root row of the map data.
	 * @param  {Integer}  col   The root column of the map data.
	 */
	function createChunk(that, map, row, col, name)
	{
		var geometryList = generateGeometry(that, map, row, col);
		var chunkMaterial = (USE_CHUNK_MATERIAL) ? getChunkMaterial() : null;
		var mesh, meshList = [];

		var x = col + (CHUNK_SIZE / 2) - (Region.SIZE / 2);
		var z = row + (CHUNK_SIZE / 2) - (Region.SIZE / 2);

		for (var l in BlockViewLayer)
		{
			var layer = BlockViewLayer[ l ];

			var geo = geometryList[ layer ];
			var mat = (USE_CHUNK_MATERIAL) ? chunkMaterial : Resources.materialList[ layer ];
			
			geo.mergeVertices();
			geo.computeVertexNormals();
			geo.computeBoundingBox();
			geo.dynamic = false;

			mesh = new THREE.Mesh(geo, mat);
			mesh.name = name;
			mesh.position.x = x;
			mesh.position.z = z;
			mesh.updateMatrix();

			meshList[ layer ] = mesh;

			that._meshList[ layer ].add(mesh);
			that._meshList[ layer ].updateMatrixWorld();
		}

		mesh = meshList[ BlockViewLayer.WALL ];
		vertexShifter.generateNoise(mesh.geometry, mesh.matrixWorld, that.seed, [ BlockType.ROCK, BlockType.DIRT ]);

		mesh = meshList[ BlockViewLayer.CEILING ];
		vertexShifter.correctGeometry(mesh.geometry, mesh.matrixWorld);
		
		var objectData = map[ RegionLayer.OBJECT ];

		var container = new THREE.Object3D();
		container.name = name;
		container.position.x = x;
		container.position.z = z;
		container.updateMatrix();
		that._objects.add(container);

		for (var i = row; i < row + CHUNK_SIZE; i++)
		{
			for (var j = col; j < col + CHUNK_SIZE; j++)
			{
				var objectType = objectData[ i ][ j ];

				if (objectType)
				{
					var instance = new objectType();

					var bz = (i - row) + (1 / 2) - (CHUNK_SIZE / 2);
					var bx = (j - col) + (1 / 2) - (CHUNK_SIZE / 2);
					var transMatrix = new THREE.Matrix4().makeTranslation(bx, 0, bz);

					instance.applyMatrix(transMatrix);
					container.add(instance);
				}
			}
		}
	}

	/**
	 * Creates a new chunk material.
	 * 
	 * @return {THREE.Material}
	 */
	function getChunkMaterial()
	{
		var material = new THREE.MeshBasicMaterial({
			wireframe: WIREFRAME_CHUNKS,
			vertexColors: THREE.VertexColors,
		});

		if (COLORIZE_CHUNKS)
			material.color.setHex(Math.random() * 0xffffff);

		return material;
	}

	/**
	 * Generates geometry for each layer using a section of the map data.
	 * 
	 * @param  {MapView}         that  The map view to build on.
	 * @param  {Array}           map   A 2D array of map data.
	 * @param  {Integer}         row   The root row of the map data.
	 * @param  {Integer}         col   The root column of the map data.
	 * @return {THREE.Geometry}        The array of geometry for each layer.
	 */
	function generateGeometry(that, map, row, col)
	{
		var geometryList = [];

		// Initialize geometry for each layer
		for (var l in BlockViewLayer)
		{
			var layer = BlockViewLayer[ l ];
			geometryList[ layer ] = new THREE.Geometry();
		}

		// Build geometry block by block
		for (var i = row; i < row + CHUNK_SIZE; i++)
		{
			for (var j = col; j < col + CHUNK_SIZE; j++)
			{
				var bz = (i - row) + (1 / 2) - (CHUNK_SIZE / 2);
				var bx = (j - col) + (1 / 2) - (CHUNK_SIZE / 2);

				var transMatrix = new THREE.Matrix4().makeTranslation(bx, 0, bz);

				var block = BlockView.generate(map, i, j);

				for (var l in BlockViewLayer)
				{
					var layer = BlockViewLayer[ l ];
					var blockMatrix = block.matrix[ layer ];
					var blockGeometry = block.geometry[ layer ];

					if (blockMatrix !== undefined)
					{
						blockMatrix.multiplyMatrices(transMatrix, blockMatrix);
					}

					if (blockGeometry !== undefined)
					{
						var matrix = (blockMatrix !== undefined) ? blockMatrix : transMatrix;
						geometryList[ layer ].merge(blockGeometry, matrix);
					}
				}
			}
		}

		// Set the boundary layer as a copy of the wall layer
		geometryList[ BlockViewLayer.BOUNDARY ] = geometryList[ BlockViewLayer.WALL ].clone();

		return geometryList;
	}

	/**
	 * Removes all shared vertices with a position outside the defined area.
	 * This function is for memory management.
	 * 
	 * @param  {Number}  z         The center Z position.
	 * @param  {Number}  x         The center X position.
	 * @param  {Number}  distance  The distance from the center.
	 */
	MapView.cleanSharedVertices = function(z, x, distance, container)
	{
		vertexShifter.clean(z, x, distance, container);
	};

	MapView.prototype = {

		constructor: MapView,

		/**
		 * Partially updates the map view in a particular direction. Only chunks in the far row direction or far column
		 * direction are updated. If the given direction is the same as the last direction then no updates occur.
		 * 
		 * @param  {Array}    map     A 2D array of map data.
		 * @param  {Integer}  rowDir  The row direction to update. {-1, 0, 1}
		 * @param  {Integer}  colDir  The column direction to update. {-1, 0, 1}
		 * @return {Boolean}          True if changes were made to the view, false otherwise.
		 */
		updateToward: function(map, rowDir, colDir)
		{
			var updated = (rowDir == this._lastRowDir && colDir == this._lastColDir);
			var noDir = (rowDir == 0 && colDir == 0);

			if (updated || noDir)
				return false;

			for (var i = 0; i < DIVISIONS; i++)
			{
				for (var j = 0; j < DIVISIONS; j++)
				{
					var a = rowDir > 0 && i == DIVISIONS - 1;
					var b = rowDir < 0 && i == 0;
					var c = colDir > 0 && j == DIVISIONS - 1;
					var d = colDir < 0 && j == 0;

					if (a || b || c || d)
					{
						var row = i * CHUNK_SIZE;
						var col = j * CHUNK_SIZE;

						generateMeshAt(this, map, row, col);
					}
				}
			}

			// Set the last direction that was updated
			this._lastRowDir = rowDir;
			this._lastColDir = colDir;

			return true;
		},

		/**
		 * Sets the last direction that the map view was updated in. If the map view is updated in this direction
		 * then no changes will be made. 
		 * 
		 * @param  {Integer}  rowDir  The row direction.
		 * @param  {Integer}  colDir  The column direction.
		 */
		setLastDirection: function(rowDir, colDir)
		{
			this._lastRowDir = rowDir;
			this._lastColDir = colDir;
		},

		/**
		 * Gets a reference to the mesh representing the boundary.
		 * 
		 * @return  {THREE.Mesh}  The boundary mesh.
		 */
		getBoundary: function()
		{
			return this._meshList[ BlockViewLayer.BOUNDARY ];
		},

		/**
		 * Creates a clone of the ceiling geometry. Essentially a 2D representation of the 
		 * map view structure.
		 *
		 * @param   {THREE.Material}  material  The material to apply to the cloned ceiling.
		 * @return  {THREE.Object3D}            A container with the cloned ceiling mesh.
		 */
		cloneCeiling: function(material)
		{
			var ceiling = this._meshList[ BlockViewLayer.CEILING ];
			var root = new THREE.Object3D();

			root.scale.set(World.SCALE, World.SCALE, World.SCALE);
			root.rotation.x = Math.PI / 2;

			for (var i = 0; i < ceiling.children.length; i++)
			{
				var child = ceiling.children[i];
				var mesh = new THREE.Mesh(child.geometry.clone(), material);

				mesh.position.copy(child.position);
				root.add(mesh);
			}

			return root;
		},

		/**
		 * Removes the root from its parent and disposes all geometry.
		 */
		destruct: function()
		{
			if (this.root.parent !== undefined)
				this.root.parent.remove(this.root);

			for (var l in BlockViewLayer)
			{
				var layer = BlockViewLayer[ l ];
				var mesh = this._meshList[ layer ];

				for (var i = 0; i < mesh.children.length; i++)
				{
					mesh.children[i].geometry.dispose();
				}
			}
		},
	};

	return MapView;

})();

/**
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Object that provides a function to load all resources and 
 *                  store them as properties.
 */

Resources = {};

Resources.load = function(callback)
{
	var files = [
		'img/rock_wall.jpg',
		'img/rock_floor.jpg',
		'img/dirt_wall.jpg',
		'img/stone_wall.jpg',
		'img/stone_ceil.jpg',
		'img/stone_floor.jpg'
	];

	var textures = {};
	var loader = new THREE.TextureLoader();
	var count = 0;

	files.forEach(function(file) {

		loader.load(file, function(texture) {

			textures[ file ] = texture;
			count++;

			if (count == files.length)
				initAssets();
		});
	});

	function initAssets()
	{
		var WIDTH_SEGMENTS = 3;
		var HEIGHT_SEGMENTS = 5;

		Resources.sideCeiling = new BasicSideCeiling();
		Resources.fullCeiling = new BasicPlane().colorAll();
		Resources.basicPlane = new BasicPlane();

		Resources.wallList = [];

		// ROCK WALL GEOMETRY
		Resources.wallList[ BlockType.ROCK ] = {
			tip:    new BasicTipWall(1, BlockView.HEIGHT_SCALE, Math.ceil(WIDTH_SEGMENTS / 2), HEIGHT_SEGMENTS),
			corner: new BasicCornerWall(1, BlockView.HEIGHT_SCALE , WIDTH_SEGMENTS, HEIGHT_SEGMENTS),
			side:   new BasicSideWall(1, BlockView.HEIGHT_SCALE, WIDTH_SEGMENTS, HEIGHT_SEGMENTS),
		};

		// DIRT WALL GEOMETRY
		Resources.wallList[ BlockType.DIRT ] = {
			tip:    Resources.wallList[ BlockType.ROCK ].tip,
			corner: Resources.wallList[ BlockType.ROCK ].corner,
			side:   Resources.wallList[ BlockType.ROCK ].side,
		};

		// STONE WALL GEOMETRY
		Resources.wallList[ BlockType.STONE ] = {
			tip:    new StoneTipWall(1, BlockView.HEIGHT_SCALE, HEIGHT_SEGMENTS),
			corner: new StoneCornerWall(1, BlockView.HEIGHT_SCALE, HEIGHT_SEGMENTS),
			side:   new BasicSideWall(1, BlockView.HEIGHT_SCALE, 1, HEIGHT_SEGMENTS),
		};

		Resources.ceilingList = [];

		// ROCK CEILING GEOMETRY
		Resources.ceilingList[ BlockType.ROCK ] = {
			tip:    new BasicTipCeiling(Math.ceil(WIDTH_SEGMENTS / 2)),
			corner: new BasicCornerCeiling(WIDTH_SEGMENTS),
			side:   new BasicSideCeiling(WIDTH_SEGMENTS),
		};

		// DIRT CEILING GEOMETRY
		Resources.ceilingList[ BlockType.DIRT ] = {
			tip:    Resources.ceilingList[ BlockType.ROCK ].tip,
			corner: Resources.ceilingList[ BlockType.ROCK ].corner,
			side:   Resources.ceilingList[ BlockType.ROCK ].side,
		};

		// STONE CEILING GEOMETRY
		Resources.ceilingList[ BlockType.STONE ] = {
			tip:    new StoneTipCeiling(),
			corner: new StoneCornerCeiling(),
			side:   new BasicSideCeiling(),
		};

		// ROCK MATERIAL
		var rock = {};

		var rockTexture = textures['img/rock_wall.jpg'].clone();
		rockTexture.wrapS = rockTexture.wrapT = THREE.RepeatWrapping;
		rockTexture.repeat.set(1, BlockView.HEIGHT_SCALE);
		rockTexture.needsUpdate = true;

		rock.wall = new THREE.MeshLambertMaterial({
			map:          rockTexture,
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.SmoothShading,
		});

		rock.ceil = new THREE.MeshLambertMaterial({
			map:          textures['img/rock_wall.jpg'],
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.SmoothShading,
		});

		rock.floor = new THREE.MeshLambertMaterial({
			map:     textures['img/rock_floor.jpg'],
			ambient: 0x999999,
			color:   0x8A8A70,
		});
		rock.floor.rowRepeat = 3;
		rock.floor.colRepeat = 3;

		// DIRT MATERIAL
		var dirt = {};

		var dirtTexture = textures['img/dirt_wall.jpg'].clone();
		dirtTexture.wrapS = dirtTexture.wrapT = THREE.RepeatWrapping;
		dirtTexture.repeat.set(1, BlockView.HEIGHT_SCALE);
		dirtTexture.needsUpdate = true;

		dirt.wall = new THREE.MeshLambertMaterial({
			map:          dirtTexture,
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.SmoothShading,
		});

		dirt.ceil = new THREE.MeshLambertMaterial({
			map:          textures['img/dirt_wall.jpg'],
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.SmoothShading,
		});

		// STONE MATERIAL
		var stone = {};
		
		var stoneTexture = textures['img/stone_wall.jpg'].clone();
		stoneTexture.wrapS = stoneTexture.wrapT = THREE.RepeatWrapping;
		stoneTexture.repeat.set(1, BlockView.HEIGHT_SCALE);
		stoneTexture.needsUpdate = true;

		stone.wall = new THREE.MeshLambertMaterial({
			map:          stoneTexture,
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.FlatShading,
		});

		stone.ceil = new THREE.MeshLambertMaterial({
			map:          textures['img/stone_ceil.jpg'],
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.FlatShading,
		});

		stone.floor = new THREE.MeshLambertMaterial({
			map:     textures['img/stone_floor.jpg'],
			ambient: 0x888888,
			color:   0x8A8A70,
		});
		stone.floor.rowRepeat = 1;
		stone.floor.colRepeat = 1;

		Resources.materialList = [];

		// WATER MATERIAL
		Resources.materialList[ BlockViewLayer.WATER ] = new THREE.MeshLambertMaterial({
			color:       0x0000dd,
			ambient:     0x999999,
			transparent: true,
			opacity:     0.25,
		});

		var list;

		// FLOOR MATERIAL LIST
		list = [];
		list[ BlockType.ROCK ] = rock.floor;
		list[ BlockType.DIRT ] = rock.floor;
		list[ BlockType.STONE ] = stone.floor;

		Resources.materialList[ BlockViewLayer.FLOOR ] = new THREE.MeshFaceMaterial(list);

		// WALL MATERIAL LIST
		list = [];
		list[ BlockType.ROCK ] = rock.wall;
		list[ BlockType.DIRT ] = dirt.wall;
		list[ BlockType.STONE ] = stone.wall;

		Resources.materialList[ BlockViewLayer.WALL ] = new THREE.MeshFaceMaterial(list);

		// CEILING MATERIAL LIST
		list = [];
		list[ BlockType.ROCK ] = rock.ceil;
		list[ BlockType.DIRT ] = dirt.ceil;
		list[ BlockType.STONE ] = stone.ceil;

		Resources.materialList[ BlockViewLayer.CEILING ] = new THREE.MeshFaceMaterial(list);

		callback();
	}
};

!function(a,b,c,d,e,f,g,h,i){function j(a){var b,c=a.length,e=this,f=0,g=e.i=e.j=0,h=e.S=[];for(c||(a=[c++]);d>f;)h[f]=f++;for(f=0;d>f;f++)h[f]=h[g=r&g+a[f%c]+(b=h[f])],h[g]=b;(e.g=function(a){for(var b,c=0,f=e.i,g=e.j,h=e.S;a--;)b=h[f=r&f+1],c=c*d+h[r&(h[f]=h[g=r&g+b])+(h[g]=b)];return e.i=f,e.j=g,c})(d)}function k(a,b){var c,d=[],e=typeof a;if(b&&"object"==e)for(c in a)try{d.push(k(a[c],b-1))}catch(f){}return d.length?d:"string"==e?a:a+"\0"}function l(a,b){for(var c,d=a+"",e=0;e<d.length;)b[r&e]=r&(c^=19*b[r&e])+d.charCodeAt(e++);return n(b)}function m(c){try{return a.crypto.getRandomValues(c=new Uint8Array(d)),n(c)}catch(e){return[+new Date,a,(c=a.navigator)&&c.plugins,a.screen,n(b)]}}function n(a){return String.fromCharCode.apply(0,a)}var o=c.pow(d,e),p=c.pow(2,f),q=2*p,r=d-1,s=c["seed"+i]=function(a,f,g){var h=[];f=1==f?{entropy:!0}:f||{};var r=l(k(f.entropy?[a,n(b)]:null==a?m():a,3),h),s=new j(h);return l(n(s.S),b),(f.pass||g||function(a,b,d){return d?(c[i]=a,b):a})(function(){for(var a=s.g(e),b=o,c=0;p>a;)a=(a+c)*d,b*=d,c=s.g(1);for(;a>=q;)a/=2,b/=2,c>>>=1;return(a+c)/b},r,"global"in f?f.global:this==c)};l(c[i](),b),g&&g.exports?g.exports=s:h&&h.amd&&h(function(){return s})}(this,[],Math,256,6,52,"object"==typeof module&&module,"function"==typeof define&&define,"random");

/**
 * @class           World
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     Stores a 2D array of regions and keeps track which of regions are generated.
 */

World.prototype.constructor = World;

World.SCALE = 2;

function World()
{
	var regionList = [];
	var current = { row : 0, col : 0 };

	regionList[ current.row ] = [];
	regionList[ current.row ][ current.col ] = new CaveRegion();

	initializeArea(current.row, current.col);
	generateArea(current.row, current.col);

	function initializeArea(row, col)
	{
		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[d];
			var r = row + dir.row;
			var c = col + dir.col;
			
			if (regionList[r] == null)
				regionList[r] = [];

			if (regionList[r][c] == null)
				regionList[r][c] = createRegion();

			regionList[r][c].initialize();
		}
	}

	function createRegion()
	{
		var region = new SolidRegion();

		if (Math.random() < 0.95)
		{
			region = (Math.random() < 0.1) ? new DungeonRegion() : new CaveRegion();
		}
		
		return region;
	}

	function generateArea(row, col)
	{
		Region.generate(regionList, row, col);
	}

	function clearArea(row, col)
	{
		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[d];
			regionList[ row + dir.row ][ col + dir.col ].clear();
		}
	}

	function isRegionAvailable(row, col)
	{
		return (regionList[ row ] != null && regionList[ row ][ col ] != null && regionList[ row ][ col ].data != null);
	}

	this.move = function(rowDir, colDir)
	{
		clearArea(current.row, current.col);
		current.row += rowDir;
		current.col += colDir;
		initializeArea(current.row, current.col);
		generateArea(current.row, current.col);
	};

	this.moveNorth = function()
	{
		this.move(-1, 0);
	};

	this.moveSouth = function()
	{
		this.move(1, 0);
	};

	this.moveEast = function()
	{
		this.move(0, 1);
	};

	this.moveWest = function()
	{
		this.move(0, -1);
	};

	this.getCurrentRegionIndex = function()
	{
		return { row : current.row, col : current.col };
	};

	this.getRegionIndex = function(z, x)
	{
		var regLength = Region.SIZE * World.SCALE;
		var regRow = Math.floor((z + (regLength / 2)) / regLength);
		var regCol = Math.floor((x + (regLength / 2)) / regLength);
		return { row : regRow, col : regCol };
	};

	this.getRegionPosition = function(regRow, regCol)
	{
		var z = regRow * Region.SIZE * World.SCALE;
		var x = regCol * Region.SIZE * World.SCALE;
		return { z : z, x : x };
	};

	this.getCellIndex = function(z, x)
	{
		// TO DO
	};

	this.getCellPosition = function(regRow, regCol, cellRow, cellCol)
	{
		var regLength = Region.SIZE * World.SCALE;
		var z = (regRow * regLength) + (cellRow * World.SCALE) + (World.SCALE / 2) - (regLength / 2);
		var x = (regCol * regLength) + (cellCol * World.SCALE) + (World.SCALE / 2) - (regLength / 2);
		return { z : z, x : x };
	};

	this.getCellValue = function(regRow, regCol, cellIndex)
	{
		return regionList[ regRow ][ regCol ].getCellValue(cellIndex);
	};

	this.getRandomFloorPosition = function(regRow, regCol)
	{
		var layer = regionList[ regRow ][ regCol ].data[ RegionLayer.FLOOR ];
		var position = Generator.getRandomPosition(layer, true);
		return (position == null) ? null : this.getCellPosition(regRow, regCol, position.row, position.col);
	};

	this.getRegionData = function(row, col)
	{
		var copy = [];

		copy[ RegionLayer.BLOCK ] = regionList[ row ][ col ].data[ RegionLayer.BLOCK ];
		copy[ RegionLayer.FLOOR ] = Generator.getPaddedMap(regionList, row, col, RegionLayer.FLOOR, false);
		copy[ RegionLayer.WATER ] = regionList[ row ][ col ].data[ RegionLayer.WATER ];
		copy[ RegionLayer.OBJECT ] = regionList[ row ][ col ].data[ RegionLayer.OBJECT ];

		return copy;
	};

	this.getRegionSeed = function(row, col)
	{
		return regionList[ row ][ col ].seeds[ BlockType.NONE ];
	};
}

/**
 * @class           WorldManager
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     Singleton that manages the model and view for the world, and triggers 
 *                  world movement based on the movement of a target position.
 */
WorldManager = (function() {

	var instance;

	function init()
	{
		var container = new THREE.Object3D();
		var target = new THREE.Vector3();

		var world = new World();
		var worldView = new WorldView(container, world);

		var threshold = World.SCALE * 6;

		function checkBounds()
		{
			var regIndex = world.getCurrentRegionIndex();
			var regPos = world.getRegionPosition(regIndex.row, regIndex.col);
			var regLength = Region.SIZE * World.SCALE;
			
			var northBoundHit = target.z < regPos.z - (regLength / 2) - threshold;
			var southBoundHit = target.z > regPos.z + (regLength / 2) + threshold;
			var eastBoundHit = target.x > regPos.x + (regLength / 2) + threshold;
			var westBoundHit = target.x < regPos.x - (regLength / 2) - threshold;

			if (northBoundHit || southBoundHit || eastBoundHit || westBoundHit) {

				var targetRegIndex = world.getRegionIndex(target.z, target.x);

				var rowDir = targetRegIndex.row - regIndex.row;
				var colDir = targetRegIndex.col - regIndex.col;

				world.move(rowDir, colDir);
				worldView.move(rowDir, colDir);
				worldView.clean();
			}
		}

		return {

			update: function() {

				checkBounds();
				worldView.update();
			},

			addMoveable: function(moveable) {

				worldView.addMoveable(moveable);
			},

			setTarget: function(pos) {

				target = pos;
			},

			setMiniWorldMaterial: function(material) {

				worldView.setMiniWorldMaterial(material);
			},

			getContainer: function() {

				return container;
			},

			getMiniWorld: function() {

				return worldView.getMiniWorld();
			},

			getLocalPosition: function(x, z) {

				var regIndex = world.getCurrentRegionIndex();
				var pos = world.getRegionPosition(regIndex.row, regIndex.col);
				return { x : x - pos.x, z : z - pos.z };
			},

			getRegionPosition: function(regRow, regCol) {

				return world.getRegionPosition(regRow, regCol);
			},

			getCellPosition: function(regRow, regCol, cellRow, cellCol) {

				return world.getCellPosition(regRow, regCol, cellRow, cellCol);
			},

			getRandomFloorPosition: function(regRow, regCol) {

				return world.getRandomFloorPosition(regRow, regCol);
			},
		};
	}

	return {

		getInstance: function() {

			if (!instance) {

				instance = init();
			}

			return instance;
		}
	};

})();

/**
 * @class           WorldView
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     Manages the generation of region views and maintains a mini world for navigation.
 */

WorldView.prototype.constructor = WorldView;

function WorldView(container, world)
{
	var tickCount = 0;
	var updateInterval = 6;
	var updateQueue = [];
	var moveableList = [];

	var usingMiniWorld = true;

	var miniWorldMaterial = BlockView.ceilingMaterial; //.clone(); ?
	var miniWorld = new THREE.Object3D();
	
	var mapView = new Array(3);
	var miniMapView = new Array(3);

	for (var i = -1; i <= 1; i++)
	{
		mapView[i] = new Array(3);
		miniMapView[i] = new Array(3);
		for (var j = -1; j <= 1; j++)
		{
			updateMap(i, j);

			if (usingMiniWorld)
				updateMiniMap(i, j);
		}
	}

	function updateMap(row, col, rowDir, colDir)
	{
		var mapIndex = world.getCurrentRegionIndex();
		var map = world.getRegionData(mapIndex.row + row, mapIndex.col + col);

		if (mapView[row][col] != null)
		{
			// Partially update the map view
			rowDir = rowDir || 0;
			colDir = colDir || 0;
			return mapView[row][col].updateToward(map, rowDir, colDir);
		}
		else
		{
			// Create a new map view
			var pos = world.getRegionPosition(mapIndex.row + row, mapIndex.col + col);
			var seed = world.getRegionSeed(mapIndex.row + row, mapIndex.col + col);

			mapView[row][col] = new MapView(map, pos.z, pos.x, seed);
			container.add(mapView[row][col].root);

			var o = mapView[row][col].getBoundary();
			for (var i = 0; i < moveableList.length; i++)
			{
				moveableList[i].addObstacle(o);
			}
			return true;
		}
	}

	function updateMiniMap(row, col)
	{
		if (mapView[row][col] !== null)
		{
			if (miniMapView[row][col] !== null)
			{
				miniWorld.remove(miniMapView[row][col]);
				// miniMapView[row][col].geometry.dispose();
			}

			miniMapView[row][col] = mapView[row][col].cloneCeiling(miniWorldMaterial);

			updateMiniMapPosition(row, col);
			miniWorld.add(miniMapView[row][col]);
			// geometry.uvsNeedUpdate = true;
			// geometry.normalsNeedUpdate = true;
		}
	}

	function updateMiniMapPosition(row, col)
	{
		if (miniMapView[row][col] !== null)
		{
			var x = col * Region.SIZE * World.SCALE;
			var y = row * Region.SIZE * World.SCALE;
			miniMapView[row][col].position.set(x, -y, 0);
		}
	}

	function addToUpdateQueue(row, col, rowDir, colDir)
	{
		var rowDir = rowDir || 0;
		var colDir = colDir || 0;

		for (var i = 0; i < updateQueue.length; i++)
		{
			if (updateQueue[i].row == row && updateQueue[i].col == col)
			{
				updateQueue[i].rowDir += rowDir;
				updateQueue[i].colDir += colDir;
				return;
			}
		}
		updateQueue.push({
			row : row,
			col : col,
			rowDir : rowDir,
			colDir : colDir,
		});
	}

	function moveToward(rowDir, colDir)
	{
		rowDir = Math.sign(rowDir);
		colDir = Math.sign(colDir);

		for (var i = -1; i <= 1; i++)
		{
			// Destruct map in backward direction
			var br = (rowDir == 0) ? i : -rowDir;   // back row index
			var bc = (colDir == 0) ? i : -colDir;   // back column index
			if (mapView[br][bc] != null)
			{
				var o = mapView[br][bc].getBoundary();
				for (var n = 0; n < moveableList.length; n++)
				{
					moveableList[n].removeObstacle(o);
				}
				mapView[br][bc].destruct();
			}

			// Set map in the backward direction to the current middle map
			var mr = (rowDir == 0) ? i : 0;         // middle row index
			var mc = (colDir == 0) ? i : 0;         // middle column index
			mapView[br][bc] = mapView[mr][mc];

			// Set the direction that the current middle map was last updated
			if (mapView[mr][mc] != null)
			{
				mapView[mr][mc].setLastDirection(-rowDir, -colDir);
			}

			// Set middle map to the current map in the forward direction
			var fr = (rowDir == 0) ? i : rowDir;    // front row index
			var fc = (colDir == 0) ? i : colDir;    // front column index
			mapView[mr][mc] = mapView[fr][fc];

			// Clear map in the forward direction
			mapView[fr][fc] = null;

			// Apply the same procedure to the mini world
			if (usingMiniWorld)
			{
				if (miniMapView[br][bc] != null)
				{
					miniWorld.remove(miniMapView[br][bc]);
					// miniMapView[br][bc].geometry.dispose();
				}

				miniMapView[br][bc] = miniMapView[mr][mc];
				updateMiniMapPosition(br, bc);

				miniMapView[mr][mc] = miniMapView[fr][fc];
				updateMiniMapPosition(mr, mc);

				miniMapView[fr][fc] = null;
			}

			// Add middle map to the update queue
			addToUpdateQueue(mr, mc, rowDir, colDir);

			// Add forward map to the update queue
			addToUpdateQueue(fr, fc);
		}
	}

	this.addMoveable = function(moveable)
	{
		moveableList.push(moveable);
		for (var i = -1; i <= 1; i++)
		{
			for (var j = -1; j <= 1; j++)
			{
				if (mapView[i][j] != null)
				{
					var o = mapView[i][j].getBoundary();
					moveable.addObstacle(o);
				}
			}
		}
	};

	this.setMiniWorldMaterial = function(material)
	{
		miniWorldMaterial = material;

		if (usingMiniWorld)
		{
			for (var i = -1; i <= 1; i++)
			{
				for (var j = -1; j <= 1; j++)
				{
					updateMiniMap(i, j);
				}
			}
		}
	};

	this.getMiniWorld = function()
	{
		return miniWorld;
	};

	this.update = function()
	{
		if (updateQueue.length > 0)
		{
			tickCount ++;
			if (tickCount == updateInterval)
			{
				tickCount = 0;
				var event = updateQueue.shift();
				var hasChanges = updateMap(event.row, event.col, event.rowDir, event.colDir);

				if (usingMiniWorld && hasChanges)
				{
					updateMiniMap(event.row, event.col);
				}
			}
		}
	};

	this.clean = function()
	{
		var mapIndex = world.getCurrentRegionIndex();
		var pos = world.getRegionPosition(mapIndex.row, mapIndex.col);
		var distance = Region.SIZE * World.SCALE * 3 / 2;
		
		MapView.cleanSharedVertices(pos.z, pos.x, distance, container);
	};

	this.move = function(rowDir, colDir)
	{
		if (rowDir)
			moveToward(rowDir, 0);

		if (colDir)
			moveToward(0, colDir);
	};
}

/**
 * @class			BasicCornerCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
BasicCornerCeiling = (function() {

	var BLACK = new THREE.Color(0x000000);
	var CLEAR = new THREE.Color(0xffffff);

	function BasicCornerCeiling(segments)
	{
		THREE.Geometry.call(this);

		this.segments = segments || 1;

		var step = 1 / this.segments;

		// Add vertices
		for (var i = 0; i < 1; i += step)
		{
			this.vertices.push(new THREE.Vector3(i, i, 0));
		}
		this.vertices.push(new THREE.Vector3(1, 1, 0));
		this.vertices.push(new THREE.Vector3(0, 1, 0));

		var vl = this.vertices.length;
		
		// Add faces
		for (var i = 0; i < this.segments; i++)
		{
			this.faces.push(new THREE.Face3(i, i + 1, vl - 1));
		}

		this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, -0.5, 0));
		this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

		this.removeColor();
		this.computeFaceNormals();
		this.computeVertexNormals();

		BasicSideCeiling.computeVertexUvs(this);
	}

	BasicCornerCeiling.prototype = new THREE.Geometry();

	BasicCornerCeiling.prototype.constructor = BasicCornerCeiling;

	BasicCornerCeiling.prototype.colorCorner = function()
	{
		for (var i = 0; i < this.segments; i++)
		{
			this.faces[i].vertexColors[2] = BLACK;
		}
	};

	BasicCornerCeiling.prototype.removeColor = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = CLEAR;
			}
		}
	};

	return BasicCornerCeiling;

})();

/**
 * @class			BasicCornerWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

BasicCornerWall.prototype = new THREE.PlaneGeometry();

BasicCornerWall.prototype.constructor = BasicCornerWall;

function BasicCornerWall(size, height, widthSegments, heightSegments)
{
	THREE.PlaneGeometry.call(this, size * Math.sqrt(2), height, widthSegments, heightSegments);

	var axis = new THREE.Vector3(0, 1, 0);
	var angle = Math.PI / 4;
	var matrix = new THREE.Matrix4().makeRotationAxis(axis, angle);

	for (var i = 0; i < this.vertices.length; i++)
	{
		this.vertices[i].applyMatrix4(matrix);
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}

/**
 * @class			BasicPlane
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
BasicPlane = (function() {

	var BLACK = new THREE.Color(0x000000);

	function BasicPlane()
	{
		THREE.Geometry.call(this);

		// Add vertices
		this.vertices.push(new THREE.Vector3(0, 0, 0));
		this.vertices.push(new THREE.Vector3(1, 0, 1));
		this.vertices.push(new THREE.Vector3(0, 0, 1));
		this.vertices.push(new THREE.Vector3(1, 0, 0));

		// Add faces
		this.faces.push(new THREE.Face3(0, 2, 1));
		this.faces.push(new THREE.Face3(0, 1, 3));

		this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, 0, -0.5));

		this.computeFaceNormals();
		this.computeVertexNormals();
		this.removeColor();

		BasicSideCeiling.computeVertexUvs(this);
	}

	BasicPlane.prototype = Object.create(THREE.Geometry.prototype);

	BasicPlane.prototype.constructor = BasicPlane;

	BasicPlane.prototype.colorAll = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = BLACK;
			}
		}

		return this;
	};

	BasicPlane.prototype.removeColor = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = new THREE.Color(1, 1, 1);
			}
		}

		return this;
	};

	return BasicPlane;

})();

/**
 * @class			BasicSideCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
BasicSideCeiling = (function() {

	var BLACK = new THREE.Color(0x000000);
	var FADE = new THREE.Color(0x777777);
	var CLEAR = new THREE.Color(0xffffff);

	var FACE_INDICES = ['a','b','c'];

	var Z_AXIS = new THREE.Vector3(0, 0, -1);

	var OFFSET = 0.5;

	function BasicSideCeiling(segments)
	{
		THREE.Geometry.call(this);

		this.segments = segments || 1;

		var step = 1 / this.segments;

		// Add vertices
		for (var i = 0; i < 1; i += step)
		{
			this.vertices.push(new THREE.Vector3(i, 0, 0));
		}
		this.vertices.push(new THREE.Vector3(1, 0, 0));
		this.vertices.push(new THREE.Vector3(0.5, 0.5, 0));

		var vl = this.vertices.length;
		
		// Add faces
		for (var i = 0; i < this.segments; i++)
		{
			this.faces.push(new THREE.Face3(i, i + 1, vl - 1));
		}

		this.applyMatrix(new THREE.Matrix4().makeTranslation(-OFFSET, -OFFSET, 0));
		this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

		this.computeFaceNormals();
		this.computeVertexNormals();
		this.removeColor();

		BasicSideCeiling.computeVertexUvs(this);
	}

	BasicSideCeiling.prototype = Object.create(THREE.Geometry.prototype);

	BasicSideCeiling.prototype.constructor = BasicSideCeiling;

	BasicSideCeiling.computeVertexUvs = function(geometry, theta, row, col, rowRepeat, colRepeat)
	{
		theta = theta || 0;
		row = row || 0;
		col = col || 0;
		rowRepeat = rowRepeat || 1;
		colRepeat = colRepeat || 1;

		var c = col % colRepeat;
		var r = row % rowRepeat;

		for (var i = 0; i < geometry.faces.length; i++)
		{
			if (geometry.faceVertexUvs[0][i] === undefined)
				geometry.faceVertexUvs[0][i] = [ new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2() ];

			var uvs = geometry.faceVertexUvs[0][i];
			var face = geometry.faces[i];

			for (var j = 0; j < FACE_INDICES.length; j++)
			{
				var index = FACE_INDICES[j];
				var vertex = geometry.vertices[ face[index] ];

				var vec = new THREE.Vector3(vertex.x, vertex.z, 0).applyAxisAngle(Z_AXIS, theta);

				uvs[j].x = ((vec.x + OFFSET) / colRepeat) + (c / colRepeat);
				uvs[j].y = ((vec.y + OFFSET) / rowRepeat) + (r / rowRepeat);
			}
		}
	};

	BasicSideCeiling.colorNorthWest = function(geometry)
	{
		BasicSideCeiling.addGadient(geometry, -0.5, -0.5);
	};

	BasicSideCeiling.colorNorthEast = function(geometry)
	{
		BasicSideCeiling.addGadient(geometry, 0.5, -0.5);
	};

	BasicSideCeiling.colorSouthWest = function(geometry)
	{
		BasicSideCeiling.addGadient(geometry, -0.5, 0.5);
	};

	BasicSideCeiling.colorSouthEast = function(geometry)
	{
		BasicSideCeiling.addGadient(geometry, 0.5, 0.5);
	};

	BasicSideCeiling.addGadient = function(geometry, x, z)
	{
		for (var i = 0; i < geometry.faces.length; i++)
		{
			var face = geometry.faces[i];

			for (var j = 0; j < FACE_INDICES.length; j++)
			{
				var vertex = geometry.vertices[ face[ FACE_INDICES[j] ] ];
				var dx = Math.abs(vertex.x - x);
				var dz = Math.abs(vertex.z - z);
				var dist = Math.sqrt(dx * dx + dz * dz);
				var s = 1 - Math.min(dist, 1);

				face.vertexColors[j].addScalar(-s);
			}
		}
	};

	BasicSideCeiling.prototype.colorAll = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = BLACK;
			}
		}

		return this;
	};

	BasicSideCeiling.prototype.removeColor = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = new THREE.Color(1, 1, 1);
			}
		}

		return this;
	};

	return BasicSideCeiling;

})();

/**
 * @class			BasicSideWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

BasicSideWall.prototype = new THREE.PlaneGeometry();

BasicSideWall.prototype.constructor = BasicSideWall;

function BasicSideWall(size, height, widthSegments, heightSegments)
{
	THREE.PlaneGeometry.call(this, size, height, widthSegments, heightSegments);

	var matrix = new THREE.Matrix4().makeTranslation(0, 0, size / 2);

	for (var i = 0; i < this.vertices.length; i++)
	{
		this.vertices[i].applyMatrix4(matrix);
	}

	this.computeFaceNormals();
	this.computeVertexNormals();
}

/**
 * @class			BasicTipCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

BasicTipCeiling.prototype = new THREE.Geometry();

BasicTipCeiling.prototype.constructor = BasicTipCeiling;

function BasicTipCeiling(segments)
{
	THREE.Geometry.call(this);

	this.segments = segments || 1;

	var step = 1 / (this.segments * 2);

	// Add vertices
	for (var i = 0; i < 0.5; i += step)
	{
		this.vertices.push(new THREE.Vector3(i, 1 - i, 0));
		this.vertices.push(new THREE.Vector3(1 - i, 1 - i, 0));
	}
	this.vertices.push(new THREE.Vector3(0.5, 0.5, 0));

	var vl = this.vertices.length;
	
	// Add faces
	for (var i = 0; i <= this.segments - 2; i += 2)
	{
		this.faces.push(new THREE.Face3(i, i + 3, i + 1));
		this.faces.push(new THREE.Face3(i, i + 2, i + 3));
	}
	this.faces.push(new THREE.Face3(vl - 1, vl - 2, vl - 3));

	// Set vertex colors
	var clear = new THREE.Color(0xffffff);

	for (var i = 0; i < this.faces.length; i++)
	{
		for (var j = 0; j < 3; j++)
		{
			this.faces[i].vertexColors[j] = clear;
		}
	}

	this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, -0.5, 0));
	this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

	this.computeFaceNormals();
	this.computeVertexNormals();

	BasicSideCeiling.computeVertexUvs(this);
}

/**
 * @class			BasicTipWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

BasicTipWall.prototype = new THREE.PlaneGeometry();

BasicTipWall.prototype.constructor = BasicTipWall;

function BasicTipWall(size, height, widthSegments, heightSegments)
{
	THREE.PlaneGeometry.call(this, size * Math.sqrt(2), height, widthSegments * 2, heightSegments);

	var axis = new THREE.Vector3(0, 1, 0);
	var angle = Math.PI / 4;
	var matrix = new THREE.Matrix4().makeRotationAxis(axis, angle);

	for (var i = 0; i < this.vertices.length; i++)
	{
		if (this.vertices[i].x < 0)
		{
			var temp = -this.vertices[i].z;
			this.vertices[i].z = this.vertices[i].x;
			this.vertices[i].x = temp;
		}
		this.vertices[i].applyMatrix4(matrix);
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}

/**
 * @class			StoneCornerCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
StoneCornerCeiling = (function() {

	var BLACK = new THREE.Color(0x000000);
	var CLEAR = new THREE.Color(0xffffff);

	function StoneCornerCeiling()
	{
		THREE.Geometry.call(this);

		// Add vertices
		this.vertices.push(new THREE.Vector3(0, 0, 0));
		this.vertices.push(new THREE.Vector3(0, 1, 0));
		this.vertices.push(new THREE.Vector3(1, 1, 0));
		this.vertices.push(new THREE.Vector3(0.75, 0, 0));
		this.vertices.push(new THREE.Vector3(1, 0.25, 0));
		
		// Add faces
		this.faces.push(new THREE.Face3(1, 0, 3));
		this.faces.push(new THREE.Face3(4, 1, 3));
		this.faces.push(new THREE.Face3(4, 2, 1));

		this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, -0.5, 0));
		this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

		this.removeColor();
		this.computeFaceNormals();
		this.computeVertexNormals();

		BasicSideCeiling.computeVertexUvs(this);
	}

	StoneCornerCeiling.prototype = new THREE.Geometry();

	StoneCornerCeiling.prototype.constructor = StoneCornerCeiling;

	StoneCornerCeiling.prototype.colorCorner = function()
	{
		this.faces[0].vertexColors[0] = BLACK;
		this.faces[1].vertexColors[1] = BLACK;
		this.faces[2].vertexColors[2] = BLACK;
	};

	StoneCornerCeiling.prototype.removeColor = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = CLEAR;
			}
		}
	};

	return StoneCornerCeiling;

})();

/**
 * @class			StoneCornerWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

StoneCornerWall.prototype = new THREE.PlaneGeometry();

StoneCornerWall.prototype.constructor = StoneCornerWall;

function StoneCornerWall(size, height, heightSegments)
{
	var widthSegments = 4;
	THREE.PlaneGeometry.call(this, size, height, widthSegments, heightSegments);

	for (var i = 0; i < this.vertices.length; i += widthSegments + 1)
	{
		this.vertices[i].x = -0.5;
		this.vertices[i].z = 0.5;

		this.vertices[i + 1].x = 0.25;
		this.vertices[i + 1].z = 0.5;

		this.vertices[i + 2].x = 0.375;
		this.vertices[i + 2].z = 0.375;

		this.vertices[i + 3].x = 0.5;
		this.vertices[i + 3].z = 0.25;

		this.vertices[i + 4].x = 0.5;
		this.vertices[i + 4].z = -0.5;
	}

	var uvAdjustLeft = [0, 0.8, 1, 0.2, 1];
	var uvAdjustRight = [0, 0.8, 0, 0.2, 1];
	var step = 1 / widthSegments;

	for (var i = 0; i < this.faceVertexUvs[0].length; i++)
	{
		var uvs = this.faceVertexUvs[0][i];

		var isLeft = (i % (widthSegments * 2) < widthSegments);
		var uvAdjust = (isLeft) ? uvAdjustLeft : uvAdjustRight;

		uvs[0].x = uvAdjust[ Math.round(uvs[0].x / step) ];
		uvs[1].x = uvAdjust[ Math.round(uvs[1].x / step) ];
		uvs[2].x = uvAdjust[ Math.round(uvs[2].x / step) ];
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}

/**
 * @class			StoneTipCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

StoneTipCeiling.prototype = new THREE.Geometry();

StoneTipCeiling.prototype.constructor = StoneTipCeiling;

function StoneTipCeiling()
{
	THREE.Geometry.call(this);

	// Add vertices
	this.vertices.push(new THREE.Vector3(0, 1, 0));
	this.vertices.push(new THREE.Vector3(1, 1, 0));
	this.vertices.push(new THREE.Vector3(0.5, 1, 0));
	this.vertices.push(new THREE.Vector3(0, 0.25, 0));
	this.vertices.push(new THREE.Vector3(0.25, 0, 0));
	this.vertices.push(new THREE.Vector3(0.75, 0, 0));
	this.vertices.push(new THREE.Vector3(1, 0.25, 0));
	
	// Add faces
	this.faces.push(new THREE.Face3(2, 0, 3));
	this.faces.push(new THREE.Face3(2, 3, 4));
	this.faces.push(new THREE.Face3(2, 4, 5));
	this.faces.push(new THREE.Face3(2, 5, 6));
	this.faces.push(new THREE.Face3(2, 6, 1));

	// Set vertex colors
	var clear = new THREE.Color(0xffffff);

	for (var i = 0; i < this.faces.length; i++)
	{
		for (var j = 0; j < 3; j++)
		{
			this.faces[i].vertexColors[j] = clear;
		}
	}

	this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, -0.5, 0));
	this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

	this.computeFaceNormals();
	this.computeVertexNormals();

	BasicSideCeiling.computeVertexUvs(this);
}

/**
 * @class			StoneTipWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

StoneTipWall.prototype = new THREE.PlaneGeometry();

StoneTipWall.prototype.constructor = StoneTipWall;

function StoneTipWall(size, height, heightSegments)
{
	var widthSegments = 6;
	THREE.PlaneGeometry.call(this, size, height, widthSegments, heightSegments);

	for (var i = 0; i < this.vertices.length; i += widthSegments + 1)
	{
		this.vertices[i].x = size * -0.5;
		this.vertices[i].z = size * -0.5;

		this.vertices[i + 1].x = size * -0.5;
		this.vertices[i + 1].z = size * 0.25;

		this.vertices[i + 2].x = size * -0.25;
		this.vertices[i + 2].z = size * 0.5;

		this.vertices[i + 3].x = size * 0;
		this.vertices[i + 3].z = size * 0.5;

		this.vertices[i + 4].x = size * 0.25;
		this.vertices[i + 4].z = size * 0.5;

		this.vertices[i + 5].x = size * 0.5;
		this.vertices[i + 5].z = size * 0.25;

		this.vertices[i + 6].x = size * 0.5;
		this.vertices[i + 6].z = size * -0.5;
	}

	var uvAdjustLeft = [0, 0.56, 0.8, 1, 0.2, 0.44, 1];
	var uvAdjustRight = [0, 0.56, 0.8, 0, 0.2, 0.44, 1];
	var step = 1 / widthSegments;

	for (var i = 0; i < this.faceVertexUvs[0].length; i++)
	{
		var uvs = this.faceVertexUvs[0][i];

		var isLeft = (i % (widthSegments * 2) < widthSegments);
		var uvAdjust = (isLeft) ? uvAdjustLeft : uvAdjustRight;

		uvs[0].x = uvAdjust[ Math.round(uvs[0].x / step) ];
		uvs[1].x = uvAdjust[ Math.round(uvs[1].x / step) ];
		uvs[2].x = uvAdjust[ Math.round(uvs[2].x / step) ];
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}

/**
 * @class			CaveRegion
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Extension of Region that generates a cave environment.
 */
CaveRegion = (function() {

	var ROCK_DISTRIBUTION = 0.54;
	var DIRT_DISTRIBUTION = 0.50;

	var CA_BORN_THRESHOLD = 5;
	var CA_SURVIVE_THRESHOLD = 3;
	var CA_PASS_COUNT = 6;
	var CA_SMOOTH_PASS_COUNT = 5;
	var CA_SMOOTH_THRESHOLD = 5;

	/**
	 * Class Constructor
	 */
	function CaveRegion()
	{
		Region.call(this);
	}

	Region.addEventListener(RegionEvent.BEFORE_GENERATE, function(e) {

		// Generate rock layer
		prepareLayer(e.list, e.row, e.col, BlockType.ROCK, CA_PASS_COUNT, CA_BORN_THRESHOLD, CA_SURVIVE_THRESHOLD);
		prepareLayer(e.list, e.row, e.col, BlockType.ROCK, CA_SMOOTH_PASS_COUNT, CA_SMOOTH_THRESHOLD, CA_SMOOTH_THRESHOLD);

		// Generate dirt layer
		prepareLayer(e.list, e.row, e.col, BlockType.DIRT, CA_PASS_COUNT, CA_BORN_THRESHOLD, CA_SURVIVE_THRESHOLD);
		prepareLayer(e.list, e.row, e.col, BlockType.DIRT, CA_SMOOTH_PASS_COUNT, CA_SMOOTH_THRESHOLD, CA_SMOOTH_THRESHOLD);
	});

	Region.addEventListener(RegionEvent.AFTER_GENERATE, function(e) {

		addWaterSource(e.list, e.row, e.col);
	});

	/**
	 * [addWaterSource description]
	 * 
	 * @param  {[type]} regList [description]
	 * @param  {[type]} regRow  [description]
	 * @param  {[type]} regCol  [description]
	 * @return {[type]}         [description]
	 */
	function addWaterSource(regList, regRow, regCol)
	{
		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[d];
			var region = regList[ regRow + dir.row ][ regCol + dir.col ];

			if (region instanceof CaveRegion)
			{
				var layer = region.data[ RegionLayer.FLOOR ];
				var seed = region.seeds[ BlockType.ROCK ];
				var pos = Generator.getRandomPosition(layer, true, seed);

				region.data[ RegionLayer.OBJECT ][ pos.row ][ pos.col ] = WaterSource;

				WaterSource.generate(regList, regRow + dir.row, regCol + dir.col, pos.row, pos.col);
			}
		}
	}

	/**
	 * [prepareLayer description]
	 * 
	 * @param  {[type]} regList    [description]
	 * @param  {[type]} regRow     [description]
	 * @param  {[type]} regCol     [description]
	 * @param  {[type]} layerIndex [description]
	 * @param  {[type]} passCount  [description]
	 * @param  {[type]} born       [description]
	 * @param  {[type]} survive    [description]
	 */
	function prepareLayer(regList, regRow, regCol, layerIndex, passCount, born, survive)
	{
		for (var n = 0; n < passCount; n++)
		{
			var count = 0;
			var tempData = [];

			// Apply a single iteration to each data set in the neighborhood
			for (var d in Direction.NEIGHBORS)
			{
				var dir = Direction.NEIGHBORS[d];
				var currentReg = regList[ regRow + dir.row ][ regCol + dir.col ];

				if (currentReg instanceof CaveRegion)
				{
					var currentData = currentReg.layers[ layerIndex ];

					tempData[count] = Generator.applyCellAutomata(currentData, born, survive, {
						regList:    regList,
						regRow:     regRow + dir.row,
						regCol:     regCol + dir.col,
						layerIndex: layerIndex,
						dead:       layerIndex,
						live:       BlockType.NONE,
					});
					count ++;
				}
			}

			// Replace each data set in the neighborhood with the new data set
			count = 0;
			for (var d in Direction.NEIGHBORS)
			{
				var dir = Direction.NEIGHBORS[d];
				var currentReg = regList[ regRow + dir.row ][ regCol + dir.col ];

				if (currentReg instanceof CaveRegion)
				{
					currentReg.layers[ layerIndex ] = tempData[count];
					count ++;
				}
			}
		}
	}

	CaveRegion.prototype = Object.create(Region.prototype);

	CaveRegion.prototype.constructor = CaveRegion;

	CaveRegion.prototype.initialize = function()
	{
		Region.prototype.initialize.call(this);

		// Initialize rock layer
		this.layers[ BlockType.ROCK ] = Generator.getRandomData(this.length, ROCK_DISTRIBUTION, {
			seed: this.seeds[ BlockType.ROCK ],
			high: BlockType.ROCK,
			low:  BlockType.NONE,
		});

		// Initialize dirt layer
		this.layers[ BlockType.DIRT ] = Generator.getRandomData(this.length, DIRT_DISTRIBUTION, {
			seed: this.seeds[ BlockType.DIRT ],
			high: BlockType.DIRT,
			low:  BlockType.NONE,
		});
	};

	CaveRegion.prototype.exportLayers = function()
	{
		Region.prototype.exportLayers.call(this);

		for (var i = 0; i < this.length; i++)
		{
			for (var j = 0; j < this.length; j++)
			{
				var hasRock = (this.layers[ BlockType.ROCK ][i][j] == BlockType.ROCK);
				var hasDirt = (this.layers[ BlockType.DIRT ][i][j] == BlockType.DIRT);

				this.data[ RegionLayer.BLOCK ][i][j] = (hasRock && hasDirt) ? BlockType.DIRT : BlockType.ROCK;
				this.data[ RegionLayer.FLOOR ][i][j] = !hasRock;
				this.data[ RegionLayer.WATER ][i][j] = false;
				this.data[ RegionLayer.OBJECT ][i][j] = null;
			}
		}
	};

	return CaveRegion;

})();

/**
 * @class			DungeonRegion
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Extension of Region that generates a dungeon environment.
 */
DungeonRegion = (function() {

	var MAX_FLOOR_EXTEND = 8;

	/**
	 * Class Constructor
	 */
	function DungeonRegion()
	{
		Region.call(this);
	}
	
	Region.addEventListener(RegionEvent.AFTER_GENERATE, function(e) {

		extendFloor(e.list, e.row, e.col);
	});

	/**
	 * [extendFloor description]
	 * 
	 * @param  {[type]} regList [description]
	 * @param  {[type]} regRow  [description]
	 * @param  {[type]} regCol  [description]
	 * @return {[type]}         [description]
	 */
	function extendFloor(regList, regRow, regCol)
	{
		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[d];
			var rr = regRow + dir.row;
			var cc = regCol + dir.col;

			var currentReg = regList[rr][cc];
			var layer = currentReg.data[ RegionLayer.FLOOR ];
			var seed = currentReg.seeds[ BlockType.STONE ];

			if (currentReg instanceof DungeonRegion)
			{
				extend(Direction.CARDINALS.N);
				extend(Direction.CARDINALS.W);
				extend(Direction.CARDINALS.S);
				extend(Direction.CARDINALS.E);
			}
		}

		function extend(dir)
		{
			var pos = Generator.findFromEdge(layer, true, -dir.row, -dir.col, seed);
			var next, path = [];

			do
			{
				path.push({ row: pos.row, col: pos.col });
				pos.row += dir.row;
				pos.col += dir.col;
				next = Generator.getElement(regList, rr, cc, pos.row, pos.col, RegionLayer.FLOOR);

				if (next)
				{
					for (var n = 1; n < path.length; n++)
					{
						var p = path[n];

						// Set the element of the current path position
						Generator.setElement(regList, rr, cc, p.row, p.col, RegionLayer.FLOOR, true);
						Generator.setElement(regList, rr, cc, p.row, p.col, RegionLayer.BLOCK, BlockType.STONE);

						// Set the element orthogonal to the current path position (positive)
						Generator.setElement(regList, rr, cc, p.row + dir.col, p.col + dir.row, RegionLayer.FLOOR, false);
						Generator.setElement(regList, rr, cc, p.row + dir.col, p.col + dir.row, RegionLayer.BLOCK, BlockType.STONE);

						// Set the element orthogonal to the current path position (negative)
						Generator.setElement(regList, rr, cc, p.row - dir.col, p.col - dir.row, RegionLayer.FLOOR, false);
						Generator.setElement(regList, rr, cc, p.row - dir.col, p.col - dir.row, RegionLayer.BLOCK, BlockType.STONE);
					}
					break;
				}
			}
			while (next !== null && path.length < MAX_FLOOR_EXTEND);
		}
	}

	DungeonRegion.prototype = Object.create(Region.prototype);

	DungeonRegion.prototype.constructor = DungeonRegion;

	DungeonRegion.prototype.initialize = function()
	{
		Region.prototype.initialize.call(this);

		// Initialize stone layer
		this.layers[ BlockType.STONE ] = Generator.createDungeon(this.length, BlockType.STONE, BlockType.NONE, {
			seed:         this.seeds[ BlockType.STONE ],
			minRoomSize:  5,
			maxRoomSize:  11,
			roomAttempts: 10,
		});
	};

	DungeonRegion.prototype.exportLayers = function()
	{
		Region.prototype.exportLayers.call(this);

		for (var i = 0; i < this.length; i++)
		{
			for (var j = 0; j < this.length; j++)
			{
				var hasStone = (this.layers[ BlockType.STONE ][i][j] == BlockType.STONE);

				this.data[ RegionLayer.BLOCK ][i][j] = BlockType.STONE;
				this.data[ RegionLayer.FLOOR ][i][j] = !hasStone;
				this.data[ RegionLayer.WATER ][i][j] = false;
				this.data[ RegionLayer.OBJECT ][i][j] = null;
			}
		}
	};

	return DungeonRegion;

})();

/**
 * @class			SolidRegion
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Extension of Region that generates a solid rock environment.
 */
SolidRegion = (function() {

	/**
	 * Class Constructor
	 */
	function SolidRegion()
	{
		Region.call(this);
	}

	SolidRegion.prototype = Object.create(Region.prototype);

	SolidRegion.prototype.constructor = SolidRegion;

	SolidRegion.prototype.exportLayers = function()
	{
		Region.prototype.exportLayers.call(this);

		for (var i = 0; i < this.length; i++)
		{
			for (var j = 0; j < this.length; j++)
			{
				this.data[ RegionLayer.BLOCK ][i][j] = BlockType.ROCK;
				this.data[ RegionLayer.FLOOR ][i][j] = false;
				this.data[ RegionLayer.WATER ][i][j] = false;
				this.data[ RegionLayer.OBJECT ][i][j] = null;
			}
		}
	};

	return SolidRegion;

})();

/**
 * @class			WaterSource
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
WaterSource = (function() {

	var geometry = new THREE.SphereGeometry(0.25);
	var material = new THREE.MeshLambertMaterial({ color: 0x0000ff });

	/**
	 * Class Constructor
	 */
	function WaterSource()
	{
		EnvironmentObject.call(this);

		this.add(new THREE.Mesh(geometry, material));
	}

	WaterSource.generate = function(grid, outerRow, outerCol, innerRow, innerCol)
	{
		var region = grid[ outerRow ][ outerCol ];

		Generator.floodFill(null, innerRow, innerCol, true, true, {
			decay:        0.8,
			useCardinals: true,
			seed:         region.seeds[ BlockType.NONE ],
			regList:      grid,
			regRow:       outerRow,
			regCol:       outerCol,
			fromLayer:    RegionLayer.FLOOR,
			toLayer:      RegionLayer.WATER,
		});
	};

	WaterSource.prototype = Object.create(EnvironmentObject.prototype);

	WaterSource.prototype.constructor = WaterSource;

	/**
	 * Updates the water source for the next frame.
	 */
	WaterSource.prototype.update = function()
	{
		EnvironmentObject.prototype.update.call(this);
	};

	return WaterSource;

})();

/**
 * @class           HeadsUpDisplay
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     
 */
HeadsUpDisplay = (function() {

	/**
	 * Class Constructor
	 * 
	 * @param {THREE.WebGLRenderer}  renderer  The renderer to use for the HUD.
	 */
	function HeadsUpDisplay(renderer)
	{
		this.renderer = renderer;

		this.camera = new THREE.OrthographicCamera(-1, 1, 1, -1, 1, 10);
		this.camera.position.z = 2;

		this.scene = new THREE.Scene();
		this.scene.add(this.camera);

		this.resize();

		window.addEventListener('resize', this.resize.bind(this), false);
	}

	HeadsUpDisplay.prototype = {

		constructor: HeadsUpDisplay,

		/**
		 * Resizes the camera bounds and scales the position of the objects in the scene.
		 */
		resize: function()
		{
			var oldWidth = this.camera.right - this.camera.left;
			var oldHeight = this.camera.top - this.camera.bottom;

			var width = window.innerWidth / 2;
			var height = window.innerHeight / 2;

			this.camera.left = -width;
			this.camera.right = width;
			this.camera.top = height;
			this.camera.bottom = -height;

			this.camera.updateProjectionMatrix();

			for (var i = 0; i < this.scene.children.length; i++)
			{
				var child = this.scene.children[ i ];

				if (child instanceof THREE.Camera)
					continue;

				child.position.x *= window.innerWidth / oldWidth;
				child.position.y *= window.innerHeight / oldHeight;
			}
		},

		/**
		 * Renders the scene without clearing the renderer.
		 */
		render: function()
		{
			this.renderer.autoClear = false;
			this.renderer.render(this.scene, this.camera);
			this.renderer.autoClear = true;
		},
	};

	return HeadsUpDisplay;

})();

/**
 * @class			Radar
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Represents a minified version of the world.
 */
Radar = (function() {

	/**
	 * @constant {Number}	The radius of the radar center mark.
	 */
	var CENTER_MARK_RADIUS = 2;

	/**
	 * @constant {Number}	The opacity of the foreground material on the radar.
	 */
	var FORE_OPACITY = 0.6;

	/**
	 * @constant {Number}	The opacity of the background material on the radar.
	 */
	var BACK_OPACITY = 0.4;

	/**
	 * @constant {THREE.Vector3}	The color of the foreground material on the radar.
	 */
	var FORE_COLOR = new THREE.Vector3(0.0, 0.0, 0.0);

	/**
	 * @constant {THREE.Vector3}	The color of the background material on the radar.
	 */
	var BACK_COLOR = new THREE.Vector3(0.5, 0.5, 0.5);

	/**
	 * @constant {String}	The vertex shader program.
	 */
	var VERTEX_SHADER =
		'varying vec3 vPosition;' +

		'void main() {' +
			'vPosition = (modelViewMatrix * vec4(position, 1.0)).xyz;' +
			'gl_Position = projectionMatrix * vec4(vPosition, 1.0);' +
		'}';

	/**
	 * @constant {String}	The fragment shader program.
	 */
	var FRAGMENT_SHADER =
		'uniform vec3 materialColor;' +
		'uniform vec2 maskPosition;' +
		'uniform float opacity;' +
		'uniform float radius;' +

		'varying vec3 vPosition;' +

		'void main() {' +
			'float dist = distance(maskPosition, vec2(vPosition));' +
			'float vOpacity = opacity * float(dist < radius);' +
			'gl_FragColor = vec4(materialColor, vOpacity);' +
		'}';

	/**
	 * Class Constructor
	 * 
	 * @param {Number} x      The x position of the radar.
	 * @param {Number} y      The y position of the radar.
	 * @param {Number} radius The radius of the radar.
	 */
	function Radar(x, y, radius)
	{
		THREE.Object3D.call(this);

		this.position.set(x, y, 0);
		
		var uniforms = {
			materialColor: { type: 'v3', value: FORE_COLOR },
			maskPosition:  { type: 'v2', value: new THREE.Vector2(x, y) },
			radius:        { type: 'f', value: radius },
			opacity:       { type: 'f', value: FORE_OPACITY },
		};

		this.foreMaterial = new THREE.ShaderMaterial({
			vertexShader:   VERTEX_SHADER,
			fragmentShader: FRAGMENT_SHADER,
			uniforms:       uniforms,
			vertexColors:   THREE.VertexColors,
			transparent:    true,
		});

		var materialList = [];
		materialList[ BlockType.ROCK ] = this.foreMaterial;
		materialList[ BlockType.DIRT ] = this.foreMaterial;
		materialList[ BlockType.STONE ] = this.foreMaterial;

		this.material = new THREE.MeshFaceMaterial(materialList);

		this.backMaterial = this.foreMaterial.clone();
		this.backMaterial.uniforms.materialColor.value = BACK_COLOR;
		this.backMaterial.uniforms.opacity.value = BACK_OPACITY;

		var back = new THREE.Mesh(new THREE.PlaneGeometry(radius * 2, radius * 2), this.backMaterial);
		back.position.z = -1;
		this.add(back);

		var centerMark = new THREE.Mesh(new THREE.CircleGeometry(CENTER_MARK_RADIUS), new THREE.MeshDepthMaterial());
		centerMark.position.z = 1;
		this.add(centerMark);
	}

	/**
	 * Calculates the rotation of the radar based on a facing direction.
	 * 
	 * @param  {THREE.Vector3} faceDir  The facing direction.
	 * @return {Number}                 The radar rotation.
	 */
	function calcRotation(faceDir)
	{
		faceDir.y = 0;

		var angle = faceDir.angleTo(new THREE.Vector3(0, 0, -1));

		return (faceDir.x < 0) ? -angle : angle;
	}

	Radar.prototype = Object.create(THREE.Object3D.prototype);

	Radar.prototype.constructor = Radar;

	/**
	 * Updates the rotation and mask of the radar.
	 * 
	 * @param  {THREE.Vector3}  faceDir  The direction that the camera is facing.
	 */
	Radar.prototype.update = function(faceDir)
	{
		this.rotation.set(0, 0, calcRotation(faceDir));
		this.foreMaterial.uniforms.maskPosition.value.set(this.position.x, this.position.y);
		this.backMaterial.uniforms.maskPosition.value.set(this.position.x, this.position.y);
	};

	return Radar;

})();
