/**
 * Class:       Map
 * Author:      John Salis
 * Purpose:     Stores terrain data for a single map.
 * 
 * Date:        October 24, 2014
 */

Map.prototype.constructor = Map;

function Map()
{
	this.size = null;
	this.data = null;
}

Map.prototype.initialize = function(size, distribution)
{
	this.size = size;
	this.data = [];

	for (var i = 0; i < size; i++)
	{
		this.data[i] = [];
		for (var j = 0; j < size; j++)
		{
			this.data[i][j] = (Math.random() < distribution) ? 1 : 0;
		}
	}
}

Map.prototype.generate = function(born, survive, neighborhood)
{
	var tempMap = [];
	for (var i = 0; i < this.size; i++)
	{
		tempMap[i] = [];
		for (var j = 0; j < this.size; j++)
		{
			tempMap[i][j] = this.generateAt(i, j, born, survive, neighborhood);
		}
	}
	this.data = tempMap;
}

Map.prototype.generateAt = function(row, col, born, survive, neighborhood)
{
	var neighborCount = 0;

	for (var i = -neighborhood; i <= neighborhood; i++)
	{
		for (var j = -neighborhood; j <= neighborhood; j++)
		{
			if (this.inRange(this.data, row + i, col + j))
			{
				if (i == 0 && j == 0) continue;

				if (this.data[row + i][col + j] == 1)
				{
					neighborCount ++;
				}
			}
		}
	}

	if (neighborCount >= born)
	{
		return 1;
	}
	else if (neighborCount >= survive && this.data[row][col] == 1)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

Map.prototype.inRange = function(map, row, col)
{
	if (row < 0 || col < 0 || row >= map.length || col >= map.length)
	{
		return false;
	}
	else
	{
		return true;
	}
}

Map.prototype.print = function()
{
	for (var i = 0; i < this.data.length; i++)
	{
		var str = "";
		for (var j = 0; j < this.data.length; j++)
		{
			if (this.data[i][j] == 0)
			{
				str += ". ";
			}
			else
			{
				str += this.data[i][j] + " ";
			}
		}
		console.log(str);
	}
}
