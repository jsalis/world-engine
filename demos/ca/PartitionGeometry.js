/**
 * Class:       PartitionGeometry
 * Author:      John Salis
 * Purpose:     Extension of a plane geometry that uses an array of data
 *				to apply materials to separate sets of faces
 * 
 * Date:        October 24, 2014
 */

PartitionGeometry.prototype = new THREE.PlaneGeometry();

PartitionGeometry.prototype.constructor = THREE.PlaneGeometry;

function PartitionGeometry(length, size, data)
{
	THREE.PlaneGeometry.call(this, length, length, size, size);

	var index = 0;
	for (var i = 0; i < size; i++)
	{
		for (var j = 0; j < size; j++)
		{
			var cell = index * 2;

			this.faces[cell].materialIndex = data[i][j];
			this.faces[cell + 1].materialIndex = data[i][j];

			index ++;
		}
	}
}
