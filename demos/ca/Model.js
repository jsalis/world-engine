/**
 * Class:       Model
 * Author:      John Salis
 * Purpose:     Controls the scene, and renders to the container.
 * 
 * Date:        October 24, 2014
 */

var container, renderer, scene, camera, map;

var unit = 12;
var mapSize = 32;
var distribution = 0.48;
var born = 5;
var survive = 3;
var neighborhood = 1;

var mapGeometry, mapMesh;

var deadMaterial = new THREE.MeshBasicMaterial({ color: 0x999999 });
var liveMaterial = new THREE.MeshBasicMaterial({ color: 0x333333 });
var mapMaterial = new THREE.MeshFaceMaterial([ deadMaterial, liveMaterial ]);

function onLoad()
{
    initWebGL("webGL");
    
    camera = new THREE.OrthographicCamera(unit / -2, unit / 2, unit / 2, unit / -2, 1, 1000);
    camera.position.set(0, 0, unit);
    scene.add(camera);

    map = new Map();
    map.initialize(mapSize, distribution);

    mapMesh = new THREE.Mesh();

    initControlPanel();
    updateView();
}

function initWebGL(containerId)
{
    // Get container
    container = document.getElementById(containerId);

    // Create renderer and add it to the container
    renderer = new THREE.WebGLRenderer({ antialias: false });
    renderer.setSize(container.offsetWidth, container.offsetHeight);
    container.appendChild(renderer.domElement);

    // Create new scene
    scene = new THREE.Scene();
}

function initControlPanel()
{
    var controls = new function() {

        this.distribution = distribution;
        this.mapSize = mapSize;
        this.born = born;
        this.survive = survive;
        this.neighborhood = neighborhood;

        this.generate = function()
        {
            map.generate(born, survive, neighborhood);
            updateView();
        };

        this.reset = function()
        {
            map.initialize(mapSize, distribution);
            updateView();
        };

        this.redraw = function()
        {
            distribution = controls.distribution;
            mapSize = controls.mapSize = Math.floor(controls.mapSize);
            born = controls.born = Math.floor(controls.born);
            survive = controls.survive = Math.floor(controls.survive);
            neighborhood = controls.neighborhood = Math.floor(controls.neighborhood);
        };
    }
    var gui = new dat.GUI();
    gui.add(controls, 'mapSize', 8, 128).onChange(controls.redraw);
    gui.add(controls, 'distribution', 0, 1).onChange(controls.redraw);
    gui.add(controls, 'born', 0, 16).onChange(controls.redraw);
    gui.add(controls, 'survive', 0, 16).onChange(controls.redraw);
    gui.add(controls, 'neighborhood', 1, 3).onChange(controls.redraw);
    gui.add(controls, 'generate');
    gui.add(controls, 'reset');
    controls.redraw();
}

function updateView()
{
    scene.remove(mapMesh);
    mapMesh.geometry.dispose();

    mapGeometry = new PartitionGeometry(unit, map.size, map.data);
    mapMesh = new THREE.Mesh(mapGeometry, mapMaterial);

    scene.add(mapMesh);
    renderer.render(scene, camera);
}
