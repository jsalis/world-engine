//////////////////////////////////////////////////////////////////
// Class:           Moveable                                    //
// Author:          Michael Branton                             //
// Purpose:         represents a mesh which can					//
//                  be animated in some fashion, and has a      //
//                  position, velocity and acceleration.		//
//                  actual motion is determined by the move		//
//					method										//
//																//
// Date:            March 19, 2002								//
//					March 21, 2002								//
//					added mass and force to be used in classes  //
//					which extend this							//
//																//
// Modified:        Oct 19, 2013                                //
//                  ported to javascript and THREE.js           //
//                                                              //
//////////////////////////////////////////////////////////////////

Moveable.prototype.constructor = Moveable;

Moveable.PRECISION = 4;

function Moveable(position)
{
	this.position = position;
	this.velocity = new THREE.Vector3(0, 0, 0);
	this.force = new THREE.Vector3(0, 0, 0);
	this.setMass(1.0);
	this.setFriction(1.0);
	this.setTimestep(1.0);
	this.setMaxVelocity(Infinity);
	this.obstacleList = [];
	this.collisionEnabled = true;
}

Moveable.prototype.setPosition = function(x, y, z)
{
	this.position.x = x;
	this.position.y = y;
	this.position.z = z;
};

Moveable.prototype.setVelocity = function(x, y, z)
{
	this.velocity.x = x;
	this.velocity.y = y;
	this.velocity.z = z;
};

Moveable.prototype.setMaxVelocity = function(v)
{
	this.maxVelocity = v;
};

Moveable.prototype.setMass = function(m)
{
	this.mass = m;
};

Moveable.prototype.setFriction = function(f)
{
	this.friction = f;
};

Moveable.prototype.setForce = function(x, y, z)
{
	this.force.x = x;
	this.force.y = y;
	this.force.z = z;
};

Moveable.prototype.setTimestep = function(dt)
{
	this.dt = dt;
};

Moveable.prototype.setTime = function(t)
{
	this.t = t;
};

Moveable.prototype.addObstacle = function(o)
{
	this.obstacleList.push(o);
};

Moveable.prototype.removeObstacle = function(o)
{
	var index = this.obstacleList.indexOf(o);
	if (index != -1)
	{
		this.obstacleList.splice(index, 1);
	}
};

Moveable.prototype.checkCollision = function()
{
	var caster = new THREE.Raycaster();
	caster.far = 0.5;

	var yAxis = new THREE.Vector3(0, 1, 0);
	var rayList = [];
	rayList.push(this.velocity.clone());
	rayList.push(this.velocity.clone().applyAxisAngle(yAxis, Math.PI / 2));
	rayList.push(this.velocity.clone().applyAxisAngle(yAxis, Math.PI / -2));
	rayList.push(this.velocity.clone().applyAxisAngle(yAxis, Math.PI / 4));
	rayList.push(this.velocity.clone().applyAxisAngle(yAxis, Math.PI / -4));

	for (var i = 0; i < rayList.length; i++)
	{
		caster.set(this.position, rayList[i]);
		var collisionList = caster.intersectObjects(this.obstacleList, true);

		if (collisionList.length > 0)
		{
			distanceOver = caster.far - collisionList[0].distance;
			this.position.sub(rayList[i].clone().setLength(distanceOver));
		}

		if (collisionList.length == 1)
		{
			var a = collisionList[0].face.normal.clone().applyAxisAngle(yAxis, Math.PI / 2);
			a.setLength(this.force.length());

			var b = collisionList[0].face.normal.clone().applyAxisAngle(yAxis, Math.PI / -2);
			b.setLength(this.force.length());

			var angleA = a.angleTo(this.force);
			var angleB = b.angleTo(this.force);

			var tolerance = Math.PI / 2.5;
			if (angleA < angleB && angleA < tolerance)
			{
				this.setForce(a.x, a.y, a.z);
			}
			else if (angleB < angleA && angleB < tolerance)
			{
				this.setForce(b.x, b.y, b.z);
			}
			else
			{
				this.setForce(0, 0, 0);
				this.setVelocity(0, 0, 0);
			}
		}
	}
};

Moveable.prototype.move = function()
{
	var oldPosition = this.position.clone();

	this.t += this.dt;

	// Update position
	this.position.x += this.dt * this.velocity.x;
	this.position.y += this.dt * this.velocity.y;
	this.position.z += this.dt * this.velocity.z;

	// Check collision
	if (this.collisionEnabled && this.velocity.length() > 0)
	{
		this.checkCollision();
	}

	// Update velocity
	this.velocity.multiplyScalar(this.friction);
	this.velocity.x += this.force.x / this.mass * this.dt;
	this.velocity.y += this.force.y / this.mass * this.dt;
	this.velocity.z += this.force.z / this.mass * this.dt;

	// Check velocity range
	if (this.velocity.length() > this.maxVelocity)
	{
		this.velocity.setLength(this.maxVelocity);
	}
	else if (this.velocity.length() < 1 / Math.pow(10, Moveable.PRECISION))
	{
		this.velocity.setLength(0);
	}

	return this.position.clone().sub(oldPosition);
};
