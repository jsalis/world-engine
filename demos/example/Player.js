/**
 * @class           Player
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     
 */

Player.prototype = new Moveable();

Player.prototype.constructor = Player;

Player.MOVE_FORCE = 0.03;
Player.MAX_VELOCITY = 0.16;
Player.FRICTION = 0.92;

function Player(scene)
{
	Moveable.call(this, new THREE.Vector3(0, 0, 0));

	this.setFriction(Player.FRICTION);
	this.setMaxVelocity(Player.MAX_VELOCITY);

	var yAxis = new THREE.Vector3(0, 1, 0);

	var lightColor = 0xF0D7B3;

	var playerGeometry = new THREE.SphereGeometry(World.SCALE / 8, 16, 16);
	var playerMaterial = new THREE.MeshBasicMaterial({
		color: lightColor,
		transparent: true,
		opacity: 0.6,
	});
	var playerMesh = new THREE.Mesh(playerGeometry, playerMaterial);
	scene.add(playerMesh);

	var light = new THREE.PointLight(lightColor, 4, 16);
	//light.castShadow = true;
	//light.target = new Object3D();
	scene.add(light);

	// TEMP
	var tempGeometry = new THREE.SphereGeometry(World.SCALE / 36);
	var tempMaterial = new THREE.MeshDepthMaterial();
	var temp1 = new THREE.Mesh(tempGeometry, tempMaterial);
	var temp2 = new THREE.Mesh(tempGeometry, tempMaterial);
	var temp3 = new THREE.Mesh(tempGeometry, tempMaterial);
	var temp4 = new THREE.Mesh(tempGeometry, tempMaterial);
	var temp5 = new THREE.Mesh(tempGeometry, tempMaterial);
	scene.add(temp1);
	scene.add(temp2);
	scene.add(temp3);
	scene.add(temp4);
	scene.add(temp5);

	this.setMoveDirection = function(faceDir, forward, backward, left, right)
	{
		faceDir.y = 0;

		var zDir = new THREE.Vector3();
		var xDir = new THREE.Vector3();

		if (forward && !backward)
		{
			zDir = faceDir.clone();
		}
		else if (backward && !forward)
		{
			zDir = faceDir.clone().applyAxisAngle(yAxis, Math.PI);
		}

		if (left && !right)
		{
			xDir = faceDir.clone().applyAxisAngle(yAxis, Math.PI / 2);
		}
		else if (right && !left)
		{
			xDir = faceDir.clone().applyAxisAngle(yAxis, Math.PI / -2);
		}

		var playerDir = xDir.add(zDir);
		playerDir.setLength(Player.MOVE_FORCE);
		this.setForce(playerDir.x, 0, playerDir.z);
	};
	
	this.update = function()
	{
		playerMesh.position.copy(this.position);
		light.position.copy(this.position);
		light.position.y = 6; // TEMP

		// TEMP
		var unitVel = this.velocity.clone().divideScalar(Player.MAX_VELOCITY);
		temp1.position.copy(this.position.clone().add(unitVel));
		temp2.position.copy(this.position.clone().add(unitVel.clone().applyAxisAngle(yAxis, Math.PI / 2)));
		temp3.position.copy(this.position.clone().add(unitVel.clone().applyAxisAngle(yAxis, Math.PI / -2)));
		temp4.position.copy(this.position.clone().add(unitVel.clone().applyAxisAngle(yAxis, Math.PI / 4)));
		temp5.position.copy(this.position.clone().add(unitVel.clone().applyAxisAngle(yAxis, Math.PI / -4)));
	};

	this.destruct = function()
	{
		scene.remove(playerMesh);
		scene.remove(light);
		playerGeometry.dispose();
	};
}
