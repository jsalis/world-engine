/**
 * @class			Game
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Contains the scene, camera and all other game components.
 */
Game = (function() {

	/**
	 * Class Constructor
	 * 
	 * @param {THREE.WebGLRenderer}  renderer  The renderer to use for the game.
	 */
	function Game(renderer)
	{
		// Create scene
		this.scene = new THREE.Scene();
		
		// Create camera
		var fov = window.innerWidth / window.innerHeight;
		var frustumFar = Region.SIZE * World.SCALE * 4;
		this.camera = new THREE.PerspectiveCamera(45, fov, 1, frustumFar);
		this.scene.add(this.camera);

		// this.scene.fog = new THREE.Fog(0x000000, (Region.SIZE / 2) * World.SCALE, Region.SIZE * World.SCALE);

		// Create HUD
		var headsUpDisplay = new HeadsUpDisplay(renderer);

		// Create input manager
		var inputManager = new InputManager();
		inputManager.mapKey(InputManager.action.MOVE_FORWARD, 87);
		inputManager.mapKey(InputManager.action.MOVE_BACKWARD, 83);
		inputManager.mapKey(InputManager.action.MOVE_LEFT, 65);
		inputManager.mapKey(InputManager.action.MOVE_RIGHT, 68);

		// Create ambient light
		var ambientLight = new THREE.AmbientLight(0x6B97C9);
		this.scene.add(ambientLight);

		// Create directional light
		var directionalLight = new THREE.DirectionalLight(0xffffff, 0.3);
		directionalLight.position.set(1, 1, 0);
		this.scene.add(directionalLight);

		// Create hemisphere light
		// var h_light = new THREE.HemisphereLight(0xffffff, 0xffffff, 1);
		// this.scene.add(h_light);

		// Create player
		var player = new Player(this.scene);

		// Configure world manager
		var worldManager = WorldManager.getInstance();
		worldManager.setTarget(player.position);
		worldManager.addMoveable(player);
		this.scene.add(worldManager.getContainer());

		// Create radar
		var radius = 100;
		var offset = 15;
		var rx = (window.innerWidth / 2) - radius - offset;
		var ry = (window.innerHeight / 2) - radius - offset;
		var radar = new Radar(rx, -ry, radius);

		worldManager.setMiniWorldMaterial(radar.material);
		var miniWorld = worldManager.getMiniWorld();
		miniWorld.scale.set(2.5, 2.5, 0.1);

		radar.add(miniWorld);
		headsUpDisplay.scene.add(radar);

		// Place the player on a random floor position
		var pos = worldManager.getRandomFloorPosition(0, 0);
		player.position.set(pos.x, World.SCALE, pos.z);

		// Create a controller for the camera
		var cameraController = new THREE.OrbitControls(this.camera);
		cameraController.minDistance = World.SCALE * 4;
		cameraController.maxDistance = Region.SIZE * World.SCALE * 2.5;
		cameraController.maxPolarAngle = Math.PI / 2;
		cameraController.target = player.position;

		// Initialize camera position
		this.camera.position.x = player.position.x;
		this.camera.position.y = player.position.y + cameraController.minDistance;
		this.camera.position.z = player.position.z + cameraController.minDistance;
		cameraController.update();

		this.update = function()
		{
			// Update world
			worldManager.update();

			var forwardKey = inputManager.down(InputManager.action.MOVE_FORWARD);
			var backwardKey = inputManager.down(InputManager.action.MOVE_BACKWARD);
			var leftKey = inputManager.down(InputManager.action.MOVE_LEFT);
			var rightKey = inputManager.down(InputManager.action.MOVE_RIGHT);

			var faceDir = player.position.clone().sub(this.camera.position);
			player.setMoveDirection(faceDir, forwardKey, backwardKey, leftKey, rightKey);

			this.camera.position.add(player.move());
			this.camera.lookAt(player.position);
			player.update();

			radar.update(faceDir);

			var pos = worldManager.getLocalPosition(player.position.x, player.position.z);
			miniWorld.position.set(-pos.x * miniWorld.scale.x, pos.z * miniWorld.scale.y, 0);

			// Render the scene
			renderer.render(this.scene, this.camera);

			// Render the HUD
			headsUpDisplay.render();
		};
	}

	Game.prototype.constructor = Game;

	return Game;

})();
