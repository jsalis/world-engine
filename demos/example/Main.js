
$(document).ready(function() {

	var renderer, stats, game;

	Resources.load(function() {

		renderer = initRenderer('webGL');
		stats = initStats();

		game = new Game(renderer);

		window.addEventListener('resize', onWindowResize, false);

		run();
	});

	function initRenderer(containerId)
	{
		var container = document.getElementById(containerId);
		var renderer = new THREE.WebGLRenderer({ antialias: false });

		renderer.setSize(window.innerWidth, window.innerHeight);
		
		while (container.hasChildNodes())
			container.removeChild(container.firstChild);

		container.appendChild(renderer.domElement);

		return renderer;
	}

	function initStats()
	{
		var stats = new Stats();

		// 0: fps, 1: ms
		stats.setMode(0);

		stats.domElement.style.position = 'absolute';
		stats.domElement.style.left = '0px';
		stats.domElement.style.top = '0px';

		document.body.appendChild(stats.domElement);

		return stats;
	}

	function onWindowResize()
	{
		game.camera.aspect = window.innerWidth / window.innerHeight;
		game.camera.updateProjectionMatrix();
		renderer.setSize(window.innerWidth, window.innerHeight);
	}

	function run()
	{
		// Update the game
		game.update();

		// Update performance stats
		stats.update();

		// Request another frame
		requestAnimationFrame(run);
	}
});
