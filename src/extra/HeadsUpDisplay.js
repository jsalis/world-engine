/**
 * @class           HeadsUpDisplay
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     
 */
HeadsUpDisplay = (function() {

	/**
	 * Class Constructor
	 * 
	 * @param {THREE.WebGLRenderer}  renderer  The renderer to use for the HUD.
	 */
	function HeadsUpDisplay(renderer)
	{
		this.renderer = renderer;

		this.camera = new THREE.OrthographicCamera(-1, 1, 1, -1, 1, 10);
		this.camera.position.z = 2;

		this.scene = new THREE.Scene();
		this.scene.add(this.camera);

		this.resize();

		window.addEventListener('resize', this.resize.bind(this), false);
	}

	HeadsUpDisplay.prototype = {

		constructor: HeadsUpDisplay,

		/**
		 * Resizes the camera bounds and scales the position of the objects in the scene.
		 */
		resize: function()
		{
			var oldWidth = this.camera.right - this.camera.left;
			var oldHeight = this.camera.top - this.camera.bottom;

			var width = window.innerWidth / 2;
			var height = window.innerHeight / 2;

			this.camera.left = -width;
			this.camera.right = width;
			this.camera.top = height;
			this.camera.bottom = -height;

			this.camera.updateProjectionMatrix();

			for (var i = 0; i < this.scene.children.length; i++)
			{
				var child = this.scene.children[ i ];

				if (child instanceof THREE.Camera)
					continue;

				child.position.x *= window.innerWidth / oldWidth;
				child.position.y *= window.innerHeight / oldHeight;
			}
		},

		/**
		 * Renders the scene without clearing the renderer.
		 */
		render: function()
		{
			this.renderer.autoClear = false;
			this.renderer.render(this.scene, this.camera);
			this.renderer.autoClear = true;
		},
	};

	return HeadsUpDisplay;

})();
