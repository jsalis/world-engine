/**
 * @class			Radar
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Represents a minified version of the world.
 */
Radar = (function() {

	/**
	 * @constant {Number}	The radius of the radar center mark.
	 */
	var CENTER_MARK_RADIUS = 2;

	/**
	 * @constant {Number}	The opacity of the foreground material on the radar.
	 */
	var FORE_OPACITY = 0.6;

	/**
	 * @constant {Number}	The opacity of the background material on the radar.
	 */
	var BACK_OPACITY = 0.4;

	/**
	 * @constant {THREE.Vector3}	The color of the foreground material on the radar.
	 */
	var FORE_COLOR = new THREE.Vector3(0.0, 0.0, 0.0);

	/**
	 * @constant {THREE.Vector3}	The color of the background material on the radar.
	 */
	var BACK_COLOR = new THREE.Vector3(0.5, 0.5, 0.5);

	/**
	 * @constant {String}	The vertex shader program.
	 */
	var VERTEX_SHADER =
		'varying vec3 vPosition;' +

		'void main() {' +
			'vPosition = (modelViewMatrix * vec4(position, 1.0)).xyz;' +
			'gl_Position = projectionMatrix * vec4(vPosition, 1.0);' +
		'}';

	/**
	 * @constant {String}	The fragment shader program.
	 */
	var FRAGMENT_SHADER =
		'uniform vec3 materialColor;' +
		'uniform vec2 maskPosition;' +
		'uniform float opacity;' +
		'uniform float radius;' +

		'varying vec3 vPosition;' +

		'void main() {' +
			'float dist = distance(maskPosition, vec2(vPosition));' +
			'float vOpacity = opacity * float(dist < radius);' +
			'gl_FragColor = vec4(materialColor, vOpacity);' +
		'}';

	/**
	 * Class Constructor
	 * 
	 * @param {Number} x      The x position of the radar.
	 * @param {Number} y      The y position of the radar.
	 * @param {Number} radius The radius of the radar.
	 */
	function Radar(x, y, radius)
	{
		THREE.Object3D.call(this);

		this.position.set(x, y, 0);
		
		var uniforms = {
			materialColor: { type: 'v3', value: FORE_COLOR },
			maskPosition:  { type: 'v2', value: new THREE.Vector2(x, y) },
			radius:        { type: 'f', value: radius },
			opacity:       { type: 'f', value: FORE_OPACITY },
		};

		this.foreMaterial = new THREE.ShaderMaterial({
			vertexShader:   VERTEX_SHADER,
			fragmentShader: FRAGMENT_SHADER,
			uniforms:       uniforms,
			vertexColors:   THREE.VertexColors,
			transparent:    true,
		});

		var materialList = [];
		materialList[ BlockType.ROCK ] = this.foreMaterial;
		materialList[ BlockType.DIRT ] = this.foreMaterial;
		materialList[ BlockType.STONE ] = this.foreMaterial;

		this.material = new THREE.MeshFaceMaterial(materialList);

		this.backMaterial = this.foreMaterial.clone();
		this.backMaterial.uniforms.materialColor.value = BACK_COLOR;
		this.backMaterial.uniforms.opacity.value = BACK_OPACITY;

		var back = new THREE.Mesh(new THREE.PlaneGeometry(radius * 2, radius * 2), this.backMaterial);
		back.position.z = -1;
		this.add(back);

		var centerMark = new THREE.Mesh(new THREE.CircleGeometry(CENTER_MARK_RADIUS), new THREE.MeshDepthMaterial());
		centerMark.position.z = 1;
		this.add(centerMark);
	}

	/**
	 * Calculates the rotation of the radar based on a facing direction.
	 * 
	 * @param  {THREE.Vector3} faceDir  The facing direction.
	 * @return {Number}                 The radar rotation.
	 */
	function calcRotation(faceDir)
	{
		faceDir.y = 0;

		var angle = faceDir.angleTo(new THREE.Vector3(0, 0, -1));

		return (faceDir.x < 0) ? -angle : angle;
	}

	Radar.prototype = Object.create(THREE.Object3D.prototype);

	Radar.prototype.constructor = Radar;

	/**
	 * Updates the rotation and mask of the radar.
	 * 
	 * @param  {THREE.Vector3}  faceDir  The direction that the camera is facing.
	 */
	Radar.prototype.update = function(faceDir)
	{
		this.rotation.set(0, 0, calcRotation(faceDir));
		this.foreMaterial.uniforms.maskPosition.value.set(this.position.x, this.position.y);
		this.backMaterial.uniforms.maskPosition.value.set(this.position.x, this.position.y);
	};

	return Radar;

})();
