/**
 * @class			StoneCornerCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
StoneCornerCeiling = (function() {

	var BLACK = new THREE.Color(0x000000);
	var CLEAR = new THREE.Color(0xffffff);

	function StoneCornerCeiling()
	{
		THREE.Geometry.call(this);

		// Add vertices
		this.vertices.push(new THREE.Vector3(0, 0, 0));
		this.vertices.push(new THREE.Vector3(0, 1, 0));
		this.vertices.push(new THREE.Vector3(1, 1, 0));
		this.vertices.push(new THREE.Vector3(0.75, 0, 0));
		this.vertices.push(new THREE.Vector3(1, 0.25, 0));
		
		// Add faces
		this.faces.push(new THREE.Face3(1, 0, 3));
		this.faces.push(new THREE.Face3(4, 1, 3));
		this.faces.push(new THREE.Face3(4, 2, 1));

		this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, -0.5, 0));
		this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

		this.removeColor();
		this.computeFaceNormals();
		this.computeVertexNormals();

		BasicSideCeiling.computeVertexUvs(this);
	}

	StoneCornerCeiling.prototype = new THREE.Geometry();

	StoneCornerCeiling.prototype.constructor = StoneCornerCeiling;

	StoneCornerCeiling.prototype.colorCorner = function()
	{
		this.faces[0].vertexColors[0] = BLACK;
		this.faces[1].vertexColors[1] = BLACK;
		this.faces[2].vertexColors[2] = BLACK;
	};

	StoneCornerCeiling.prototype.removeColor = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = CLEAR;
			}
		}
	};

	return StoneCornerCeiling;

})();
