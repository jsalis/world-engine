/**
 * @class			StoneTipCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

StoneTipCeiling.prototype = new THREE.Geometry();

StoneTipCeiling.prototype.constructor = StoneTipCeiling;

function StoneTipCeiling()
{
	THREE.Geometry.call(this);

	// Add vertices
	this.vertices.push(new THREE.Vector3(0, 1, 0));
	this.vertices.push(new THREE.Vector3(1, 1, 0));
	this.vertices.push(new THREE.Vector3(0.5, 1, 0));
	this.vertices.push(new THREE.Vector3(0, 0.25, 0));
	this.vertices.push(new THREE.Vector3(0.25, 0, 0));
	this.vertices.push(new THREE.Vector3(0.75, 0, 0));
	this.vertices.push(new THREE.Vector3(1, 0.25, 0));
	
	// Add faces
	this.faces.push(new THREE.Face3(2, 0, 3));
	this.faces.push(new THREE.Face3(2, 3, 4));
	this.faces.push(new THREE.Face3(2, 4, 5));
	this.faces.push(new THREE.Face3(2, 5, 6));
	this.faces.push(new THREE.Face3(2, 6, 1));

	// Set vertex colors
	var clear = new THREE.Color(0xffffff);

	for (var i = 0; i < this.faces.length; i++)
	{
		for (var j = 0; j < 3; j++)
		{
			this.faces[i].vertexColors[j] = clear;
		}
	}

	this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, -0.5, 0));
	this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

	this.computeFaceNormals();
	this.computeVertexNormals();

	BasicSideCeiling.computeVertexUvs(this);
}
