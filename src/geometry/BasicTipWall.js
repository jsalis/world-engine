/**
 * @class			BasicTipWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

BasicTipWall.prototype = new THREE.PlaneGeometry();

BasicTipWall.prototype.constructor = BasicTipWall;

function BasicTipWall(size, height, widthSegments, heightSegments)
{
	THREE.PlaneGeometry.call(this, size * Math.sqrt(2), height, widthSegments * 2, heightSegments);

	var axis = new THREE.Vector3(0, 1, 0);
	var angle = Math.PI / 4;
	var matrix = new THREE.Matrix4().makeRotationAxis(axis, angle);

	for (var i = 0; i < this.vertices.length; i++)
	{
		if (this.vertices[i].x < 0)
		{
			var temp = -this.vertices[i].z;
			this.vertices[i].z = this.vertices[i].x;
			this.vertices[i].x = temp;
		}
		this.vertices[i].applyMatrix4(matrix);
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}
