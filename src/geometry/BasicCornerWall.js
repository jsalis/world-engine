/**
 * @class			BasicCornerWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

BasicCornerWall.prototype = new THREE.PlaneGeometry();

BasicCornerWall.prototype.constructor = BasicCornerWall;

function BasicCornerWall(size, height, widthSegments, heightSegments)
{
	THREE.PlaneGeometry.call(this, size * Math.sqrt(2), height, widthSegments, heightSegments);

	var axis = new THREE.Vector3(0, 1, 0);
	var angle = Math.PI / 4;
	var matrix = new THREE.Matrix4().makeRotationAxis(axis, angle);

	for (var i = 0; i < this.vertices.length; i++)
	{
		this.vertices[i].applyMatrix4(matrix);
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}
