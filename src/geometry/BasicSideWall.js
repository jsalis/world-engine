/**
 * @class			BasicSideWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

BasicSideWall.prototype = new THREE.PlaneGeometry();

BasicSideWall.prototype.constructor = BasicSideWall;

function BasicSideWall(size, height, widthSegments, heightSegments)
{
	THREE.PlaneGeometry.call(this, size, height, widthSegments, heightSegments);

	var matrix = new THREE.Matrix4().makeTranslation(0, 0, size / 2);

	for (var i = 0; i < this.vertices.length; i++)
	{
		this.vertices[i].applyMatrix4(matrix);
	}

	this.computeFaceNormals();
	this.computeVertexNormals();
}
