/**
 * @class			BasicCornerCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
BasicCornerCeiling = (function() {

	var BLACK = new THREE.Color(0x000000);
	var CLEAR = new THREE.Color(0xffffff);

	function BasicCornerCeiling(segments)
	{
		THREE.Geometry.call(this);

		this.segments = segments || 1;

		var step = 1 / this.segments;

		// Add vertices
		for (var i = 0; i < 1; i += step)
		{
			this.vertices.push(new THREE.Vector3(i, i, 0));
		}
		this.vertices.push(new THREE.Vector3(1, 1, 0));
		this.vertices.push(new THREE.Vector3(0, 1, 0));

		var vl = this.vertices.length;
		
		// Add faces
		for (var i = 0; i < this.segments; i++)
		{
			this.faces.push(new THREE.Face3(i, i + 1, vl - 1));
		}

		this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, -0.5, 0));
		this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

		this.removeColor();
		this.computeFaceNormals();
		this.computeVertexNormals();

		BasicSideCeiling.computeVertexUvs(this);
	}

	BasicCornerCeiling.prototype = new THREE.Geometry();

	BasicCornerCeiling.prototype.constructor = BasicCornerCeiling;

	BasicCornerCeiling.prototype.colorCorner = function()
	{
		for (var i = 0; i < this.segments; i++)
		{
			this.faces[i].vertexColors[2] = BLACK;
		}
	};

	BasicCornerCeiling.prototype.removeColor = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = CLEAR;
			}
		}
	};

	return BasicCornerCeiling;

})();
