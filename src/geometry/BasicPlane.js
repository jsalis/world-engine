/**
 * @class			BasicPlane
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
BasicPlane = (function() {

	var BLACK = new THREE.Color(0x000000);

	function BasicPlane()
	{
		THREE.Geometry.call(this);

		// Add vertices
		this.vertices.push(new THREE.Vector3(0, 0, 0));
		this.vertices.push(new THREE.Vector3(1, 0, 1));
		this.vertices.push(new THREE.Vector3(0, 0, 1));
		this.vertices.push(new THREE.Vector3(1, 0, 0));

		// Add faces
		this.faces.push(new THREE.Face3(0, 2, 1));
		this.faces.push(new THREE.Face3(0, 1, 3));

		this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, 0, -0.5));

		this.computeFaceNormals();
		this.computeVertexNormals();
		this.removeColor();

		BasicSideCeiling.computeVertexUvs(this);
	}

	BasicPlane.prototype = Object.create(THREE.Geometry.prototype);

	BasicPlane.prototype.constructor = BasicPlane;

	BasicPlane.prototype.colorAll = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = BLACK;
			}
		}

		return this;
	};

	BasicPlane.prototype.removeColor = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = new THREE.Color(1, 1, 1);
			}
		}

		return this;
	};

	return BasicPlane;

})();
