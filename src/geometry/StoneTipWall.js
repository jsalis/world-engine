/**
 * @class			StoneTipWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

StoneTipWall.prototype = new THREE.PlaneGeometry();

StoneTipWall.prototype.constructor = StoneTipWall;

function StoneTipWall(size, height, heightSegments)
{
	var widthSegments = 6;
	THREE.PlaneGeometry.call(this, size, height, widthSegments, heightSegments);

	for (var i = 0; i < this.vertices.length; i += widthSegments + 1)
	{
		this.vertices[i].x = size * -0.5;
		this.vertices[i].z = size * -0.5;

		this.vertices[i + 1].x = size * -0.5;
		this.vertices[i + 1].z = size * 0.25;

		this.vertices[i + 2].x = size * -0.25;
		this.vertices[i + 2].z = size * 0.5;

		this.vertices[i + 3].x = size * 0;
		this.vertices[i + 3].z = size * 0.5;

		this.vertices[i + 4].x = size * 0.25;
		this.vertices[i + 4].z = size * 0.5;

		this.vertices[i + 5].x = size * 0.5;
		this.vertices[i + 5].z = size * 0.25;

		this.vertices[i + 6].x = size * 0.5;
		this.vertices[i + 6].z = size * -0.5;
	}

	var uvAdjustLeft = [0, 0.56, 0.8, 1, 0.2, 0.44, 1];
	var uvAdjustRight = [0, 0.56, 0.8, 0, 0.2, 0.44, 1];
	var step = 1 / widthSegments;

	for (var i = 0; i < this.faceVertexUvs[0].length; i++)
	{
		var uvs = this.faceVertexUvs[0][i];

		var isLeft = (i % (widthSegments * 2) < widthSegments);
		var uvAdjust = (isLeft) ? uvAdjustLeft : uvAdjustRight;

		uvs[0].x = uvAdjust[ Math.round(uvs[0].x / step) ];
		uvs[1].x = uvAdjust[ Math.round(uvs[1].x / step) ];
		uvs[2].x = uvAdjust[ Math.round(uvs[2].x / step) ];
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}
