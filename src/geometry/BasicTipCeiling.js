/**
 * @class			BasicTipCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

BasicTipCeiling.prototype = new THREE.Geometry();

BasicTipCeiling.prototype.constructor = BasicTipCeiling;

function BasicTipCeiling(segments)
{
	THREE.Geometry.call(this);

	this.segments = segments || 1;

	var step = 1 / (this.segments * 2);

	// Add vertices
	for (var i = 0; i < 0.5; i += step)
	{
		this.vertices.push(new THREE.Vector3(i, 1 - i, 0));
		this.vertices.push(new THREE.Vector3(1 - i, 1 - i, 0));
	}
	this.vertices.push(new THREE.Vector3(0.5, 0.5, 0));

	var vl = this.vertices.length;
	
	// Add faces
	for (var i = 0; i <= this.segments - 2; i += 2)
	{
		this.faces.push(new THREE.Face3(i, i + 3, i + 1));
		this.faces.push(new THREE.Face3(i, i + 2, i + 3));
	}
	this.faces.push(new THREE.Face3(vl - 1, vl - 2, vl - 3));

	// Set vertex colors
	var clear = new THREE.Color(0xffffff);

	for (var i = 0; i < this.faces.length; i++)
	{
		for (var j = 0; j < 3; j++)
		{
			this.faces[i].vertexColors[j] = clear;
		}
	}

	this.applyMatrix(new THREE.Matrix4().makeTranslation(-0.5, -0.5, 0));
	this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

	this.computeFaceNormals();
	this.computeVertexNormals();

	BasicSideCeiling.computeVertexUvs(this);
}
