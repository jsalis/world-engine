/**
 * @class			BasicSideCeiling
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
BasicSideCeiling = (function() {

	var BLACK = new THREE.Color(0x000000);
	var FADE = new THREE.Color(0x777777);
	var CLEAR = new THREE.Color(0xffffff);

	var FACE_INDICES = ['a','b','c'];

	var Z_AXIS = new THREE.Vector3(0, 0, -1);

	var OFFSET = 0.5;

	function BasicSideCeiling(segments)
	{
		THREE.Geometry.call(this);

		this.segments = segments || 1;

		var step = 1 / this.segments;

		// Add vertices
		for (var i = 0; i < 1; i += step)
		{
			this.vertices.push(new THREE.Vector3(i, 0, 0));
		}
		this.vertices.push(new THREE.Vector3(1, 0, 0));
		this.vertices.push(new THREE.Vector3(0.5, 0.5, 0));

		var vl = this.vertices.length;
		
		// Add faces
		for (var i = 0; i < this.segments; i++)
		{
			this.faces.push(new THREE.Face3(i, i + 1, vl - 1));
		}

		this.applyMatrix(new THREE.Matrix4().makeTranslation(-OFFSET, -OFFSET, 0));
		this.applyMatrix(new THREE.Matrix4().makeRotationX(Math.PI / -2));

		this.computeFaceNormals();
		this.computeVertexNormals();
		this.removeColor();

		BasicSideCeiling.computeVertexUvs(this);
	}

	BasicSideCeiling.prototype = Object.create(THREE.Geometry.prototype);

	BasicSideCeiling.prototype.constructor = BasicSideCeiling;

	BasicSideCeiling.computeVertexUvs = function(geometry, theta, row, col, rowRepeat, colRepeat)
	{
		theta = theta || 0;
		row = row || 0;
		col = col || 0;
		rowRepeat = rowRepeat || 1;
		colRepeat = colRepeat || 1;

		var c = col % colRepeat;
		var r = row % rowRepeat;

		for (var i = 0; i < geometry.faces.length; i++)
		{
			if (geometry.faceVertexUvs[0][i] === undefined)
				geometry.faceVertexUvs[0][i] = [ new THREE.Vector2(), new THREE.Vector2(), new THREE.Vector2() ];

			var uvs = geometry.faceVertexUvs[0][i];
			var face = geometry.faces[i];

			for (var j = 0; j < FACE_INDICES.length; j++)
			{
				var index = FACE_INDICES[j];
				var vertex = geometry.vertices[ face[index] ];

				var vec = new THREE.Vector3(vertex.x, vertex.z, 0).applyAxisAngle(Z_AXIS, theta);

				uvs[j].x = ((vec.x + OFFSET) / colRepeat) + (c / colRepeat);
				uvs[j].y = ((vec.y + OFFSET) / rowRepeat) + (r / rowRepeat);
			}
		}
	};

	BasicSideCeiling.colorNorthWest = function(geometry)
	{
		BasicSideCeiling.addGadient(geometry, -0.5, -0.5);
	};

	BasicSideCeiling.colorNorthEast = function(geometry)
	{
		BasicSideCeiling.addGadient(geometry, 0.5, -0.5);
	};

	BasicSideCeiling.colorSouthWest = function(geometry)
	{
		BasicSideCeiling.addGadient(geometry, -0.5, 0.5);
	};

	BasicSideCeiling.colorSouthEast = function(geometry)
	{
		BasicSideCeiling.addGadient(geometry, 0.5, 0.5);
	};

	BasicSideCeiling.addGadient = function(geometry, x, z)
	{
		for (var i = 0; i < geometry.faces.length; i++)
		{
			var face = geometry.faces[i];

			for (var j = 0; j < FACE_INDICES.length; j++)
			{
				var vertex = geometry.vertices[ face[ FACE_INDICES[j] ] ];
				var dx = Math.abs(vertex.x - x);
				var dz = Math.abs(vertex.z - z);
				var dist = Math.sqrt(dx * dx + dz * dz);
				var s = 1 - Math.min(dist, 1);

				face.vertexColors[j].addScalar(-s);
			}
		}
	};

	BasicSideCeiling.prototype.colorAll = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = BLACK;
			}
		}

		return this;
	};

	BasicSideCeiling.prototype.removeColor = function()
	{
		for (var i = 0; i < this.faces.length; i++)
		{
			for (var j = 0; j < 3; j++)
			{
				this.faces[i].vertexColors[j] = new THREE.Color(1, 1, 1);
			}
		}

		return this;
	};

	return BasicSideCeiling;

})();
