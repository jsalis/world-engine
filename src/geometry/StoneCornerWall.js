/**
 * @class			StoneCornerWall
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */

StoneCornerWall.prototype = new THREE.PlaneGeometry();

StoneCornerWall.prototype.constructor = StoneCornerWall;

function StoneCornerWall(size, height, heightSegments)
{
	var widthSegments = 4;
	THREE.PlaneGeometry.call(this, size, height, widthSegments, heightSegments);

	for (var i = 0; i < this.vertices.length; i += widthSegments + 1)
	{
		this.vertices[i].x = -0.5;
		this.vertices[i].z = 0.5;

		this.vertices[i + 1].x = 0.25;
		this.vertices[i + 1].z = 0.5;

		this.vertices[i + 2].x = 0.375;
		this.vertices[i + 2].z = 0.375;

		this.vertices[i + 3].x = 0.5;
		this.vertices[i + 3].z = 0.25;

		this.vertices[i + 4].x = 0.5;
		this.vertices[i + 4].z = -0.5;
	}

	var uvAdjustLeft = [0, 0.8, 1, 0.2, 1];
	var uvAdjustRight = [0, 0.8, 0, 0.2, 1];
	var step = 1 / widthSegments;

	for (var i = 0; i < this.faceVertexUvs[0].length; i++)
	{
		var uvs = this.faceVertexUvs[0][i];

		var isLeft = (i % (widthSegments * 2) < widthSegments);
		var uvAdjust = (isLeft) ? uvAdjustLeft : uvAdjustRight;

		uvs[0].x = uvAdjust[ Math.round(uvs[0].x / step) ];
		uvs[1].x = uvAdjust[ Math.round(uvs[1].x / step) ];
		uvs[2].x = uvAdjust[ Math.round(uvs[2].x / step) ];
	}
	
	this.computeFaceNormals();
	this.computeVertexNormals();
}
