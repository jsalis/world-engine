/**
 * @class			WaterSource
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
WaterSource = (function() {

	var geometry = new THREE.SphereGeometry(0.25);
	var material = new THREE.MeshLambertMaterial({ color: 0x0000ff });

	/**
	 * Class Constructor
	 */
	function WaterSource()
	{
		EnvironmentObject.call(this);

		this.add(new THREE.Mesh(geometry, material));
	}

	WaterSource.generate = function(grid, outerRow, outerCol, innerRow, innerCol)
	{
		var region = grid[ outerRow ][ outerCol ];

		Generator.floodFill(null, innerRow, innerCol, true, true, {
			decay:        0.8,
			useCardinals: true,
			seed:         region.seeds[ BlockType.NONE ],
			regList:      grid,
			regRow:       outerRow,
			regCol:       outerCol,
			fromLayer:    RegionLayer.FLOOR,
			toLayer:      RegionLayer.WATER,
		});
	};

	WaterSource.prototype = Object.create(EnvironmentObject.prototype);

	WaterSource.prototype.constructor = WaterSource;

	/**
	 * Updates the water source for the next frame.
	 */
	WaterSource.prototype.update = function()
	{
		EnvironmentObject.prototype.update.call(this);
	};

	return WaterSource;

})();
