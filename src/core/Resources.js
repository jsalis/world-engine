/**
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Object that provides a function to load all resources and 
 *                  store them as properties.
 */

Resources = {};

Resources.load = function(callback)
{
	var files = [
		'img/rock_wall.jpg',
		'img/rock_floor.jpg',
		'img/dirt_wall.jpg',
		'img/stone_wall.jpg',
		'img/stone_ceil.jpg',
		'img/stone_floor.jpg'
	];

	var textures = {};
	var loader = new THREE.TextureLoader();
	var count = 0;

	files.forEach(function(file) {

		loader.load(file, function(texture) {

			textures[ file ] = texture;
			count++;

			if (count == files.length)
				initAssets();
		});
	});

	function initAssets()
	{
		var WIDTH_SEGMENTS = 3;
		var HEIGHT_SEGMENTS = 5;

		Resources.sideCeiling = new BasicSideCeiling();
		Resources.fullCeiling = new BasicPlane().colorAll();
		Resources.basicPlane = new BasicPlane();

		Resources.wallList = [];

		// ROCK WALL GEOMETRY
		Resources.wallList[ BlockType.ROCK ] = {
			tip:    new BasicTipWall(1, BlockView.HEIGHT_SCALE, Math.ceil(WIDTH_SEGMENTS / 2), HEIGHT_SEGMENTS),
			corner: new BasicCornerWall(1, BlockView.HEIGHT_SCALE , WIDTH_SEGMENTS, HEIGHT_SEGMENTS),
			side:   new BasicSideWall(1, BlockView.HEIGHT_SCALE, WIDTH_SEGMENTS, HEIGHT_SEGMENTS),
		};

		// DIRT WALL GEOMETRY
		Resources.wallList[ BlockType.DIRT ] = {
			tip:    Resources.wallList[ BlockType.ROCK ].tip,
			corner: Resources.wallList[ BlockType.ROCK ].corner,
			side:   Resources.wallList[ BlockType.ROCK ].side,
		};

		// STONE WALL GEOMETRY
		Resources.wallList[ BlockType.STONE ] = {
			tip:    new StoneTipWall(1, BlockView.HEIGHT_SCALE, HEIGHT_SEGMENTS),
			corner: new StoneCornerWall(1, BlockView.HEIGHT_SCALE, HEIGHT_SEGMENTS),
			side:   new BasicSideWall(1, BlockView.HEIGHT_SCALE, 1, HEIGHT_SEGMENTS),
		};

		Resources.ceilingList = [];

		// ROCK CEILING GEOMETRY
		Resources.ceilingList[ BlockType.ROCK ] = {
			tip:    new BasicTipCeiling(Math.ceil(WIDTH_SEGMENTS / 2)),
			corner: new BasicCornerCeiling(WIDTH_SEGMENTS),
			side:   new BasicSideCeiling(WIDTH_SEGMENTS),
		};

		// DIRT CEILING GEOMETRY
		Resources.ceilingList[ BlockType.DIRT ] = {
			tip:    Resources.ceilingList[ BlockType.ROCK ].tip,
			corner: Resources.ceilingList[ BlockType.ROCK ].corner,
			side:   Resources.ceilingList[ BlockType.ROCK ].side,
		};

		// STONE CEILING GEOMETRY
		Resources.ceilingList[ BlockType.STONE ] = {
			tip:    new StoneTipCeiling(),
			corner: new StoneCornerCeiling(),
			side:   new BasicSideCeiling(),
		};

		// ROCK MATERIAL
		var rock = {};

		var rockTexture = textures['img/rock_wall.jpg'].clone();
		rockTexture.wrapS = rockTexture.wrapT = THREE.RepeatWrapping;
		rockTexture.repeat.set(1, BlockView.HEIGHT_SCALE);
		rockTexture.needsUpdate = true;

		rock.wall = new THREE.MeshLambertMaterial({
			map:          rockTexture,
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.SmoothShading,
		});

		rock.ceil = new THREE.MeshLambertMaterial({
			map:          textures['img/rock_wall.jpg'],
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.SmoothShading,
		});

		rock.floor = new THREE.MeshLambertMaterial({
			map:     textures['img/rock_floor.jpg'],
			ambient: 0x999999,
			color:   0x8A8A70,
		});
		rock.floor.rowRepeat = 3;
		rock.floor.colRepeat = 3;

		// DIRT MATERIAL
		var dirt = {};

		var dirtTexture = textures['img/dirt_wall.jpg'].clone();
		dirtTexture.wrapS = dirtTexture.wrapT = THREE.RepeatWrapping;
		dirtTexture.repeat.set(1, BlockView.HEIGHT_SCALE);
		dirtTexture.needsUpdate = true;

		dirt.wall = new THREE.MeshLambertMaterial({
			map:          dirtTexture,
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.SmoothShading,
		});

		dirt.ceil = new THREE.MeshLambertMaterial({
			map:          textures['img/dirt_wall.jpg'],
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.SmoothShading,
		});

		// STONE MATERIAL
		var stone = {};
		
		var stoneTexture = textures['img/stone_wall.jpg'].clone();
		stoneTexture.wrapS = stoneTexture.wrapT = THREE.RepeatWrapping;
		stoneTexture.repeat.set(1, BlockView.HEIGHT_SCALE);
		stoneTexture.needsUpdate = true;

		stone.wall = new THREE.MeshLambertMaterial({
			map:          stoneTexture,
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.FlatShading,
		});

		stone.ceil = new THREE.MeshLambertMaterial({
			map:          textures['img/stone_ceil.jpg'],
			ambient:      0x999999,
			color:        0x8A8A70,
			vertexColors: THREE.VertexColors,
			shading:      THREE.FlatShading,
		});

		stone.floor = new THREE.MeshLambertMaterial({
			map:     textures['img/stone_floor.jpg'],
			ambient: 0x888888,
			color:   0x8A8A70,
		});
		stone.floor.rowRepeat = 1;
		stone.floor.colRepeat = 1;

		Resources.materialList = [];

		// WATER MATERIAL
		Resources.materialList[ BlockViewLayer.WATER ] = new THREE.MeshLambertMaterial({
			color:       0x0000dd,
			ambient:     0x999999,
			transparent: true,
			opacity:     0.25,
		});

		var list;

		// FLOOR MATERIAL LIST
		list = [];
		list[ BlockType.ROCK ] = rock.floor;
		list[ BlockType.DIRT ] = rock.floor;
		list[ BlockType.STONE ] = stone.floor;

		Resources.materialList[ BlockViewLayer.FLOOR ] = new THREE.MeshFaceMaterial(list);

		// WALL MATERIAL LIST
		list = [];
		list[ BlockType.ROCK ] = rock.wall;
		list[ BlockType.DIRT ] = dirt.wall;
		list[ BlockType.STONE ] = stone.wall;

		Resources.materialList[ BlockViewLayer.WALL ] = new THREE.MeshFaceMaterial(list);

		// CEILING MATERIAL LIST
		list = [];
		list[ BlockType.ROCK ] = rock.ceil;
		list[ BlockType.DIRT ] = dirt.ceil;
		list[ BlockType.STONE ] = stone.ceil;

		Resources.materialList[ BlockViewLayer.CEILING ] = new THREE.MeshFaceMaterial(list);

		callback();
	}
};
