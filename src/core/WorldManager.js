/**
 * @class           WorldManager
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     Singleton that manages the model and view for the world, and triggers 
 *                  world movement based on the movement of a target position.
 */
WorldManager = (function() {

	var instance;

	function init()
	{
		var container = new THREE.Object3D();
		var target = new THREE.Vector3();

		var world = new World();
		var worldView = new WorldView(container, world);

		var threshold = World.SCALE * 6;

		function checkBounds()
		{
			var regIndex = world.getCurrentRegionIndex();
			var regPos = world.getRegionPosition(regIndex.row, regIndex.col);
			var regLength = Region.SIZE * World.SCALE;
			
			var northBoundHit = target.z < regPos.z - (regLength / 2) - threshold;
			var southBoundHit = target.z > regPos.z + (regLength / 2) + threshold;
			var eastBoundHit = target.x > regPos.x + (regLength / 2) + threshold;
			var westBoundHit = target.x < regPos.x - (regLength / 2) - threshold;

			if (northBoundHit || southBoundHit || eastBoundHit || westBoundHit) {

				var targetRegIndex = world.getRegionIndex(target.z, target.x);

				var rowDir = targetRegIndex.row - regIndex.row;
				var colDir = targetRegIndex.col - regIndex.col;

				world.move(rowDir, colDir);
				worldView.move(rowDir, colDir);
				worldView.clean();
			}
		}

		return {

			update: function() {

				checkBounds();
				worldView.update();
			},

			addMoveable: function(moveable) {

				worldView.addMoveable(moveable);
			},

			setTarget: function(pos) {

				target = pos;
			},

			setMiniWorldMaterial: function(material) {

				worldView.setMiniWorldMaterial(material);
			},

			getContainer: function() {

				return container;
			},

			getMiniWorld: function() {

				return worldView.getMiniWorld();
			},

			getLocalPosition: function(x, z) {

				var regIndex = world.getCurrentRegionIndex();
				var pos = world.getRegionPosition(regIndex.row, regIndex.col);
				return { x : x - pos.x, z : z - pos.z };
			},

			getRegionPosition: function(regRow, regCol) {

				return world.getRegionPosition(regRow, regCol);
			},

			getCellPosition: function(regRow, regCol, cellRow, cellCol) {

				return world.getCellPosition(regRow, regCol, cellRow, cellCol);
			},

			getRandomFloorPosition: function(regRow, regCol) {

				return world.getRandomFloorPosition(regRow, regCol);
			},
		};
	}

	return {

		getInstance: function() {

			if (!instance) {

				instance = init();
			}

			return instance;
		}
	};

})();
