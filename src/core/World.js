/**
 * @class           World
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     Stores a 2D array of regions and keeps track which of regions are generated.
 */

World.prototype.constructor = World;

World.SCALE = 2;

function World()
{
	var regionList = [];
	var current = { row : 0, col : 0 };

	regionList[ current.row ] = [];
	regionList[ current.row ][ current.col ] = new CaveRegion();

	initializeArea(current.row, current.col);
	generateArea(current.row, current.col);

	function initializeArea(row, col)
	{
		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[d];
			var r = row + dir.row;
			var c = col + dir.col;
			
			if (regionList[r] == null)
				regionList[r] = [];

			if (regionList[r][c] == null)
				regionList[r][c] = createRegion();

			regionList[r][c].initialize();
		}
	}

	function createRegion()
	{
		var region = new SolidRegion();

		if (Math.random() < 0.95)
		{
			region = (Math.random() < 0.1) ? new DungeonRegion() : new CaveRegion();
		}
		
		return region;
	}

	function generateArea(row, col)
	{
		Region.generate(regionList, row, col);
	}

	function clearArea(row, col)
	{
		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[d];
			regionList[ row + dir.row ][ col + dir.col ].clear();
		}
	}

	function isRegionAvailable(row, col)
	{
		return (regionList[ row ] != null && regionList[ row ][ col ] != null && regionList[ row ][ col ].data != null);
	}

	this.move = function(rowDir, colDir)
	{
		clearArea(current.row, current.col);
		current.row += rowDir;
		current.col += colDir;
		initializeArea(current.row, current.col);
		generateArea(current.row, current.col);
	};

	this.moveNorth = function()
	{
		this.move(-1, 0);
	};

	this.moveSouth = function()
	{
		this.move(1, 0);
	};

	this.moveEast = function()
	{
		this.move(0, 1);
	};

	this.moveWest = function()
	{
		this.move(0, -1);
	};

	this.getCurrentRegionIndex = function()
	{
		return { row : current.row, col : current.col };
	};

	this.getRegionIndex = function(z, x)
	{
		var regLength = Region.SIZE * World.SCALE;
		var regRow = Math.floor((z + (regLength / 2)) / regLength);
		var regCol = Math.floor((x + (regLength / 2)) / regLength);
		return { row : regRow, col : regCol };
	};

	this.getRegionPosition = function(regRow, regCol)
	{
		var z = regRow * Region.SIZE * World.SCALE;
		var x = regCol * Region.SIZE * World.SCALE;
		return { z : z, x : x };
	};

	this.getCellIndex = function(z, x)
	{
		// TO DO
	};

	this.getCellPosition = function(regRow, regCol, cellRow, cellCol)
	{
		var regLength = Region.SIZE * World.SCALE;
		var z = (regRow * regLength) + (cellRow * World.SCALE) + (World.SCALE / 2) - (regLength / 2);
		var x = (regCol * regLength) + (cellCol * World.SCALE) + (World.SCALE / 2) - (regLength / 2);
		return { z : z, x : x };
	};

	this.getCellValue = function(regRow, regCol, cellIndex)
	{
		return regionList[ regRow ][ regCol ].getCellValue(cellIndex);
	};

	this.getRandomFloorPosition = function(regRow, regCol)
	{
		var layer = regionList[ regRow ][ regCol ].data[ RegionLayer.FLOOR ];
		var position = Generator.getRandomPosition(layer, true);
		return (position == null) ? null : this.getCellPosition(regRow, regCol, position.row, position.col);
	};

	this.getRegionData = function(row, col)
	{
		var copy = [];

		copy[ RegionLayer.BLOCK ] = regionList[ row ][ col ].data[ RegionLayer.BLOCK ];
		copy[ RegionLayer.FLOOR ] = Generator.getPaddedMap(regionList, row, col, RegionLayer.FLOOR, false);
		copy[ RegionLayer.WATER ] = regionList[ row ][ col ].data[ RegionLayer.WATER ];
		copy[ RegionLayer.OBJECT ] = regionList[ row ][ col ].data[ RegionLayer.OBJECT ];

		return copy;
	};

	this.getRegionSeed = function(row, col)
	{
		return regionList[ row ][ col ].seeds[ BlockType.NONE ];
	};
}
