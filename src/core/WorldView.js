/**
 * @class           WorldView
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     Manages the generation of region views and maintains a mini world for navigation.
 */

WorldView.prototype.constructor = WorldView;

function WorldView(container, world)
{
	var tickCount = 0;
	var updateInterval = 6;
	var updateQueue = [];
	var moveableList = [];

	var usingMiniWorld = true;

	var miniWorldMaterial = BlockView.ceilingMaterial; //.clone(); ?
	var miniWorld = new THREE.Object3D();
	
	var mapView = new Array(3);
	var miniMapView = new Array(3);

	for (var i = -1; i <= 1; i++)
	{
		mapView[i] = new Array(3);
		miniMapView[i] = new Array(3);
		for (var j = -1; j <= 1; j++)
		{
			updateMap(i, j);

			if (usingMiniWorld)
				updateMiniMap(i, j);
		}
	}

	function updateMap(row, col, rowDir, colDir)
	{
		var mapIndex = world.getCurrentRegionIndex();
		var map = world.getRegionData(mapIndex.row + row, mapIndex.col + col);

		if (mapView[row][col] != null)
		{
			// Partially update the map view
			rowDir = rowDir || 0;
			colDir = colDir || 0;
			return mapView[row][col].updateToward(map, rowDir, colDir);
		}
		else
		{
			// Create a new map view
			var pos = world.getRegionPosition(mapIndex.row + row, mapIndex.col + col);
			var seed = world.getRegionSeed(mapIndex.row + row, mapIndex.col + col);

			mapView[row][col] = new MapView(map, pos.z, pos.x, seed);
			container.add(mapView[row][col].root);

			var o = mapView[row][col].getBoundary();
			for (var i = 0; i < moveableList.length; i++)
			{
				moveableList[i].addObstacle(o);
			}
			return true;
		}
	}

	function updateMiniMap(row, col)
	{
		if (mapView[row][col] !== null)
		{
			if (miniMapView[row][col] !== null)
			{
				miniWorld.remove(miniMapView[row][col]);
				// miniMapView[row][col].geometry.dispose();
			}

			miniMapView[row][col] = mapView[row][col].cloneCeiling(miniWorldMaterial);

			updateMiniMapPosition(row, col);
			miniWorld.add(miniMapView[row][col]);
			// geometry.uvsNeedUpdate = true;
			// geometry.normalsNeedUpdate = true;
		}
	}

	function updateMiniMapPosition(row, col)
	{
		if (miniMapView[row][col] !== null)
		{
			var x = col * Region.SIZE * World.SCALE;
			var y = row * Region.SIZE * World.SCALE;
			miniMapView[row][col].position.set(x, -y, 0);
		}
	}

	function addToUpdateQueue(row, col, rowDir, colDir)
	{
		var rowDir = rowDir || 0;
		var colDir = colDir || 0;

		for (var i = 0; i < updateQueue.length; i++)
		{
			if (updateQueue[i].row == row && updateQueue[i].col == col)
			{
				updateQueue[i].rowDir += rowDir;
				updateQueue[i].colDir += colDir;
				return;
			}
		}
		updateQueue.push({
			row : row,
			col : col,
			rowDir : rowDir,
			colDir : colDir,
		});
	}

	function moveToward(rowDir, colDir)
	{
		rowDir = Math.sign(rowDir);
		colDir = Math.sign(colDir);

		for (var i = -1; i <= 1; i++)
		{
			// Destruct map in backward direction
			var br = (rowDir == 0) ? i : -rowDir;   // back row index
			var bc = (colDir == 0) ? i : -colDir;   // back column index
			if (mapView[br][bc] != null)
			{
				var o = mapView[br][bc].getBoundary();
				for (var n = 0; n < moveableList.length; n++)
				{
					moveableList[n].removeObstacle(o);
				}
				mapView[br][bc].destruct();
			}

			// Set map in the backward direction to the current middle map
			var mr = (rowDir == 0) ? i : 0;         // middle row index
			var mc = (colDir == 0) ? i : 0;         // middle column index
			mapView[br][bc] = mapView[mr][mc];

			// Set the direction that the current middle map was last updated
			if (mapView[mr][mc] != null)
			{
				mapView[mr][mc].setLastDirection(-rowDir, -colDir);
			}

			// Set middle map to the current map in the forward direction
			var fr = (rowDir == 0) ? i : rowDir;    // front row index
			var fc = (colDir == 0) ? i : colDir;    // front column index
			mapView[mr][mc] = mapView[fr][fc];

			// Clear map in the forward direction
			mapView[fr][fc] = null;

			// Apply the same procedure to the mini world
			if (usingMiniWorld)
			{
				if (miniMapView[br][bc] != null)
				{
					miniWorld.remove(miniMapView[br][bc]);
					// miniMapView[br][bc].geometry.dispose();
				}

				miniMapView[br][bc] = miniMapView[mr][mc];
				updateMiniMapPosition(br, bc);

				miniMapView[mr][mc] = miniMapView[fr][fc];
				updateMiniMapPosition(mr, mc);

				miniMapView[fr][fc] = null;
			}

			// Add middle map to the update queue
			addToUpdateQueue(mr, mc, rowDir, colDir);

			// Add forward map to the update queue
			addToUpdateQueue(fr, fc);
		}
	}

	this.addMoveable = function(moveable)
	{
		moveableList.push(moveable);
		for (var i = -1; i <= 1; i++)
		{
			for (var j = -1; j <= 1; j++)
			{
				if (mapView[i][j] != null)
				{
					var o = mapView[i][j].getBoundary();
					moveable.addObstacle(o);
				}
			}
		}
	};

	this.setMiniWorldMaterial = function(material)
	{
		miniWorldMaterial = material;

		if (usingMiniWorld)
		{
			for (var i = -1; i <= 1; i++)
			{
				for (var j = -1; j <= 1; j++)
				{
					updateMiniMap(i, j);
				}
			}
		}
	};

	this.getMiniWorld = function()
	{
		return miniWorld;
	};

	this.update = function()
	{
		if (updateQueue.length > 0)
		{
			tickCount ++;
			if (tickCount == updateInterval)
			{
				tickCount = 0;
				var event = updateQueue.shift();
				var hasChanges = updateMap(event.row, event.col, event.rowDir, event.colDir);

				if (usingMiniWorld && hasChanges)
				{
					updateMiniMap(event.row, event.col);
				}
			}
		}
	};

	this.clean = function()
	{
		var mapIndex = world.getCurrentRegionIndex();
		var pos = world.getRegionPosition(mapIndex.row, mapIndex.col);
		var distance = Region.SIZE * World.SCALE * 3 / 2;
		
		MapView.cleanSharedVertices(pos.z, pos.x, distance, container);
	};

	this.move = function(rowDir, colDir)
	{
		if (rowDir)
			moveToward(rowDir, 0);

		if (colDir)
			moveToward(0, colDir);
	};
}
