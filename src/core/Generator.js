/**
 * @class			Generator
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Holds static functions for generating and mutating two dimensional 
 *                  data sets using various algorithms.
 */
Generator = (function() {

	/**
	 * Class Constructor
	 */
	function Generator() {}

	/**
	 * Checks if the specified element exists within a 2D array of data.
	 * 
	 * @param  {Array}		data	The 2D array of data.
	 * @param  {Number}		row     The row of the array to check.
	 * @param  {Number}		col     The column of the array to check.
	 * @return {Boolean}			Whether the element is not null.
	 */
	function elementExists(data, row, col)
	{
		return (data != null && data[row] != null && data[row][col] != null);
	}

	/**
	 * Gets a random element from a list of data.
	 * 
	 * @param  {Array} data		The array pick an element from.
	 * @return {var}
	 */
	function randomElement(data, random)
	{
		return data[ Math.floor(random() * data.length) ];
	}

	/**
	 * Checks if a given position is in the range of a data set.
	 * 
	 * @param  {Array}   data The 2D array of data.
	 * @param  {Number}  row  The row of the position.
	 * @param  {Number}  col  The column of the position.
	 * @return {Boolean}      True if in range, false otherwise.
	 */
	function inRange(data, row, col)
	{
		return checkIndex(data, row) === 0 && checkIndex(data[0], col) === 0;
	}

	/**
	 * Compares the index of an array to the range of the data. Output is 0 if
	 * the index is within range, -1 if below the range, and 1 if above the range.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} index   The index to compare.
	 * @return {Number}			The result of the comparison. {-1, 0, 1}
	 */
	function checkIndex(data, index)
	{
		if (index < 0)
		{
			return -1;
		}
		else if (index >= data.length)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Wraps an index of an array around the range of the data.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} index   The index to wrap.
	 * @return {Number}			The new index.
	 */
	function wrapIndex(data, index)
	{
		if (index < 0)
		{
			return index + (data.length - 1);
		}
		else if (index >= data.length)
		{
			return index - (data.length - 1);
		}
		else
		{
			return index;
		}
	}

	/**
	 * Localizes a specified location within a 2D array of 2D data by shifting the location when 
	 * the inner location is out of range. The inner data sets should also be 2D arrays but they 
	 * are not accessed within this function.
	 * 
	 * @param  {Array}  data		The 2D array of data.
	 * @param  {Number} outerRow	The outer row location.
	 * @param  {Number} outerCol	The outer column location.
	 * @param  {Number} innerRow	The inner row location.
	 * @param  {Number} innerCol	The inner column location.
	 * @return {Object}				An object containing the local data set and the new inner location.
	 */
	function localize(data, outerRow, outerCol, innerRow, innerCol)
	{
		if (!elementExists(data, outerRow, outerCol))
			return null;

		var check, temp = data[outerRow][outerCol];

		// Localize row index
		check = checkIndex(temp, innerRow);
		while (check !== 0)
		{
			outerRow += check;
			innerRow -= check * temp.length;
			check = checkIndex(temp, innerRow);
		}

		// Localize column index
		check = checkIndex(temp, innerCol);
		while (check !== 0)
		{
			outerCol += check;
			innerCol -= check * temp.length;
			check = checkIndex(temp, innerCol);
		}

		if (elementExists(data, outerRow, outerCol))
		{
			return {
				data: data[outerRow][outerCol],
				row: innerRow,
				col: innerCol,
			};
		}
		else
		{
			return null;
		}
	}

	/**
	 * [getElement description]
	 * 
	 * @param  {[type]} regList    [description]
	 * @param  {[type]} regRow     [description]
	 * @param  {[type]} regCol     [description]
	 * @param  {[type]} row        [description]
	 * @param  {[type]} col        [description]
	 * @param  {[type]} layerIndex [description]
	 * @return {[type]}            [description]
	 */
	Generator.getElement = function(regList, regRow, regCol, row, col, layerIndex)
	{
		var local = localize(regList, regRow, regCol, row, col);

		if (local != null)
			return local.data.getElement(local.row, local.col, layerIndex);
		else
			return null;
	};

	/**
	 * [setElement description]
	 * 
	 * @param {[type]} regList    [description]
	 * @param {[type]} regRow     [description]
	 * @param {[type]} regCol     [description]
	 * @param {[type]} row        [description]
	 * @param {[type]} col        [description]
	 * @param {[type]} layerIndex [description]
	 * @param {[type]} val        [description]
	 */
	Generator.setElement = function(regList, regRow, regCol, row, col, layerIndex, val)
	{
		var local = localize(regList, regRow, regCol, row, col);

		if (local != null)
			local.data.setElement(local.row, local.col, val, layerIndex);
	};

	/**
	 * Gets a random value that can be used seed any random generation.
	 * 
	 * @return {String}
	 */
	Generator.getSeed = function()
	{
		return Math.seedrandom(null, { pass: function(prng, seed) {
			return seed;
		}});
	};

	/**
	 * [getRandomData description]
	 * 
	 * @param  {[type]} size         [description]
	 * @param  {[type]} distribution [description]
	 * @param  {[type]} seed         [description]
	 * @return {[type]}              [description]
	 */
	Generator.getRandomData = function(size, distribution, options)
	{
		options = options || {};
		var random = ('seed' in options) ? new Math.seedrandom(options.seed) : Math.random;
		var low = ('low' in options) ? options.low : 0;
		var high = ('high' in options) ? options.high : 1;

		var data = new Array(size);
		for (var i = 0; i < data.length; i++)
		{
			data[i] = new Array(size);
			for (var j = 0; j < data[i].length; j++)
			{
				data[i][j] = (random() < distribution) ? high : low;
			}
		}
		return data;
	};

	/**
	 * Gets a random row and column position of a 2D array that matches a given value.
	 * All matching elements are equally likely to be chosen.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} value	The value to search for.
	 * @param  {String} seed    The seed value for the random generator.
	 * @return {Object}			An object containing a row and column. Null if no match is found.
	 */
	Generator.getRandomPosition = function(data, value, seed)
	{
		var random = (seed == null) ? Math.random : new Math.seedrandom(seed);
		var elements = [];

		for (var i = 0; i < data.length; i++)
		{
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == value)
				{
					elements.push({ row : i, col : j });
				}
			}
		}

		if (elements.length == 0)
		{
			return null;
		}
		else
		{
			var index = Math.floor(random() * (elements.length - 1));
			return elements[index];
		}
	};

	/**
	 * Counts all elements in the data that match the specified value.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} value	The value to search for.
	 * @return {Number}			The number of elements in the data that match the value.
	 */
	Generator.countElements = function(data, value)
	{
		var count = 0;
		for (var i = 0; i < data.length; i++)
		{
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == value)
					count ++;
			}
		}
		return count;
	};

	/**
	 * Finds a random element on a spedified edge of a data set. There are four possible edges,
	 * the top and bottom row, and the left and right column. Any element on the far edge has
	 * an equal probability of being chosen.
	 *
	 * @param  {Array}    data    The 2D array of data.
	 * @param  {Number}   target  The target value to search for.
	 * @param  {Boolean}  rowDir  The row direction of the search. {-1, 0, 1}
	 * @param  {Boolean}  colDir  The column direction of the search. {-1, 0, 1}
	 * @return {Object}           An object containing the row and col. Null if not found.
	 */
	Generator.findFromEdge = function(data, target, rowDir, colDir, seed)
	{
		var random = (seed === null) ? Math.random : new Math.seedrandom(seed);
		var positive = (rowDir > 0 || colDir > 0);
		var start = (positive) ? 0 : data.length - 1;
		var d = (positive) ? 1 : -1;

		for (var i = start; i >= 0 && i < data.length; i += d)
		{
			var list = [];
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] == target && colDir === 0)
				{
					list.push({ row: i, col: j });
				}
				else if (data[j][i] == target && rowDir === 0)
				{
					list.push({ row: j, col: i });
				}
			}
			if (list.length > 0)
			{
				var index = Math.round(random() * (list.length - 1));
				return list[index];
			}
		}
		return null;
	};

	/**
	 * [applyCellAutomata description]
	 * 
	 * @param  {[type]} data    [description]
	 * @param  {[type]} born    [description]
	 * @param  {[type]} survive [description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	Generator.applyCellAutomata = function(data, born, survive, options)
	{
		options = options || {};
		var passCount = options.passCount || 1;
		var neighborhoodSize = options.neighborhoodSize || 1;
		var regList = options.regList || null;
		var regRow = ('regRow' in options) ? options.regRow : null;
		var regCol = ('regCol' in options) ? options.regCol : null;
		var layerIndex = ('layerIndex' in options) ? options.layerIndex : null;
		var live = ('live' in options) ? options.live : 1;
		var dead = ('dead' in options) ? options.dead : 0;

		var newData = data;
		for (var n = 0; n < passCount; n++)
		{
			var tempData = new Array(data.length);
			for (var i = 0; i < data.length; i++)
			{
				tempData[i] = new Array(data[i].length);
				for (var j = 0; j < data[i].length; j++)
				{
					tempData[i][j] = generateAt(newData, i, j);
				}
			}
			newData = tempData;
		}

		function generateAt(data, row, col)
		{
			var neighborCount = 0;
			for (var i = -neighborhoodSize; i <= neighborhoodSize; i++)
			{
				for (var j = -neighborhoodSize; j <= neighborhoodSize; j++)
				{
					if (i == 0 && j == 0)
						continue;

					if (checkIndex(data, row + i) == 0 && checkIndex(data, col + j) == 0)
					{
						if (data[row + i][col + j] == live)
						{
							neighborCount ++;
						}
					}
					else if (regList != null && Generator.getElement(regList, regRow, regCol, row + i, col + j, layerIndex) == live) 
					{
						neighborCount ++;
					}
				}
			}

			if (neighborCount >= born)
			{
				return live;
			}
			else if (neighborCount >= survive && data[row][col] == live)
			{
				return live;
			}
			else
			{
				return dead;
			}
		}

		return newData;
	};

	/**
	 * [floodFill description]
	 * 
	 * @param  {[type]} data        [description]
	 * @param  {[type]} row         [description]
	 * @param  {[type]} col         [description]
	 * @param  {[type]} target      [description]
	 * @param  {[type]} replacement [description]
	 * @param  {[type]} options     [description]
	 */
	Generator.floodFill = function(data, row, col, target, replace, options)
	{
		options = options || {};
		var spread = ('spread' in options) ? options.spread : 1;
		var decay = ('decay' in options) ? options.decay : 1;
		var useCardinals = ('useCardinals' in options) ? options.useCardinals : false;
		var regList = ('regList' in options) ? options.regList : null;
		var regRow = ('regRow' in options) ? options.regRow : null;
		var regCol = ('regCol' in options) ? options.regCol : null;
		var fromLayer = ('fromLayer' in options) ? options.fromLayer : null;
		var toLayer = ('toLayer' in options) ? options.toLayer : null;

		var random = options.random || (('seed' in options) ? new Math.seedrandom(options.seed) : Math.random);

		var directions = (useCardinals) ? Direction.CARDINALS : Direction.NEIGHBORS;

		function floodFillRecursive(row, col, spread)
		{
			var fromElement, toElement;

			if (regList == null)
			{
				if (checkIndex(data, row) == 0 && checkIndex(data, col) == 0)
				{
					fromElement = toElement = data[row][col];
				}
				else
					return;
			}
			else
			{
				fromElement = Generator.getElement(regList, regRow, regCol, row, col, fromLayer);
				toElement = Generator.getElement(regList, regRow, regCol, row, col, toLayer);
			}
			
			if (toElement == replace)
			{
				return;
			}
			else if (fromElement == target)
			{
				if (regList == null)
				{
					data[row][col] = replace;
				}
				else
				{
					Generator.setElement(regList, regRow, regCol, row, col, toLayer, replace);
				}

				for (var d in directions)
				{
					var dir = directions[d];

					if (dir.row == 0 && dir.col == 0)
					{
						continue;
					}
					else if (spread == 1 || random() < spread)
					{
						floodFillRecursive(row + dir.row, col + dir.col, spread * decay);
					}
				}
			}
		}

		floodFillRecursive(row, col, spread);
	};

	/**
	 * [getHeightMap description]
	 * 
	 * @param  {[type]} scale       [description]
	 * @param  {[type]} roughness   [description]
	 * @param  {[type]} maxDisplace [description]
	 * @param  {[type]} seed        [description]
	 * @return {[type]}             [description]
	 */
	Generator.getHeightMap = function(scale, roughness, maxDisplace, seed)
	{
		var gen = (seed == null) ? null : new Math.seedrandom(seed);
		var size = Math.pow(2, scale) + 1;
		var map = new Array(size);

		for (var i = 0; i < map.length; i++)
		{
			map[i] = new Array(size);
			for (var j = 0; j < map[i].length; j++)
			{
				map[i][j] = 0;
			}
		}

		function calcDisplace(total, displace)
		{
			var random = (gen == null) ? Math.random() : gen();
			return (total / 4) + (random * displace * 2) - displace;
		}

		function squareStep(row, col, unit, displace)
		{
			if (map[row][col] != 0)
				return;

			var total = map[wrapIndex(map, row + unit)][col] + map[wrapIndex(map, row - unit)][col] +
						map[row][wrapIndex(map, col + unit)] + map[row][wrapIndex(map, col - unit)];
			map[row][col] = calcDisplace(total, displace);

			if (row == 0)
			{
				map[map.length - 1][col] = map[row][col];
				return;
			}
			else if (col == 0)
			{
				map[row][map.length - 1] = map[row][col];
				return;
			}
		}

		var unit = (map.length - 1) / 2;
		var displace = maxDisplace;

		while (unit >= 1)
		{
			// Diamond Step
			for (var row = 0; row < map.length - 1; row += unit * 2)
			{
				for (var col = 0; col < map.length - 1; col += unit * 2)
				{
					var total = map[row][col] + map[row][col + (unit * 2)] + 
								map[row + (unit * 2)][col] + map[row + (unit * 2)][col + (unit * 2)];
					map[row + unit][col + unit] = calcDisplace(total, displace);
				}
			}

			// Square Step
			for (var row = 0; row < map.length - 1; row += unit * 2)
			{
				for (var col = 0; col < map.length - 1; col += unit * 2)
				{
					squareStep(row, col + unit, unit, displace);
					squareStep(row + unit, col, unit, displace);
					squareStep(row + unit, col + (unit * 2), unit, displace);
					squareStep(row + (unit * 2), col + unit, unit, displace);
				}
			}

			unit /= 2;
			displace *= roughness;
		}

		return map;
	};

	/**
	 * [growMaze description]
	 * 
	 * @param  {[type]} data    [description]
	 * @param  {[type]} row     [description]
	 * @param  {[type]} col     [description]
	 * @param  {[type]} replace [description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	Generator.growMaze = function(data, row, col, replace, options)
	{
		options = options || {};
		var walkable = ('walkable' in options) ? options.walkable : null;
		var runFactor = ('runFactor' in options) ? options.runFactor : 0.5;
		var random = ('random' in options) ? options.random : Math.random;

		// TO DO : Assert that replace is not equal to walkable.

		var lastDir, nodes = [{ row: row, col: col }];

		data[row][col] = replace;

		while (nodes.length > 0)
		{
			var node = nodes[nodes.length - 1];
			var localDirs = expand(node.row, node.col);

			if (localDirs.length > 0)
			{
				var dir;

				if (localDirs.indexOf(lastDir) != -1 && random() < runFactor)
					dir = lastDir;
				else
					dir = randomElement(localDirs, random);

				var r = node.row + dir.row;
				var c = node.col + dir.col;

				data[r][c] = replace;

				r += dir.row;
				c += dir.col;

				data[r][c] = replace;

				nodes.push({ row: r, col: c });
				lastDir = dir;
			}
			else
			{
				nodes.pop();
				lastDir = null;
			}
		}

		function expand(row, col)
		{
			var localDirs = [];

			for (var d in Direction.CARDINALS)
			{
				var dir = Direction.CARDINALS[d];
				var r = row + (dir.row * 2);
				var c = col + (dir.col * 2);

				if (!inRange(data, r, c))
					continue;

				var isWalkable;

				if (walkable === null)
					isWalkable = (data[r][c] != replace);
				else
					isWalkable = (data[r][c] == walkable);

				if (isWalkable)
					localDirs.push(dir);
			}

			return localDirs;
		}
	};

	/**
	 * [getPaddedMap description]
	 * 
	 * @param  {[type]} regList [description]
	 * @param  {[type]} regRow  [description]
	 * @param  {[type]} regCol  [description]
	 * @param  {[type]} pad     [description]
	 * @return {[type]}         [description]
	 */
	Generator.getPaddedMap = function(regList, regRow, regCol, layerIndex, pad) // TO DO : Fix pad param
	{
		var length = regList[ regRow ][ regCol ].data[ layerIndex ].length;
		var paddedMap = [];

		for (var i = -1; i <= length; i++)
		{
			paddedMap[i] = [];

			for (var j = -1; j <= length; j++)
			{
				paddedMap[i][j] = Generator.getElement(regList, regRow, regCol, i, j, layerIndex);
			}
		}

		return paddedMap;
	};

	/**
	 * [createDungeon description]
	 * 
	 * @param  {[type]} size       [description]
	 * @param  {[type]} wallValue  [description]
	 * @param  {[type]} floorValue [description]
	 * @param  {[type]} options    [description]
	 * @return {[type]}            [description]
	 */
	Generator.createDungeon = function(size, wallValue, floorValue, options)
	{
		options = options || {};
		var minRoomSize = ('minRoomSize' in options) ? options.minRoomSize : 5;
		var maxRoomSize = ('maxRoomSize' in options) ? options.maxRoomSize : 13;
		var roomAttempts = ('roomAttempts' in options) ? options.roomAttempts : 8;
		var connectivity = ('connectivity' in options) ? options.connectivity : 0;
		var random = ('seed' in options) ? new Math.seedrandom(options.seed) : Math.random;

		// TO DO : Assert that min and max room size are odd.
		// TO DO : Assert that roomAttempts is positive.
		
		var NO_CONNECTOR = 0;
		var X_CONNECTOR = 1;
		var Y_CONNECTOR = 2;

		var roomRange = maxRoomSize - minRoomSize;

		var tempWallValue = 0;
		var regionIndex = 1;
		var evenSize = false;

		if (size % 2 == 0)
		{
			size --;
			evenSize = true;
		}

		var data = Generator.getRandomData(size, 1, { high: tempWallValue });

		// Create rooms
		while (roomAttempts > 0)
		{
			if (createRoom())
				regionIndex ++;
			else
				roomAttempts --;
		}

		// Fill gaps with mazes
		for (var i = 1; i < size; i += 2)
		{
			for (var j = 1; j < size; j += 2)
			{
				if (data[i][j] == tempWallValue)
				{
					Generator.growMaze(data, i, j, regionIndex, {
						walkable: tempWallValue,
						random: random,
					});
					regionIndex ++;
				}
			}
		}

		connectRegions(findConnectorNodes());
		prepareData();
		trimEnds(findEndNodes());

		function createRoom()
		{
			var row = Math.floor(random() * (size - 1) / 2) * 2 + 1;
			var col = Math.floor(random() * (size - 1) / 2) * 2 + 1;

			var width = Math.floor(random() * (roomRange - 1) / 2) * 2 + minRoomSize;
			var height = Math.floor(random() * (roomRange - 1) / 2) * 2 + minRoomSize;

			var i, j;

			for (i = 0; i < height; i++)
			{
				for (j = 0; j < width; j++)
				{
					var inRange = (checkIndex(data, row + i) === 0 && checkIndex(data, col + j) === 0);

					if (!inRange || data[row + i][col + j] != tempWallValue)
						return false;
				}
			}
			for (i = 0; i < height; i++)
			{
				for (j = 0; j < width; j++)
				{
					data[row + i][col + j] = regionIndex;
				}
			}
			return true;
		}

		function connectRegions(nodes)
		{
			var regionCount = regionIndex - 1;

			while (regionCount > 1)
			{
				var index = Math.floor(random() * nodes.length);
				var node = nodes.splice(index, 1)[0];

				var north = data[node.row - 1][node.col];
				var south = data[node.row + 1][node.col];
				var east = data[node.row][node.col + 1];
				var west = data[node.row][node.col - 1];

				if (isConnectorNode(node.row, node.col) != NO_CONNECTOR)
				{
					if (node.type == Y_CONNECTOR)
					{
						data[node.row][node.col] = south;
						Generator.floodFill(data, node.row, node.col, south, north);

						regionCount --;
					}
					else if (node.type == X_CONNECTOR)
					{
						data[node.row][node.col] = east;
						Generator.floodFill(data, node.row, node.col, east, west);

						regionCount --;
					}
				}
				else if (north == south && east == west && random() < connectivity) // TO DO : Check for connectors on both sides.
				{
					data[node.row][node.col] = floorValue;
				}
			}
		}

		function findConnectorNodes()
		{
			var nodes = [];

			for (var i = 1; i < size - 1; i++)
			{
				for (var j = 1; j < size - 1; j++)
				{
					var type = isConnectorNode(i, j);

					if (type != NO_CONNECTOR)
						nodes.push({ row: i, col: j, type: type });
				}
			}

			return nodes;
		}

		function isConnectorNode(row, col)
		{
			if (data[row][col] != tempWallValue)
				return NO_CONNECTOR;

			var north = data[row - 1][col];
			var south = data[row + 1][col];
			var east = data[row][col + 1];
			var west = data[row][col - 1];

			if (north != south && north != tempWallValue && south != tempWallValue)
				return Y_CONNECTOR;

			if (east != west && east != tempWallValue && west != tempWallValue)
				return X_CONNECTOR;

			return NO_CONNECTOR;
		}

		function trimEnds(nodes)
		{
			for (var i = 0; i < nodes.length; i++)
			{
				var node = nodes[i];
				var exit = node.exit;

				do
				{
					data[node.row][node.col] = wallValue;
					node.row = exit.row;
					node.col = exit.col;
					exit = isEndNode(node.row, node.col);
				}
				while (exit !== false);
			}
		}

		function findEndNodes()
		{
			var nodes = [];

			for (var i = 0; i < size; i++)
			{
				for (var j = 0; j < size; j++)
				{
					var exit = isEndNode(i, j);

					if (exit !== false)
						nodes.push({ row: i, col: j, exit: exit });
				}
			}

			return nodes;
		}

		function isEndNode(row, col)
		{
			if (data[row][col] == wallValue)
				return false;

			var north = (inRange(data, row - 1, col)) ? data[row - 1][col] : wallValue;
			var south = (inRange(data, row + 1, col)) ? data[row + 1][col] : wallValue;
			var east = (inRange(data, row, col + 1)) ? data[row][col + 1] : wallValue;
			var west = (inRange(data, row, col - 1)) ? data[row][col - 1] : wallValue;

			if (north == floorValue && south == wallValue && east == wallValue && west == wallValue)
				return { row: row - 1, col: col };

			if (north == wallValue && south == floorValue && east == wallValue && west == wallValue)
				return { row: row + 1, col: col };

			if (north == wallValue && south == wallValue && east == floorValue && west == wallValue)
				return { row: row, col: col + 1 };

			if (north == wallValue && south == wallValue && east == wallValue && west == floorValue)
				return { row: row, col: col - 1 };

			return false;
		}

		function prepareData()
		{
			for (var i = 0; i < size; i++)
			{
				for (var j = 0; j < size; j++)
				{
					data[i][j] = (data[i][j] == tempWallValue) ? wallValue : floorValue;
				}
			}

			if (evenSize)
			{
				data[size] = [];
				for (var i = 0; i < size + 1; i++)
				{
					data[size][i] = data[i][size] = wallValue;
				}
			}
		}

		return data;
	};

	/**
	 * Prints a 2D array to the console.
	 * 
	 * @param  {Array} data		The 2D array of data.
	 */
	Generator.print = function(data)
	{
		if (data === null)
			return;

		var str = '';
		for (var i = 0; i < data.length; i++)
		{
			for (var j = 0; j < data[i].length; j++)
			{
				if (data[i][j] === 0)
					str += '. ';
				else
					str += data[i][j] + ' ';
			}
			str += '\n';
		}
		console.log(str);
	};

	return Generator;

})();
