/**
 * @class			Region
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Stores block data for a single region of a given size.
 */
Region = (function() {

	Region.SIZE = 30;

	RegionEvent = Object.freeze({
		BEFORE_GENERATE: 'before-generate',
		AFTER_GENERATE:  'after-generate',
	});

	RegionLayer = Object.freeze({
		BLOCK:  'block',
		FLOOR:  'floor',
		WATER:  'water',
		OBJECT: 'object',
	});

	BlockType = Object.freeze({
		ROCK:  0,
		DIRT:  1,
		STONE: 2,
		NONE:  3,
	});

	/**
	 * Class Constructor
	 */
	function Region()
	{
		this.length = Region.SIZE;
		this.layers = null;
		this.data = null;
		this.seeds = [];

		for (var t in BlockType)
		{
			var type = BlockType[t];
			this.seeds[ type ] = Generator.getSeed();
		}
	}

	/**
	 * [generate description]
	 * 
	 * @param  {[type]} regList [description]
	 * @param  {[type]} regRow  [description]
	 * @param  {[type]} regCol  [description]
	 * @return {[type]}         [description]
	 */
	Region.generate = function(regList, regRow, regCol)
	{
		Region.dispatchEvent({
			type: RegionEvent.BEFORE_GENERATE,
			list: regList,
			row:  regRow,
			col:  regCol,
		});

		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[ d ];
			var current = regList[ regRow + dir.row ][ regCol + dir.col ];

			current.exportLayers();
			current.layers = null;
		}

		Region.dispatchEvent({
			type: RegionEvent.AFTER_GENERATE,
			list: regList,
			row:  regRow,
			col:  regCol,
		});
	};

	/**
	 * Compares the index of an array to the range of the data. Output is 0 if
	 * the index is within range, -1 if below the range, and 1 if above the range.
	 * 
	 * @param  {Array}  data	The 2D array of data.
	 * @param  {Number} index   The index to compare.
	 * @return {Number}			The result of the comparison. {-1, 0, 1}
	 */
	function checkIndex(data, index) // ??
	{
		if (index < 0)
		{
			return -1;
		}
		else if (index >= data.length)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	Region.prototype = {

		constructor: Region,

		exportLayers: function() {},

		initialize: function()
		{
			this.layers = [];
			this.data = [];

			// Initialize region layers
			for (var l in RegionLayer)
			{
				var layer = RegionLayer[l];
				this.data[ layer ] = [];

				for (var i = 0; i < this.length; i++)
				{
					this.data[ layer ][i] = [];
				}
			}
		},

		getElement: function(row, col, layerIndex)
		{
			var data = (typeof layerIndex == 'string') ? this.data : this.layers;

			if (data == null || data[ layerIndex ] == null)
				return null;
			else
				return data[ layerIndex ][row][col];
		},

		setElement: function(row, col, val, layerIndex)
		{
			var data = (typeof layerIndex == 'string') ? this.data : this.layers;

			if (data != null && data[ layerIndex ] != null)
				data[ layerIndex ][row][col] = val;
		},

		clear: function()
		{
			this.layers = null;
			this.data = null;
		},

		getData: function()
		{
			return (this.data == null) ? null : this.data;
		},

		getLayer: function(i)
		{
			return (this.layers == null) ? null : this.layers[ i || 0 ];
		},

		getSeed: function(i)
		{
			return (this.seeds == null) ? null : this.seeds[ i || 0 ];
		},

		getCellCount: function()
		{
			return this.length * this.length;
		},

		getIndex: function(row, col)
		{
			if (checkIndex(row, col))
			{
				return (row * this.length) + col;
			}
			else
				return -1;
		},

		getRowCol: function(i)
		{
			var r = Math.floor(i / this.length);
			var c = i % this.length;
			return { row: r, col: c };
		},

		getCellValue: function(i)
		{
			var pos = this.getRowCol(i);
			if (checkIndex(pos.row, pos.col))
			{
				return this.data[ pos.row ][ pos.col ];
			}
			else
				return -1;
		},

		print: function(layerIndex)
		{
			Generator.print(this.data[ layerIndex ]);
		},
	};

	THREE.EventDispatcher.prototype.apply(Region);

	return Region;

})();
