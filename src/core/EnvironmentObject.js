/**
 * @class			EnvironmentObject
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		
 */
EnvironmentObject = (function() {

	/**
	 * Class Constructor
	 */
	function EnvironmentObject()
	{
		THREE.Object3D.call(this);

		this.tick = 0;
	}

	EnvironmentObject.generate = function(grid, outerRow, outerCol, innerRow, innerCol) {};

	EnvironmentObject.prototype = Object.create(THREE.Object3D.prototype);

	EnvironmentObject.prototype.constructor = EnvironmentObject;

	/**
	 * Updates the environment object for the next frame.
	 */
	EnvironmentObject.prototype.update = function()
	{
		this.tick ++;
	};

	THREE.EventDispatcher.prototype.apply(EnvironmentObject.prototype);

	return EnvironmentObject;

})();
