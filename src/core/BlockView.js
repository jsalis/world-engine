/**
 * @class			BlockView
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Builds geometry to represent a single element of region data.
 */
BlockView = (function() {

	BlockViewLayer = Object.freeze({
		CEILING:  'ceiling',
		FLOOR:    'floor',
		WATER:    'water',
		WALL:     'wall',
		BOUNDARY: 'boundary',
	});

	/**
	 * @constant {Integer}  The scale of the wall height.
	 */
	BlockView.HEIGHT_SCALE = 2;

	/**
	 * @constant {Integer}  The maximum number of block neighbors (equal to Moore neighborhood).
	 */
	var MAX_NEIGHBORS = 8;

	/**
	 * @var {Object}  The object containing block configuration variables.
	 */
	var bc = {};

	/**
	 * Class Constructor
	 */
	function BlockView() {}

	/**
	 * Creates a block mesh for a single element in the region data. The geometry depends on the 
	 * value of the element and its immediate neighbors.
	 * 
	 * @param  {Array}    data  A 2D array of region data.
	 * @param  {Integer}  row   The row position of the block.
	 * @param  {Integer}  col   The column position of the block.
	 */
	BlockView.generate = function(data, row, col)
	{
		configure(data, row, col);

		var result = {};

		result.geometry = [];
		result.matrix = [];

		generateWall(result);
		generateCeiling(result);
		generateFloor(result);
		generateWater(result);

		return result;
	};

	/**
	 * Interprets the region data around the given row and column, and configures the static variables 
	 * used for block generation.
	 * 
	 * @param  {Array}    data  A 2D array of region data.
	 * @param  {Integer}  row   The row position of the block.
	 * @param  {Integer}  col   The column position of the block.
	 */
	function configure(data, row, col)
	{
		// Get the data layers
		var blockData = data[ RegionLayer.BLOCK ];
		var waterData = data[ RegionLayer.WATER ];
		var floorData = data[ RegionLayer.FLOOR ];

		// Set the block row, column, and type
		bc.row = row;
		bc.col = col;
		bc.type = blockData[ row ][ col ];

		// Set flag for the water
		bc.hasWater = waterData[ row ][ col ];

		// Set flags for the center and the neighbors
		bc.center = floorData[ row ][ col ];
		bc.north = floorData[ row - 1 ][ col ];
		bc.south = floorData[ row + 1 ][ col ];
		bc.west = floorData[ row ][ col - 1 ];
		bc.east = floorData[ row ][ col + 1 ];

		bc.northWest = floorData[ row - 1 ][ col - 1 ];
		bc.northEast = floorData[ row - 1 ][ col + 1 ];
		bc.southWest = floorData[ row + 1 ][ col - 1 ];
		bc.southEast = floorData[ row + 1 ][ col + 1 ];

		// Set flags for the four possible tip configurations
		var nn = !bc.north && bc.west && bc.south && bc.east;
		var ss = bc.north && bc.west && !bc.south && bc.east;
		var ww = bc.north && !bc.west && bc.south && bc.east;
		var ee = bc.north && bc.west && bc.south && !bc.east;

		bc.isTip = (nn || ss || ww || ee) && !bc.center;

		// Set flags for the four possible corner configurations
		var nw = !bc.north && !bc.west && bc.south && bc.east;
		var ne = !bc.north && !bc.east && bc.south && bc.west;
		var sw = !bc.south && !bc.west && bc.north && bc.east;
		var se = !bc.south && !bc.east && bc.north && bc.west;

		bc.isCorner = (nw || ne || sw || se) && !bc.center;

		bc.colorCorner = (nw && !bc.northWest) || (ne && !bc.northEast) || (sw && !bc.southWest) || (se && !bc.southEast);

		// Count total neighbors excluding center
		var neighborCount = 0;
		
		for (var i = -1; i <= 1; i++)
		{
			for (var j = -1; j <= 1; j++)
			{
				if (i === 0 && j === 0)
					continue;

				if (!floorData[row + i][col + j])
					neighborCount ++;
			}
		}

		bc.isSide = neighborCount != MAX_NEIGHBORS && !bc.center;

		// Set rotation matrix
		bc.rotation = 0;
		bc.matrix = new THREE.Matrix4();

		if (ss || se)
		{
			bc.rotation = Math.PI;
			bc.matrix.makeRotationY(bc.rotation);
		}
		else if (ww || sw)
		{
			bc.rotation = Math.PI / 2;
			bc.matrix.makeRotationY(bc.rotation);
		}
		else if (ee || ne)
		{
			bc.rotation = Math.PI / -2;
			bc.matrix.makeRotationY(bc.rotation);
		}
	}

	/**
	 * Generates a wall geometry and a matrix based on the block configuration.
	 * 
	 * @param  {Object}  result  The container object.
	 */
	function generateWall(result)
	{
		var geometry, matrix, layer = BlockViewLayer.WALL;

		if (bc.isTip)
		{
			geometry = Resources.wallList[ bc.type ].tip;
		}
		else if (bc.isCorner)
		{
			geometry = Resources.wallList[ bc.type ].corner;
		}
		else if (bc.isSide)
		{
			geometry = new THREE.Geometry();

			var sideGeometry = Resources.wallList[ bc.type ].side;

			if (bc.north)
			{
				matrix = new THREE.Matrix4().makeRotationY(Math.PI);
				geometry.merge(sideGeometry, matrix);
			}

			if (bc.east)
			{
				matrix = new THREE.Matrix4().makeRotationY(Math.PI / 2);
				geometry.merge(sideGeometry, matrix);
			}

			if (bc.west)
			{
				matrix = new THREE.Matrix4().makeRotationY(Math.PI / -2);
				geometry.merge(sideGeometry, matrix);
			}

			if (bc.south)
			{
				geometry.merge(sideGeometry);
			}
		}
		else
			return;

		// Set material index for faces
		for (var n = 0; n < geometry.faces.length; n++)
		{
			geometry.faces[n].materialIndex = bc.type;
		}

		// Set block matrix
		matrix = new THREE.Matrix4().makeTranslation(0, BlockView.HEIGHT_SCALE / 2, 0);
		matrix.multiply(bc.matrix);

		result.geometry[ layer ] = geometry;
		result.matrix[ layer ] = matrix;
	}

	/**
	 * Generates a ceiling geometry and a matrix based on the block configuration.
	 * 
	 * @param  {Object}  result  The container object.
	 */
	function generateCeiling(result)
	{
		var geometry, matrix, layer = BlockViewLayer.CEILING;

		if (bc.isTip)
		{
			geometry = Resources.ceilingList[ bc.type ].tip;
			BasicSideCeiling.computeVertexUvs(geometry, bc.rotation);
		}
		else if (bc.isCorner)
		{
			geometry = Resources.ceilingList[ bc.type ].corner;
			BasicSideCeiling.computeVertexUvs(geometry, bc.rotation);

			geometry.removeColor();

			if (bc.colorCorner)
				geometry.colorCorner();
		}
		else if (bc.isSide)
		{
			geometry = new THREE.Geometry();

			var geo, sideGeometry = Resources.ceilingList[ bc.type ].side;

			matrix = new THREE.Matrix4().makeRotationY(Math.PI);
			geo = (bc.north) ? sideGeometry : Resources.sideCeiling;
			geometry.merge(geo, matrix);

			matrix = new THREE.Matrix4().makeRotationY(Math.PI / 2);
			geo = (bc.east) ? sideGeometry : Resources.sideCeiling;
			geometry.merge(geo, matrix);

			matrix = new THREE.Matrix4().makeRotationY(Math.PI / -2);
			geo = (bc.west) ? sideGeometry : Resources.sideCeiling;
			geometry.merge(geo, matrix);

			geo = (bc.south) ? sideGeometry : Resources.sideCeiling;
			geometry.merge(geo);

			BasicSideCeiling.computeVertexUvs(geometry);

			if (!bc.northWest && !bc.north && !bc.west)
				BasicSideCeiling.colorNorthWest(geometry);

			if (!bc.northEast && !bc.north && !bc.east)
				BasicSideCeiling.colorNorthEast(geometry);

			if (!bc.southWest && !bc.south && !bc.west)
				BasicSideCeiling.colorSouthWest(geometry);

			if (!bc.southEast && !bc.south && !bc.east)
				BasicSideCeiling.colorSouthEast(geometry);
		}
		else if (!bc.center)
		{
			geometry = Resources.fullCeiling;
		}
		else
			return;

		// Set material index for faces
		for (var n = 0; n < geometry.faces.length; n++)
		{
			geometry.faces[n].materialIndex = bc.type;
		}

		// Set block matrix
		matrix = new THREE.Matrix4().makeTranslation(0, BlockView.HEIGHT_SCALE, 0);
		matrix.multiply(bc.matrix);

		result.geometry[ layer ] = geometry;
		result.matrix[ layer ] = matrix;
	}

	/**
	 * Generates a floor geometry and a matrix based on the block configuration.
	 * 
	 * @param  {Object}  result  The container object.
	 */
	function generateFloor(result)
	{
		if (!bc.isTip && !bc.isCorner && !bc.isSide && !bc.center)
			return;

		var layer = BlockViewLayer.FLOOR;
		var mat = Resources.materialList[ layer ].materials[ bc.type ];
		var rowRepeat = ('rowRepeat' in mat) ? mat.rowRepeat : 1;
		var colRepeat = ('colRepeat' in mat) ? mat.colRepeat : 1;

		BasicSideCeiling.computeVertexUvs(Resources.basicPlane, 0, bc.row, bc.col, rowRepeat, colRepeat);
		
		result.geometry[ layer ] = Resources.basicPlane;

		// Set material index for faces
		for (var n = 0; n < result.geometry[ layer ].faces.length; n++)
		{
			result.geometry[ layer ].faces[n].materialIndex = bc.type;
		}
	}

	/**
	 * Generates a water geometry and a matrix based on the block configuration.
	 * 
	 * @param  {Object}  result  The container object.
	 */
	function generateWater(result)
	{
		if (bc.hasWater)
			result.geometry[ BlockViewLayer.WATER ] = Resources.basicPlane;
	}

	return BlockView;

})();
