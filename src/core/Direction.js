/**
 * @class			Direction
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Extension of THREE.Vector2 that represents grid directions.
 */
Direction = (function() {

	/**
	 * Class Constructor
	 * 
	 * @param  {Number}  row  The row direction.
	 * @param  {Number}  col  The column direction.
	 */
	function Direction(row, col)
	{
		THREE.Vector2.call(this, col, row);
	}

	Direction.prototype = Object.create(THREE.Vector2.prototype);

	Direction.prototype.constructor = Direction;

	Object.defineProperty(Direction.prototype, 'row', {

		get: function()
		{
			return this.y;
		}
	});

	Object.defineProperty(Direction.prototype, 'col', {

		get: function()
		{
			return this.x;
		}
	});

	Direction.CARDINALS = Object.freeze({
		N: Object.freeze(new Direction(-1, 0)),
		W: Object.freeze(new Direction(0, -1)),
		S: Object.freeze(new Direction(1, 0)),
		E: Object.freeze(new Direction(0, 1)),
	});

	Direction.NEIGHBORS = Object.freeze({
		NW: Object.freeze(new Direction(-1, -1)),
		N:  Object.freeze(new Direction(-1, 0)),
		NE: Object.freeze(new Direction(-1, 1)),
		W:  Object.freeze(new Direction(0, -1)),
		C:  Object.freeze(new Direction(0, 0)),
		E:  Object.freeze(new Direction(0, 1)),
		SW: Object.freeze(new Direction(1, -1)),
		S:  Object.freeze(new Direction(1, 0)),
		SE: Object.freeze(new Direction(1, 1)),
	});

	return Direction;

})();
