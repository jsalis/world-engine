/**
 * @class           VertexShifter
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     
 */
VertexShifter = (function() {

	var ROUGHNESS = 0.32;

	var MAX_DISPLACE = 0.25;

	var SCALE = 2;

	var BASE_OFFSET = 0.35;

	/**
	 * @constant {Integer}  The decimal precision to use when comparing vertex positions.
	 */
	var PRECISION = Math.pow(10, 1);

	/**
	 * @constant {Array}    The list of valid face indices.
	 */
	var FACE_INDICES = ['a', 'b', 'c'];

	/**
	 * @constructor
	 * @param  {Number}  size  The size of geometry chunks. Used to determine which vertices are shared.
	 */
	function VertexShifter(size)
	{
		this.size = size;
		this._map = {};
	}

	/**
	 * Corrects geometry by applying the vertex transformation map.
	 * 
	 * @param  {THREE.Geometry}  geometry     The geometry to correct.
	 * @param  {THREE.Matrix4}   matrixWorld  A matrix to transform a vertex into world coordinates.
	 */
	function _correctGeometry(geometry, matrixWorld)
	{
		var inverseMatrix = new THREE.Matrix4().getInverse(matrixWorld);

		for (var i = 0; i < geometry.vertices.length; i++)
		{
			var vertex = geometry.vertices[ i ];
			var vertexWorld = vertex.clone().applyMatrix4(matrixWorld);
			var key = _encodeKey(vertexWorld);

			if (this._map[ key ] !== undefined)
			{
				var correctVertex = this._map[ key ].clone().applyMatrix4(inverseMatrix);

				vertex.x = correctVertex.x;
				vertex.z = correctVertex.z;
			}
		}
	}

	/**
	 * Transforms vertices of a geometry in the direction of their normals.
	 * 
	 * @param  {THREE.Geometry}  geometry     The geometry to apply the transformation.
	 * @param  {THREE.Matrix4}   matrixWorld  A matrix to transform a vertex into world coordinates.
	 * @param  {String}          seed         A seed for the random generator.
	 */
	function _generateNoise(geometry, matrixWorld, seed, targets)
	{
		var normList = [];
		var ignore = [];

		// Map each vertex index to its normal
		for (var i = 0; i < geometry.faces.length; i++)
		{
			var face = geometry.faces[ i ];
			for (var m = 0; m < FACE_INDICES.length; m++)
			{
				var current = face[ FACE_INDICES[ m ] ];
				normList[ current ] = face.vertexNormals[ m ].clone();
				ignore[ current ] = (targets.indexOf(face.materialIndex) === -1);
			}
		}

		var gen = new Math.seedrandom(seed);
		var heightMap = Generator.getHeightMap(SCALE, ROUGHNESS, MAX_DISPLACE, gen());
		var size = Math.pow(2, SCALE) + 1;
		var index = 0;

		var inverseMatrix = new THREE.Matrix4().getInverse(matrixWorld);

		for (var i = 0; i < geometry.vertices.length; i++)
		{
			var vertex = geometry.vertices[ i ];
			var vertexWorld = vertex.clone().applyMatrix4(matrixWorld);
			var isShared = _isSharedVertex(vertex, this.size);
			var onCeiling = vertex.y > BlockView.HEIGHT_SCALE - (1 / PRECISION);

			var key = _encodeKey(vertexWorld);

			if (this._map[ key ] !== undefined)
			{
				var correctVertex = this._map[ key ].clone().applyMatrix4(inverseMatrix);

				vertex.x = correctVertex.x;
				vertex.z = correctVertex.z;
			}
			else
			{
				if (!ignore[ i ])
				{
					var row = Math.floor(index / size);
					var col = index % size;

					vertex.x += normList[ i ].x * heightMap[ row ][ col ];
					vertex.z += normList[ i ].z * heightMap[ row ][ col ];

					if (vertex.y < (1 / PRECISION))
					{
						vertex.x += normList[ i ].x * BASE_OFFSET;
						vertex.z += normList[ i ].z * BASE_OFFSET;
					}

					if (this._map[ key ] === undefined && (isShared || onCeiling))
					{
						this._map[ key ] = vertex.clone().applyMatrix4(matrixWorld);
					}
				}
				else if (isShared)
				{
					this._map[ key ] = vertex.clone().applyMatrix4(matrixWorld);
				}
			}

			index ++;
			if (index >= size * size)
			{
				heightMap = Generator.getHeightMap(SCALE, ROUGHNESS, MAX_DISPLACE, gen());
				index = 0;
			}
		}

		geometry.computeFaceNormals();
		geometry.computeVertexNormals();
	}

	/**
	 * Removes all shared vertices with a position outside the defined area.
	 * This function is for memory management.
	 * 
	 * @param  {Number}  z         The center Z position.
	 * @param  {Number}  x         The center X position.
	 * @param  {Number}  distance  The distance from the center.
	 */
	function _clean(z, x, distance, container)
	{
		for (var prop in this._map)
		{
			var vertex = _decodeKey(prop);

			if (vertex.x > x + distance || vertex.x < x - distance ||
				vertex.z > z + distance || vertex.z < z - distance)
			{
				delete this._map[ prop ];
			}
		}
	}

	/**
	 * Determines whether a vertex is on the edge of a chunk, where it is shared by other chunks.
	 * 
	 * @param  {THREE.Vector3}  vertex  The vertex to test.
	 * @return {Boolean}                Whether the vertex is shared by multiple chunks.
	 */
	function _isSharedVertex(vertex, size)
	{
		var halfSize = size / 2;

		var farEast = vertex.x > halfSize - 0.1;
		var farWest = vertex.x < -halfSize + 0.1;
		var farNorth = vertex.z < -halfSize + 0.1;
		var farSouth = vertex.z > halfSize - 0.1;

		return farEast || farWest || farNorth || farSouth;
	}

	/**
	 * Encodes a vector into a unique string defined by its position.
	 * 
	 * @param  {THREE.Vector3}  vector  The vector to encode.
	 * @return {String}                 The encoded string.
	 */
	function _encodeKey(vector)
	{
		return Math.round(vector.x * PRECISION) + '_' +
			   Math.round(vector.y * PRECISION) + '_' +
			   Math.round(vector.z * PRECISION);
	}

	/**
	 * Decodes a string into a vector.
	 * 
	 * @param  {String}        string   The string to decode.
	 * @return {THREE.Vector3}          The decoded vector.
	 */
	function _decodeKey(string)
	{
		var parts = string.split('_');
		var vx = parseInt(parts[0], 10) / PRECISION;
		var vy = parseInt(parts[1], 10) / PRECISION;
		var vz = parseInt(parts[2], 10) / PRECISION;

		return new THREE.Vector3(vx, vy, vz);
	}

	VertexShifter.prototype = {
		constructor:     VertexShifter,
		correctGeometry: _correctGeometry,
		generateNoise:   _generateNoise,
		clean:           _clean
	};

	return VertexShifter;

})();
