/**
 * @class           MapView
 * 
 * @author          John Salis <jsalis@stetson.edu>
 * @description     Builds geometry to represent map data and controls the adding and removing 
 *                  of objects to the region.
 */
MapView = (function() {

	/**
	 * @constant {Integer}  The number of chunks to divide the length of the map data into.
	 */
	var DIVISIONS = 2;

	/**
	 * @constant {Integer}  The size of the chunks of map data.
	 */
	var CHUNK_SIZE = Region.SIZE / DIVISIONS;

	/**
	 * @constant {Boolean}  Whether to use a single material for all layers in a chunk.
	 */
	var USE_CHUNK_MATERIAL = false;

	/**
	 * @constant {Boolean}  Whether to set a random color for each chunk material.
	 */
	var COLORIZE_CHUNKS = true;

	/**
	 * @constant {Boolean}  Whether to set the chunk material to wireframe.
	 */
	var WIREFRAME_CHUNKS = true;

	/**
	 * @var {Object}  The object containing vertex positions mapped to translated positions.
	 */
	var vertexShifter = new VertexShifter(CHUNK_SIZE);

	/**
	 * Class Constructor
	 * 
	 * @param  {Array}   map   A 2D array of map data.
	 * @param  {Number}  z     The center Z position.
	 * @param  {Number}  x     The center X position.
	 * @param  {String}  seed  The seed to be used for random generation.
	 */
	function MapView(map, z, x, seed)
	{
		this.seed = seed;

		this._lastRowDir = 0;
		this._lastColDir = 0;

		this.root = new THREE.Object3D();
		this.root.scale.set(World.SCALE, World.SCALE, World.SCALE);
		this.root.position.set(x, 0, z);
		this.root.updateMatrix();
		this.root.updateMatrixWorld();

		this._objects = new THREE.Object3D();
		this.root.add(this._objects);

		this._meshList = [];

		// Initialize mesh layers
		for (var l in BlockViewLayer)
		{
			var layer = BlockViewLayer[ l ];

			this._meshList[ layer ] = new THREE.Object3D();
			this.root.add(this._meshList[ layer ]);
		}

		// Hide the boundary layer
		this._meshList[ BlockViewLayer.BOUNDARY ].visible = false;

		generateMesh(this, map);
	}

	/**
	 * Generates all chunks of mesh and adds them to their respective containers.
	 * 
	 * @param  {MapView}  that  The map view to build on.
	 * @param  {Array}    map   A 2D array of map data.
	 */
	function generateMesh(that, map)
	{
		for (var i = 0; i < Region.SIZE; i += CHUNK_SIZE)
		{
			for (var j = 0; j < Region.SIZE; j += CHUNK_SIZE)
			{
				generateMeshAt(that, map, i, j);
			}
		}
	}

	/**
	 * Builds a chunk of mesh using a section of map data, and adds them to their 
	 * respective containers.
	 * 
	 * @param  {MapView}  that  The map view.
	 * @param  {Array}    map   A 2D array of map data.
	 * @param  {Integer}  row   The root row of the map data.
	 * @param  {Integer}  col   The root column of the map data.
	 */
	function generateMeshAt(that, map, row, col)
	{
		var name = (row / CHUNK_SIZE) + '_' + (col / CHUNK_SIZE);

		removeChunk(that, name);
		createChunk(that, map, row, col, name);
	}

	/**
	 * Removes a chunk with the specified name.
	 * 
	 * @param  {MapView}  that  The map view.
	 * @param  {String}   name  The name of the chunk to remove.
	 */
	function removeChunk(that, name)
	{
		var obj = that._objects.getObjectByName(name);

		if (obj !== undefined)
		{
			that._objects.remove(obj);
		}

		for (var l in BlockViewLayer)
		{
			var layer = BlockViewLayer[ l ];
			var mesh = that._meshList[ layer ];
			obj = mesh.getObjectByName(name);

			if (obj !== undefined)
			{
				mesh.remove(obj);
				obj.geometry.dispose();
			}
		}
	}

	/**
	 * Creates a chunk for a section of the map data. The mesh is positioned according to the 
	 * position of the chunk in the map data. No special transformations are applied to the geometry.
	 *
	 * @param  {MapView}  that  The map view to build on.
	 * @param  {Array}    map   A 2D array of map data.
	 * @param  {Integer}  row   The root row of the map data.
	 * @param  {Integer}  col   The root column of the map data.
	 */
	function createChunk(that, map, row, col, name)
	{
		var geometryList = generateGeometry(that, map, row, col);
		var chunkMaterial = (USE_CHUNK_MATERIAL) ? getChunkMaterial() : null;
		var mesh, meshList = [];

		var x = col + (CHUNK_SIZE / 2) - (Region.SIZE / 2);
		var z = row + (CHUNK_SIZE / 2) - (Region.SIZE / 2);

		for (var l in BlockViewLayer)
		{
			var layer = BlockViewLayer[ l ];

			var geo = geometryList[ layer ];
			var mat = (USE_CHUNK_MATERIAL) ? chunkMaterial : Resources.materialList[ layer ];
			
			geo.mergeVertices();
			geo.computeVertexNormals();
			geo.computeBoundingBox();
			geo.dynamic = false;

			mesh = new THREE.Mesh(geo, mat);
			mesh.name = name;
			mesh.position.x = x;
			mesh.position.z = z;
			mesh.updateMatrix();

			meshList[ layer ] = mesh;

			that._meshList[ layer ].add(mesh);
			that._meshList[ layer ].updateMatrixWorld();
		}

		mesh = meshList[ BlockViewLayer.WALL ];
		vertexShifter.generateNoise(mesh.geometry, mesh.matrixWorld, that.seed, [ BlockType.ROCK, BlockType.DIRT ]);

		mesh = meshList[ BlockViewLayer.CEILING ];
		vertexShifter.correctGeometry(mesh.geometry, mesh.matrixWorld);
		
		var objectData = map[ RegionLayer.OBJECT ];

		var container = new THREE.Object3D();
		container.name = name;
		container.position.x = x;
		container.position.z = z;
		container.updateMatrix();
		that._objects.add(container);

		for (var i = row; i < row + CHUNK_SIZE; i++)
		{
			for (var j = col; j < col + CHUNK_SIZE; j++)
			{
				var objectType = objectData[ i ][ j ];

				if (objectType)
				{
					var instance = new objectType();

					var bz = (i - row) + (1 / 2) - (CHUNK_SIZE / 2);
					var bx = (j - col) + (1 / 2) - (CHUNK_SIZE / 2);
					var transMatrix = new THREE.Matrix4().makeTranslation(bx, 0, bz);

					instance.applyMatrix(transMatrix);
					container.add(instance);
				}
			}
		}
	}

	/**
	 * Creates a new chunk material.
	 * 
	 * @return {THREE.Material}
	 */
	function getChunkMaterial()
	{
		var material = new THREE.MeshBasicMaterial({
			wireframe: WIREFRAME_CHUNKS,
			vertexColors: THREE.VertexColors,
		});

		if (COLORIZE_CHUNKS)
			material.color.setHex(Math.random() * 0xffffff);

		return material;
	}

	/**
	 * Generates geometry for each layer using a section of the map data.
	 * 
	 * @param  {MapView}         that  The map view to build on.
	 * @param  {Array}           map   A 2D array of map data.
	 * @param  {Integer}         row   The root row of the map data.
	 * @param  {Integer}         col   The root column of the map data.
	 * @return {THREE.Geometry}        The array of geometry for each layer.
	 */
	function generateGeometry(that, map, row, col)
	{
		var geometryList = [];

		// Initialize geometry for each layer
		for (var l in BlockViewLayer)
		{
			var layer = BlockViewLayer[ l ];
			geometryList[ layer ] = new THREE.Geometry();
		}

		// Build geometry block by block
		for (var i = row; i < row + CHUNK_SIZE; i++)
		{
			for (var j = col; j < col + CHUNK_SIZE; j++)
			{
				var bz = (i - row) + (1 / 2) - (CHUNK_SIZE / 2);
				var bx = (j - col) + (1 / 2) - (CHUNK_SIZE / 2);

				var transMatrix = new THREE.Matrix4().makeTranslation(bx, 0, bz);

				var block = BlockView.generate(map, i, j);

				for (var l in BlockViewLayer)
				{
					var layer = BlockViewLayer[ l ];
					var blockMatrix = block.matrix[ layer ];
					var blockGeometry = block.geometry[ layer ];

					if (blockMatrix !== undefined)
					{
						blockMatrix.multiplyMatrices(transMatrix, blockMatrix);
					}

					if (blockGeometry !== undefined)
					{
						var matrix = (blockMatrix !== undefined) ? blockMatrix : transMatrix;
						geometryList[ layer ].merge(blockGeometry, matrix);
					}
				}
			}
		}

		// Set the boundary layer as a copy of the wall layer
		geometryList[ BlockViewLayer.BOUNDARY ] = geometryList[ BlockViewLayer.WALL ].clone();

		return geometryList;
	}

	/**
	 * Removes all shared vertices with a position outside the defined area.
	 * This function is for memory management.
	 * 
	 * @param  {Number}  z         The center Z position.
	 * @param  {Number}  x         The center X position.
	 * @param  {Number}  distance  The distance from the center.
	 */
	MapView.cleanSharedVertices = function(z, x, distance, container)
	{
		vertexShifter.clean(z, x, distance, container);
	};

	MapView.prototype = {

		constructor: MapView,

		/**
		 * Partially updates the map view in a particular direction. Only chunks in the far row direction or far column
		 * direction are updated. If the given direction is the same as the last direction then no updates occur.
		 * 
		 * @param  {Array}    map     A 2D array of map data.
		 * @param  {Integer}  rowDir  The row direction to update. {-1, 0, 1}
		 * @param  {Integer}  colDir  The column direction to update. {-1, 0, 1}
		 * @return {Boolean}          True if changes were made to the view, false otherwise.
		 */
		updateToward: function(map, rowDir, colDir)
		{
			var updated = (rowDir == this._lastRowDir && colDir == this._lastColDir);
			var noDir = (rowDir == 0 && colDir == 0);

			if (updated || noDir)
				return false;

			for (var i = 0; i < DIVISIONS; i++)
			{
				for (var j = 0; j < DIVISIONS; j++)
				{
					var a = rowDir > 0 && i == DIVISIONS - 1;
					var b = rowDir < 0 && i == 0;
					var c = colDir > 0 && j == DIVISIONS - 1;
					var d = colDir < 0 && j == 0;

					if (a || b || c || d)
					{
						var row = i * CHUNK_SIZE;
						var col = j * CHUNK_SIZE;

						generateMeshAt(this, map, row, col);
					}
				}
			}

			// Set the last direction that was updated
			this._lastRowDir = rowDir;
			this._lastColDir = colDir;

			return true;
		},

		/**
		 * Sets the last direction that the map view was updated in. If the map view is updated in this direction
		 * then no changes will be made. 
		 * 
		 * @param  {Integer}  rowDir  The row direction.
		 * @param  {Integer}  colDir  The column direction.
		 */
		setLastDirection: function(rowDir, colDir)
		{
			this._lastRowDir = rowDir;
			this._lastColDir = colDir;
		},

		/**
		 * Gets a reference to the mesh representing the boundary.
		 * 
		 * @return  {THREE.Mesh}  The boundary mesh.
		 */
		getBoundary: function()
		{
			return this._meshList[ BlockViewLayer.BOUNDARY ];
		},

		/**
		 * Creates a clone of the ceiling geometry. Essentially a 2D representation of the 
		 * map view structure.
		 *
		 * @param   {THREE.Material}  material  The material to apply to the cloned ceiling.
		 * @return  {THREE.Object3D}            A container with the cloned ceiling mesh.
		 */
		cloneCeiling: function(material)
		{
			var ceiling = this._meshList[ BlockViewLayer.CEILING ];
			var root = new THREE.Object3D();

			root.scale.set(World.SCALE, World.SCALE, World.SCALE);
			root.rotation.x = Math.PI / 2;

			for (var i = 0; i < ceiling.children.length; i++)
			{
				var child = ceiling.children[i];
				var mesh = new THREE.Mesh(child.geometry.clone(), material);

				mesh.position.copy(child.position);
				root.add(mesh);
			}

			return root;
		},

		/**
		 * Removes the root from its parent and disposes all geometry.
		 */
		destruct: function()
		{
			if (this.root.parent !== undefined)
				this.root.parent.remove(this.root);

			for (var l in BlockViewLayer)
			{
				var layer = BlockViewLayer[ l ];
				var mesh = this._meshList[ layer ];

				for (var i = 0; i < mesh.children.length; i++)
				{
					mesh.children[i].geometry.dispose();
				}
			}
		},
	};

	return MapView;

})();
