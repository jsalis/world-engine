/**
 * @class			SolidRegion
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Extension of Region that generates a solid rock environment.
 */
SolidRegion = (function() {

	/**
	 * Class Constructor
	 */
	function SolidRegion()
	{
		Region.call(this);
	}

	SolidRegion.prototype = Object.create(Region.prototype);

	SolidRegion.prototype.constructor = SolidRegion;

	SolidRegion.prototype.exportLayers = function()
	{
		Region.prototype.exportLayers.call(this);

		for (var i = 0; i < this.length; i++)
		{
			for (var j = 0; j < this.length; j++)
			{
				this.data[ RegionLayer.BLOCK ][i][j] = BlockType.ROCK;
				this.data[ RegionLayer.FLOOR ][i][j] = false;
				this.data[ RegionLayer.WATER ][i][j] = false;
				this.data[ RegionLayer.OBJECT ][i][j] = null;
			}
		}
	};

	return SolidRegion;

})();
