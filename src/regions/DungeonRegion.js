/**
 * @class			DungeonRegion
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Extension of Region that generates a dungeon environment.
 */
DungeonRegion = (function() {

	var MAX_FLOOR_EXTEND = 8;

	/**
	 * Class Constructor
	 */
	function DungeonRegion()
	{
		Region.call(this);
	}
	
	Region.addEventListener(RegionEvent.AFTER_GENERATE, function(e) {

		extendFloor(e.list, e.row, e.col);
	});

	/**
	 * [extendFloor description]
	 * 
	 * @param  {[type]} regList [description]
	 * @param  {[type]} regRow  [description]
	 * @param  {[type]} regCol  [description]
	 * @return {[type]}         [description]
	 */
	function extendFloor(regList, regRow, regCol)
	{
		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[d];
			var rr = regRow + dir.row;
			var cc = regCol + dir.col;

			var currentReg = regList[rr][cc];
			var layer = currentReg.data[ RegionLayer.FLOOR ];
			var seed = currentReg.seeds[ BlockType.STONE ];

			if (currentReg instanceof DungeonRegion)
			{
				extend(Direction.CARDINALS.N);
				extend(Direction.CARDINALS.W);
				extend(Direction.CARDINALS.S);
				extend(Direction.CARDINALS.E);
			}
		}

		function extend(dir)
		{
			var pos = Generator.findFromEdge(layer, true, -dir.row, -dir.col, seed);
			var next, path = [];

			do
			{
				path.push({ row: pos.row, col: pos.col });
				pos.row += dir.row;
				pos.col += dir.col;
				next = Generator.getElement(regList, rr, cc, pos.row, pos.col, RegionLayer.FLOOR);

				if (next)
				{
					for (var n = 1; n < path.length; n++)
					{
						var p = path[n];

						// Set the element of the current path position
						Generator.setElement(regList, rr, cc, p.row, p.col, RegionLayer.FLOOR, true);
						Generator.setElement(regList, rr, cc, p.row, p.col, RegionLayer.BLOCK, BlockType.STONE);

						// Set the element orthogonal to the current path position (positive)
						Generator.setElement(regList, rr, cc, p.row + dir.col, p.col + dir.row, RegionLayer.FLOOR, false);
						Generator.setElement(regList, rr, cc, p.row + dir.col, p.col + dir.row, RegionLayer.BLOCK, BlockType.STONE);

						// Set the element orthogonal to the current path position (negative)
						Generator.setElement(regList, rr, cc, p.row - dir.col, p.col - dir.row, RegionLayer.FLOOR, false);
						Generator.setElement(regList, rr, cc, p.row - dir.col, p.col - dir.row, RegionLayer.BLOCK, BlockType.STONE);
					}
					break;
				}
			}
			while (next !== null && path.length < MAX_FLOOR_EXTEND);
		}
	}

	DungeonRegion.prototype = Object.create(Region.prototype);

	DungeonRegion.prototype.constructor = DungeonRegion;

	DungeonRegion.prototype.initialize = function()
	{
		Region.prototype.initialize.call(this);

		// Initialize stone layer
		this.layers[ BlockType.STONE ] = Generator.createDungeon(this.length, BlockType.STONE, BlockType.NONE, {
			seed:         this.seeds[ BlockType.STONE ],
			minRoomSize:  5,
			maxRoomSize:  11,
			roomAttempts: 10,
		});
	};

	DungeonRegion.prototype.exportLayers = function()
	{
		Region.prototype.exportLayers.call(this);

		for (var i = 0; i < this.length; i++)
		{
			for (var j = 0; j < this.length; j++)
			{
				var hasStone = (this.layers[ BlockType.STONE ][i][j] == BlockType.STONE);

				this.data[ RegionLayer.BLOCK ][i][j] = BlockType.STONE;
				this.data[ RegionLayer.FLOOR ][i][j] = !hasStone;
				this.data[ RegionLayer.WATER ][i][j] = false;
				this.data[ RegionLayer.OBJECT ][i][j] = null;
			}
		}
	};

	return DungeonRegion;

})();
