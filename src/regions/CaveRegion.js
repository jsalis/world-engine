/**
 * @class			CaveRegion
 * 
 * @author			John Salis <jsalis@stetson.edu>
 * @description		Extension of Region that generates a cave environment.
 */
CaveRegion = (function() {

	var ROCK_DISTRIBUTION = 0.54;
	var DIRT_DISTRIBUTION = 0.50;

	var CA_BORN_THRESHOLD = 5;
	var CA_SURVIVE_THRESHOLD = 3;
	var CA_PASS_COUNT = 6;
	var CA_SMOOTH_PASS_COUNT = 5;
	var CA_SMOOTH_THRESHOLD = 5;

	/**
	 * Class Constructor
	 */
	function CaveRegion()
	{
		Region.call(this);
	}

	Region.addEventListener(RegionEvent.BEFORE_GENERATE, function(e) {

		// Generate rock layer
		prepareLayer(e.list, e.row, e.col, BlockType.ROCK, CA_PASS_COUNT, CA_BORN_THRESHOLD, CA_SURVIVE_THRESHOLD);
		prepareLayer(e.list, e.row, e.col, BlockType.ROCK, CA_SMOOTH_PASS_COUNT, CA_SMOOTH_THRESHOLD, CA_SMOOTH_THRESHOLD);

		// Generate dirt layer
		prepareLayer(e.list, e.row, e.col, BlockType.DIRT, CA_PASS_COUNT, CA_BORN_THRESHOLD, CA_SURVIVE_THRESHOLD);
		prepareLayer(e.list, e.row, e.col, BlockType.DIRT, CA_SMOOTH_PASS_COUNT, CA_SMOOTH_THRESHOLD, CA_SMOOTH_THRESHOLD);
	});

	Region.addEventListener(RegionEvent.AFTER_GENERATE, function(e) {

		addWaterSource(e.list, e.row, e.col);
	});

	/**
	 * [addWaterSource description]
	 * 
	 * @param  {[type]} regList [description]
	 * @param  {[type]} regRow  [description]
	 * @param  {[type]} regCol  [description]
	 * @return {[type]}         [description]
	 */
	function addWaterSource(regList, regRow, regCol)
	{
		for (var d in Direction.NEIGHBORS)
		{
			var dir = Direction.NEIGHBORS[d];
			var region = regList[ regRow + dir.row ][ regCol + dir.col ];

			if (region instanceof CaveRegion)
			{
				var layer = region.data[ RegionLayer.FLOOR ];
				var seed = region.seeds[ BlockType.ROCK ];
				var pos = Generator.getRandomPosition(layer, true, seed);

				region.data[ RegionLayer.OBJECT ][ pos.row ][ pos.col ] = WaterSource;

				WaterSource.generate(regList, regRow + dir.row, regCol + dir.col, pos.row, pos.col);
			}
		}
	}

	/**
	 * [prepareLayer description]
	 * 
	 * @param  {[type]} regList    [description]
	 * @param  {[type]} regRow     [description]
	 * @param  {[type]} regCol     [description]
	 * @param  {[type]} layerIndex [description]
	 * @param  {[type]} passCount  [description]
	 * @param  {[type]} born       [description]
	 * @param  {[type]} survive    [description]
	 */
	function prepareLayer(regList, regRow, regCol, layerIndex, passCount, born, survive)
	{
		for (var n = 0; n < passCount; n++)
		{
			var count = 0;
			var tempData = [];

			// Apply a single iteration to each data set in the neighborhood
			for (var d in Direction.NEIGHBORS)
			{
				var dir = Direction.NEIGHBORS[d];
				var currentReg = regList[ regRow + dir.row ][ regCol + dir.col ];

				if (currentReg instanceof CaveRegion)
				{
					var currentData = currentReg.layers[ layerIndex ];

					tempData[count] = Generator.applyCellAutomata(currentData, born, survive, {
						regList:    regList,
						regRow:     regRow + dir.row,
						regCol:     regCol + dir.col,
						layerIndex: layerIndex,
						dead:       layerIndex,
						live:       BlockType.NONE,
					});
					count ++;
				}
			}

			// Replace each data set in the neighborhood with the new data set
			count = 0;
			for (var d in Direction.NEIGHBORS)
			{
				var dir = Direction.NEIGHBORS[d];
				var currentReg = regList[ regRow + dir.row ][ regCol + dir.col ];

				if (currentReg instanceof CaveRegion)
				{
					currentReg.layers[ layerIndex ] = tempData[count];
					count ++;
				}
			}
		}
	}

	CaveRegion.prototype = Object.create(Region.prototype);

	CaveRegion.prototype.constructor = CaveRegion;

	CaveRegion.prototype.initialize = function()
	{
		Region.prototype.initialize.call(this);

		// Initialize rock layer
		this.layers[ BlockType.ROCK ] = Generator.getRandomData(this.length, ROCK_DISTRIBUTION, {
			seed: this.seeds[ BlockType.ROCK ],
			high: BlockType.ROCK,
			low:  BlockType.NONE,
		});

		// Initialize dirt layer
		this.layers[ BlockType.DIRT ] = Generator.getRandomData(this.length, DIRT_DISTRIBUTION, {
			seed: this.seeds[ BlockType.DIRT ],
			high: BlockType.DIRT,
			low:  BlockType.NONE,
		});
	};

	CaveRegion.prototype.exportLayers = function()
	{
		Region.prototype.exportLayers.call(this);

		for (var i = 0; i < this.length; i++)
		{
			for (var j = 0; j < this.length; j++)
			{
				var hasRock = (this.layers[ BlockType.ROCK ][i][j] == BlockType.ROCK);
				var hasDirt = (this.layers[ BlockType.DIRT ][i][j] == BlockType.DIRT);

				this.data[ RegionLayer.BLOCK ][i][j] = (hasRock && hasDirt) ? BlockType.DIRT : BlockType.ROCK;
				this.data[ RegionLayer.FLOOR ][i][j] = !hasRock;
				this.data[ RegionLayer.WATER ][i][j] = false;
				this.data[ RegionLayer.OBJECT ][i][j] = null;
			}
		}
	};

	return CaveRegion;

})();
